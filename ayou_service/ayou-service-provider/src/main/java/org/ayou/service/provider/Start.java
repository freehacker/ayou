package org.ayou.service.provider;

import java.io.IOException;

import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @category Start.java
 * @version 1.0
 * @author AYOU
 * @date 2016年6月15日 下午5:00:43
 */
public class Start{
	@SuppressWarnings("resource")
	public static void main(String[] args) throws IOException {
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(new String[] { "spring/applicationContext.xml" });
		context.start();
		System.out.println("SOA is OK...");
	}

	/*public void onStartup(ServletContext servletContext) throws ServletException {
		@SuppressWarnings("resource")
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(new String[] { "applicationContext.xml" });
		context.start();
		System.out.println("SOA is OK...");
	}*/
}
