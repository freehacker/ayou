#!/bin/bash

#jdk
JAVA_HOME=/tools/jdk1.8.0_102
PATH=$JAVA_HOME/bin:$PATH
CLASSPATH=.:$JAVA_HOME/lib/dt.jar:$JAVA_HOME/lib/tools.jar
export JAVA_HOME PATH CLASSPATH

#zookeeper
export ZOOKEEPER_HOME=/usr/local/zookeeper
export PATH=$ZOOKEEPER_HOME/bin:$PATH
export PATH

#maven
MAVEN_HOME=/home/service/apache-maven-3.3.9
export MAVEN_HOME
export PATH=${PATH}:${MAVEN_HOME}/bin

#sonar
PATH=/home/service/sonarqube-6.0/bin:$PATH
SONAR_HOME=/home/service/sonarqube-6.0
export PATH SONAR_HOME

#sonar scanners
PATH=$PATH:/home/service/sonarqube-6.0/extensions/plugins/sonar-scanner-2.5.1/bin
export PATH

proc_name="ayou-service-provider.jar"
nohup java -Dfile.encoding=UTF-8 -Dnet.sf.ehcache.skipUpdateCheck=true -XX:+UseConcMarkSweepGC -XX:+CMSClassUnloadingEnabled -XX:+UseParNewGC -XX:MetaspaceSize=128m -XX:MaxMetaspaceSize=512m -jar ${proc_name} &
#nohup java -jar ${proc_name}>$(date +%y%m%d).dubbo.log 2>&1
#nohup java -jar ${proc_name} &
proc_id=`ps -ef|grep -i ${proc_name}|grep -v "grep"|awk '{print $2}'`
echo ${proc_name}" pid:"
echo ${proc_id[@]}
echo -e "\n"
sleep 10s
echo -e "\n"
exit