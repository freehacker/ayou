package com.ayou.user.provider;

import java.util.List;

import com.ayou.module.identity.entity.User;

/**
 * @category 用户接口（提供ZK注入）
 * @author AYOU
 * @version 2016年5月31日 下午8:52:59
 */
public interface UserProvider {
	User view(String uuid);

	List<User> list(User user);

	User count(User user);
}
