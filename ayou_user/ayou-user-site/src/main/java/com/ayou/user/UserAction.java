package com.ayou.user;

import java.io.PrintWriter;
import java.io.StringWriter;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ayou.module.identity.entity.User;

/**
 * @category 用户控制层
 * @author AYOU
 * @version 2016年5月31日 下午9:03:00
 */
@Controller
@RequestMapping("/user")
public class UserAction extends UserBaseAction {
	
	@RequestMapping(value = "/view/{uuid}", method = RequestMethod.GET, produces = { "text/json;charset=UTF-8" })
	@ResponseBody
	public String view(ModelMap map, HttpServletRequest request,@PathVariable("uuid") String uuid) {
		try {
			User user = userProvider.view(uuid);
			
			identityService.setUser(user);
			logger.info(identityService.getUser(user.getUuid(), user).toString());
			User redisU = identityService.getUser(user.getUuid(),user);
			logger.info(redisU.getUuid());
			logger.info(redisU.getName());
			user.setName("AYOU");
			identityService.update(user);
			map.put("result", redisU);
			map.put("artile", articleService.view("571100d44c21497e96fe51db3580e91c"));
			return renderJsonMsg(map, "ok");
		} catch (Exception e) {
			// 打印堆栈信息
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			logger.error("downalod exception:" + e.getMessage() + "\t" + errors.toString());
			e.printStackTrace();
			return renderJsonError(map, "异常", e);
		}
	}
	
	@RequestMapping(value = "/delete/{uuid}", method = RequestMethod.DELETE, produces = { "text/json;charset=UTF-8" })
	@ResponseBody
	public String delete(ModelMap map, HttpServletRequest request,@PathVariable("uuid") String uuid) {
		try {
			map.put("result", userProvider.view(uuid));
			return renderJsonMsg(map, "ok");
		} catch (Exception e) {
			// 打印堆栈信息
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			logger.error("downalod exception:" + e.getMessage() + "\t" + errors.toString());
			e.printStackTrace();
			return renderJsonError(map, "异常", e);
		}
	}
	
	@RequestMapping(value = "/update/{uuid}", method = RequestMethod.PUT, produces = { "text/json;charset=UTF-8" })
	@ResponseBody
	public String update(ModelMap map, HttpServletRequest request,@PathVariable("uuid") String uuid) {
		try {
			map.put("result", userProvider.view(uuid));
			return renderJsonMsg(map, "ok");
		} catch (Exception e) {
			// 打印堆栈信息
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			logger.error("downalod exception:" + e.getMessage() + "\t" + errors.toString());
			e.printStackTrace();
			return renderJsonError(map, "异常", e);
		}
	}
}
