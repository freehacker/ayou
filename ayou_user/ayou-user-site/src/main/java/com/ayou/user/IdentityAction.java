package com.ayou.user;

import java.io.PrintWriter;
import java.io.StringWriter;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ayou.module.identity.service.IdentityService;
import com.ayou.site.base.action.SimpleActionSupport;

/**
 * @category 用户登录注册
 * @version 1.0
 * @author AYOU
 * @date 2016年6月14日 下午5:30:00
 */
@Controller
@RequestMapping("/auth")
public class IdentityAction extends SimpleActionSupport{
	@Resource
	protected IdentityService identityService;
	
	
	@RequestMapping(value = "/login", method = RequestMethod.DELETE, produces = { "text/json;charset=UTF-8" })
	@ResponseBody
	public String delete(ModelMap map, HttpServletRequest request,@PathVariable("uuid") String uuid) {
		try {
			return renderJsonMsg(map, "ok");
		} catch (Exception e) {
			// 打印堆栈信息
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			logger.error("downalod exception:" + e.getMessage() + "\t" + errors.toString());
			e.printStackTrace();
			return renderJsonError(map, "异常", e);
		}
	}
}
