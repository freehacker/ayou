package com.ayou.user;


import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ayou.article.ArticleProvider;
import com.ayou.module.identity.service.IdentityService;
import com.ayou.site.base.action.SimpleActionSupport;
import com.ayou.user.provider.UserProvider;

/**
 * @category UserBaseAction.java
 * @version 1.0
 * @author AYOU
 * @date 2016年6月15日 上午11:17:51
 */
public abstract class UserBaseAction extends SimpleActionSupport{
	protected final Logger logger = LoggerFactory.getLogger(getClass());
	
	@Resource
	protected IdentityService identityService;
	@Resource
	protected UserProvider userProvider;
	@Resource
	protected ArticleProvider articleService;

	/*@Override
	public void onStartup(ServletContext servletContext) throws ServletException {
		logger.info("启动->" + this.getClass().getSimpleName() + "...");
	}*/

}
