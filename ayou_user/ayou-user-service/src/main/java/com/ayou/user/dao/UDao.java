package com.ayou.user.dao;

import java.util.List;

import com.ayou.module.identity.entity.User;

/**
 * @category UDao.java
 * @version test
 * @author AYOU
 * @date 2016年6月17日 下午4:13:21
 */
public interface UDao {
	User view(String uuid);

	List<User> list(User user);

	Integer count(User user);
}
