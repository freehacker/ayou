package com.ayou.user.service;

import java.util.List;

import javax.annotation.Resource;

import com.alibaba.dubbo.config.annotation.Service;
import com.ayou.module.identity.entity.User;
import com.ayou.user.dao.UserDao;
import com.ayou.user.provider.UserProvider;

/**
 * @category 用户服务
 * @author AYOU
 * @version 2016年5月31日 下午8:56:56
 */
@Service(version = "1.0")
public class UserService implements UserProvider {

	@Resource
	private UserDao userDao;

	@Override
	public User view(String uuid) {
		User user = userDao.view(uuid);
		return user;
	}

	@Override
	public List<User> list(User user) {
		return null;
	}

	@Override
	public User count(User user) {
		return null;
	}

}
