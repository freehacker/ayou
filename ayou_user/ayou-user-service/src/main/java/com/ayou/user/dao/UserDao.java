package com.ayou.user.dao;

import java.util.List;

import com.ayou.module.identity.entity.User;

/**
 * @category UserDao.java
 * @author AYOU
 * @version 2016年5月31日 下午9:24:55
 */
public interface UserDao {
	User view(String uuid);

	List<User> list(User user);

	Integer count(User user);
}
