package com.ayou.user;

import java.io.IOException;

import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @category 启动-注入
 * @author AYOU
 * @version 2016年5月31日 下午9:14:55
 */
@SuppressWarnings("resource")
public class Main {
	public static void main(String[] args) throws IOException {
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(new String[] { "appctx-user-service.xml" });
		context.start();
		System.out.println("User is OK...");
		System.in.read(); 
	}
}
