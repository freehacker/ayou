define([ 'jquery', 'layer' ], function($, layer) {
	var ayou = {
		ajax : function(config) {
			var loading;
			$.ajax({
				type : config.type || "post",// post方式优先
				async : config.async === false ? false : true,// 异步优先
				cache : config.cache === false ? false : true,// 缓存优先
				dataType : config.dataType || "json",// json格式优先
				url : config.url,
				data : config.data,
				timeout : config.type || 10 * 1000,// 10s
				beforeSend : function() {
					if(config.loading == "full"){
						loading = layer.load();
					}
				},
				success : function(data) {
					//layer.close(loading);
					if ($.isFunction(config.success)) {
						config.success.call(this, data);
					}
				},
				error : function(request, status, thrown) {
					if ($.isFunction(config.error)) {
						config.error.call(this, request, status, thrown);
					} else {
						//layer.close(loading);
						layer.msg('网络链接超时！', {
							icon : 5
						});
						/*layer.msg("网络错误！", {
							offset: 'rb',
							icon : 5,
							shift: 6
						});*/
						// lovingtrip.ajaxError(request,status,thrown);
					}
				}
			});
		}
	}
	return ayou;
})