define("/ibatis/js/base/comm",["jquery","avalon"],function($,avalon){
	var lazy = {
		ajax : function(config){
			if($.isFunction(config.pre)){//前置函数
				config.pre();
			}
			$.ajax({
				type : config.type || "post",//post方式优先
				async : config.async===false ? false : true,//异步优先
				cache : config.cache===false ? false : true,//缓存优先
				dataType : config.dataType || "json",//json格式优先
				url : config.url,
				data : config.data,
				success : function(data){
					console.log(data);
					if($.isFunction(config.success)){
						config.success.call(this,data);
					}
					if($.isFunction(config.post)){
					    window.post = config.post;//此处是为了延时执行后置函数，由于新增等操作以后ES并不一定马上就修改，因此采用延时执行后置函数
					    window.postData = data;
						setTimeout("post(postData);",3000);//延迟三秒执行
					}
				},
				error : function(request,status,thrown){
					if($.isFunction(config.error)){
						config.error.call(this,request,status,thrown);
					}else{
					}
				}
			});
		},
	}
})