var UM = null;
/**
 * user
 */
define(["avalon","jquery"], function(avalon,$) {
	var sysLoginVM = avalon.define({
		$id : "sysLogin",
		show : false,
		model:{},
		logout:function(){
			/*var ajaxCall = jQuery.getJSON('https://passport.qtdu.com/sso/out' + "?callback=?", {}, function(data) {
				console.log(data);
			});*/
			$.ajax({
				type : "get",
				timeout : 30000,
				async : false,
				cache : true,
				dataType : "json",
				url : "/sso/logout",
				success : function(data){
					if(data.success){
						if(login && data.r_url){//如果是需要登录的页面就跳出
							window.location.href = decodeURIComponent(data.r_url);
						}
						UM = {};
						sysLoginVM.model=UM;
					}
				},
				error : function(data){
					console.log(data);
				}
			});
		},
		login:function(){
			window.location.href = "https://passport.qtdu.com/login.html?ReturnURL=" + encodeURIComponent(window.location.href);
		},
		reg:function(){
			window.location.href = "https://passport.qtdu.com/register.html?ReturnURL=" + encodeURIComponent(window.location.href);
		}
	});
	/**
	 * 过滤器
	 */
	avalon.filters.FTopName = function(str) {
		if (str) {
			return str;
		}else if(sysLoginVM.model.email){
			return sysLoginVM.model.email;
		}else if(sysLoginVM.model.phone){
			return sysLoginVM.model.phone;
		}
	};
	/**
	 * 配置用户信息
	 * @param data
	 */
	var L_O = 0;
	function configUM(){
		$.ajax({
			type : "get",
			timeout : 30000,
			async : true,
			cache : true,
			dataType : "json",
			url : "/sso/user_config",
			success : function(data){
				if(data.user){
					UM = data.user;
					sysLoginVM.model=UM;
				}
				if(!UM && L_O == 0){
					L_O = 1;
					getUM();
				}
			},
			error : function(data){
				/*var href = encodeURIComponent(window.location.href);
				window.location = "/user/login.html?reDirect="+href;*/
				console.log(data);
			}
		});
	}
	/**
	 * 获取用户信息
	 */
	function getUM(){
		var _localSSO = {};
		if(!UM){
			$.ajax({
				type : "get",
				timeout : 30000,
				async : true,
				cache : true,
				dataType : "json",
				url : "/sso/sso_login",
				success : function(data){
					if(data.success){
						_localSSO = data;
					}
					var ajaxCall = jQuery.getJSON(_localSSO.askurl + "?callback=?", {askData : _localSSO.askData}, function(d) {
						if (d.msg == "-1") {
							if(login){
								window.location.href = "https://passport.qtdu.com/login.html?ReturnURL=" + encodeURIComponent(window.location.href);
							}
						} else {
							$.post(_localSSO.okurl, {replyTxt : d.msg, ReturnURL:encodeURIComponent(window.location.href)}, function(e) {
								configUM();
							}, "json");
						}
					});
				},
				error : function(data){
					console.log(data);
				}
			});
		}
	}
	(function(){
		configUM();
	})();
});