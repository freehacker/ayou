/**
 * 截取URL参数
 * 
 * @param str
 * @param sep
 * @returns 示例：var params =
 *          unparam(decodeURI(decodeURI(location.search.substring(1)))); var red =
 *          params['reDirect'];
 */
function unParam(str, sep) {
	if (typeof str !== 'string' || (str.trim().length === 0))
		return {};
	var ret = {}, pairs = str.split(sep || '&'), pair, key, val, m, i = 0, len = pairs.length;
	for (; i < len; i++) {
		pair = pairs[i].split('=');
		key = decodeURIComponent(pair[0]);
		// pair[1] 可能包含gbk编码中文, 而decodeURIComponent 仅能处理utf-8 编码中文
		try {
			val = decode(pair[1]);
		} catch (e) {
			val = pair[1] || '';
		}
		if ((m = key.match(/^(\w+)\[\]$/)) && m[1]) {
			ret[m[1]] = ret[m[1]] || [];
			ret[m[1]].push(val);
		} else {
			ret[key] = val;
		}
	}
	return ret;
}
/**
 * @param p
 *            要获取的参数名称
 * @returns
 */
function getParam(p) {
	return unParam(decodeURI(decodeURI(location.search.substring(1))))[p];
}

/**
 * 用来判断页面是否需要登录
 */
var login = true;

(function() {
	try {
		window.console
			&& !document.all
			&& console.log("\n%cQTDU\n%c\u6211\u7684\u535a\u5ba2\u5730\u5740:https://www.bcd.ren\n%c\u6211\u7684\u89e3\u51b3\u65b9\u6848:https://www.qtdu.com",
					"font-family:Consolas,Monaco,'Courier New',Helvetica;font-size:30px;color:#000;line-height:25px;", "color:#333;line-height:30px", "color:#333;");
		var _console = console;
		Object.defineProperty(window, "console", {
			get : function() {
				if (_console._commandLineAPI)
					throw "抱歉, 为了用户安全, 本网站已禁用console脚本功能";
				return _console;
			},
			set : function(val) {
				_console = val;
			}
		})
	} catch (ignore) {
	}
	;
})()