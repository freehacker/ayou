/**
 * 处理jquery被污染
 */
define([ 'jquery' ], function($) {
	return jQuery.noConflict(true);
});