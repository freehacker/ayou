/* 压缩基础require的plugins
domReady : "plugin/require/domReady",
text : "plugin/require/text",
css : "plugin/require/css",
json : "plugin/require/json",     
image : "plugin/require/image",
async : "plugin/require/async",
*/
//"use strict";
//define("text",["module"],function(e){"use strict";var t,n,r,i,s,o=["Msxml2.XMLHTTP","Microsoft.XMLHTTP","Msxml2.XMLHTTP.4.0"],u=/^\s*<\?xml(\s)+version=[\'\"](\d)*.(\d)*[\'\"](\s)*\?>/im,a=/<body[^>]*>\s*([\s\S]+)\s*<\/body>/im,f=typeof location!="undefined"&&location.href,l=f&&location.protocol&&location.protocol.replace(/\:/,""),c=f&&location.hostname,h=f&&(location.port||undefined),p={},d=e.config&&e.config()||{};t={version:"2.0.13+",strip:function(e){if(e){e=e.replace(u,"");var t=e.match(a);t&&(e=t[1])}else e="";return e},jsEscape:function(e){return e.replace(/(['\\])/g,"\\$1").replace(/[\f]/g,"\\f").replace(/[\b]/g,"\\b").replace(/[\n]/g,"\\n").replace(/[\t]/g,"\\t").replace(/[\r]/g,"\\r").replace(/[\u2028]/g,"\\u2028").replace(/[\u2029]/g,"\\u2029")},createXhr:d.createXhr||function(){var e,t,n;if(typeof XMLHttpRequest!="undefined")return new XMLHttpRequest;if(typeof ActiveXObject!="undefined")for(t=0;t<3;t+=1){n=o[t];try{e=new ActiveXObject(n)}catch(r){}if(e){o=[n];break}}return e},parseName:function(e){var t,n,r,i=!1,s=e.lastIndexOf("."),o=e.indexOf("./")===0||e.indexOf("../")===0;return s!==-1&&(!o||s>1)?(t=e.substring(0,s),n=e.substring(s+1)):t=e,r=n||t,s=r.indexOf("!"),s!==-1&&(i=r.substring(s+1)==="strip",r=r.substring(0,s),n?n=r:t=r),{moduleName:t,ext:n,strip:i}},xdRegExp:/^((\w+)\:)?\/\/([^\/\\]+)/,useXhr:function(e,n,r,i){var s,o,u,a=t.xdRegExp.exec(e);return a?(s=a[2],o=a[3],o=o.split(":"),u=o[1],o=o[0],(!s||s===n)&&(!o||o.toLowerCase()===r.toLowerCase())&&(!u&&!o||u===i)):!0},finishLoad:function(e,n,r,i){r=n?t.strip(r):r,d.isBuild&&(p[e]=r),i(r)},load:function(e,n,r,i){if(i&&i.isBuild&&!i.inlineText){r();return}d.isBuild=i&&i.isBuild;var s=t.parseName(e),o=s.moduleName+(s.ext?"."+s.ext:""),u=n.toUrl(o),a=d.useXhr||t.useXhr;if(u.indexOf("empty:")===0){r();return}!f||a(u,l,c,h)?t.get(u,function(n){t.finishLoad(e,s.strip,n,r)},function(e){r.error&&r.error(e)}):n([o],function(e){t.finishLoad(s.moduleName+"."+s.ext,s.strip,e,r)})},write:function(e,n,r,i){if(p.hasOwnProperty(n)){var s=t.jsEscape(p[n]);r.asModule(e+"!"+n,"define(function () { return '"+s+"';});\n")}},writeFile:function(e,n,r,i,s){var o=t.parseName(n),u=o.ext?"."+o.ext:"",a=o.moduleName+u,f=r.toUrl(o.moduleName+u)+".js";t.load(a,r,function(n){var r=function(e){return i(f,e)};r.asModule=function(e,t){return i.asModule(e,f,t)},t.write(e,a,r,s)},s)}};if(d.env==="node"||!d.env&&typeof process!="undefined"&&process.versions&&!!process.versions.node&&!process.versions["node-webkit"]&&!process.versions["atom-shell"])n=require.nodeRequire("fs"),t.get=function(e,t,r){try{var i=n.readFileSync(e,"utf8");i[0]==="﻿"&&(i=i.substring(1)),t(i)}catch(s){r&&r(s)}};else if(d.env==="xhr"||!d.env&&t.createXhr())t.get=function(e,n,r,i){var s=t.createXhr(),o;s.open("GET",e,!0);if(i)for(o in i)i.hasOwnProperty(o)&&s.setRequestHeader(o.toLowerCase(),i[o]);d.onXhr&&d.onXhr(s,e),s.onreadystatechange=function(t){var i,o;s.readyState===4&&(i=s.status||0,i>399&&i<600?(o=new Error(e+" HTTP status: "+i),o.xhr=s,r&&r(o)):n(s.responseText),d.onXhrComplete&&d.onXhrComplete(s,e))},s.send(null)};else if(d.env==="rhino"||!d.env&&typeof Packages!="undefined"&&typeof java!="undefined")t.get=function(e,t){var n,r,i="utf-8",s=new java.io.File(e),o=java.lang.System.getProperty("line.separator"),u=new java.io.BufferedReader(new java.io.InputStreamReader(new java.io.FileInputStream(s),i)),a="";try{n=new java.lang.StringBuffer,r=u.readLine(),r&&r.length()&&r.charAt(0)===65279&&(r=r.substring(1)),r!==null&&n.append(r);while((r=u.readLine())!==null)n.append(o),n.append(r);a=String(n.toString())}finally{u.close()}t(a)};else if(d.env==="xpconnect"||!d.env&&typeof Components!="undefined"&&Components.classes&&Components.interfaces)r=Components.classes,i=Components.interfaces,Components.utils["import"]("resource://gre/modules/FileUtils.jsm"),s="@mozilla.org/windows-registry-key;1"in r,t.get=function(e,t){var n,o,u,a={};s&&(e=e.replace(/\//g,"\\")),u=new FileUtils.File(e);try{n=r["@mozilla.org/network/file-input-stream;1"].createInstance(i.nsIFileInputStream),n.init(u,1,0,!1),o=r["@mozilla.org/intl/converter-input-stream;1"].createInstance(i.nsIConverterInputStream),o.init(n,"utf-8",n.available(),i.nsIConverterInputStream.DEFAULT_REPLACEMENT_CHARACTER),o.readString(n.available(),a),o.close(),n.close(),t(a.value)}catch(f){throw new Error((u&&u.path||"")+": "+f)}};return t}),define("json",["text"],function(text){function cacheBust(e){return e=e.replace(CACHE_BUST_FLAG,""),e+=e.indexOf("?")<0?"?":"&",e+CACHE_BUST_QUERY_PARAM+"="+Math.round(2147483647*Math.random())}var CACHE_BUST_QUERY_PARAM="bust",CACHE_BUST_FLAG="!bust",jsonParse=typeof JSON!="undefined"&&typeof JSON.parse=="function"?JSON.parse:function(val){return eval("("+val+")")},buildMap={};return{load:function(e,t,n,r){r.isBuild&&(r.inlineJSON===!1||e.indexOf(CACHE_BUST_QUERY_PARAM+"=")!==-1)||t.toUrl(e).indexOf("empty:")===0?n(null):text.get(t.toUrl(e),function(t){var i;if(r.isBuild)buildMap[e]=t,n(t);else{try{i=jsonParse(t)}catch(s){n.error(s)}n(i)}},n.error,{accept:"application/json"})},normalize:function(e,t){return e.indexOf(CACHE_BUST_FLAG)!==-1&&(e=cacheBust(e)),t(e)},write:function(e,t,n){if(t in buildMap){var r=buildMap[t];n('define("'+e+"!"+t+'", function(){ return '+r+";});\n")}}}}),define("image",[],function(){function r(){}function i(n){return n=n.replace(t,""),n+=n.indexOf("?")<0?"?":"&",n+e+"="+Math.round(2147483647*Math.random())}var e="bust",t="!bust",n="!rel";return{load:function(e,t,i,s){var o;s.isBuild?i(null):(o=new Image,o.onerror=function(e){i.error(e)},o.onload=function(e){i(o);try{delete o.onload}catch(t){o.onload=r}},e.indexOf(n)!==-1?o.src=t.toUrl(e.replace(n,"")):o.src=e)},normalize:function(e,n){return e.indexOf(t)===-1?e:i(e)}}}),define("css",[],function(){if(typeof window=="undefined")return{load:function(e,t,n){n()}};var e=document.getElementsByTagName("head")[0],t=window.navigator.userAgent.match(/Trident\/([^ ;]*)|AppleWebKit\/([^ ;]*)|Opera\/([^ ;]*)|rv\:([^ ;]*)(.*?)Gecko\/([^ ;]*)|MSIE\s([^ ;]*)|AndroidWebKit\/([^ ;]*)/)||0,n=!1,r=!0;t[1]||t[7]?n=parseInt(t[1])<6||parseInt(t[7])<=9:t[2]||t[8]?r=!1:t[4]&&(n=parseInt(t[4])<18);var i={};i.pluginBuilder="./css-builder";var s,o,u=function(){s=document.createElement("style"),e.appendChild(s),o=s.styleSheet||s.sheet},a=0,f=[],l,c=function(e){o.addImport(e),s.onload=function(){h()},a++,a==31&&(u(),a=0)},h=function(){l();var e=f.shift();if(!e){l=null;return}l=e[1],c(e[0])},p=function(e,t){(!o||!o.addImport)&&u();if(o&&o.addImport)l?f.push([e,t]):(c(e),l=t);else{s.textContent='@import "'+e+'";';var n=setInterval(function(){try{s.sheet.cssRules,clearInterval(n),t()}catch(e){}},10)}},d=function(t,n){var i=document.createElement("link");i.type="text/css",i.rel="stylesheet";if(r)i.onload=function(){i.onload=function(){},setTimeout(n,7)};else var s=setInterval(function(){for(var e=0;e<document.styleSheets.length;e++){var t=document.styleSheets[e];if(t.href==i.href)return clearInterval(s),n()}},10);i.href=t,e.appendChild(i)};return i.normalize=function(e,t){return e.substr(e.length-4,4)==".css"&&(e=e.substr(0,e.length-4)),t(e)},i.load=function(e,t,r,i){(n?p:d)(t.toUrl(e+".css"),r)},i}),define("domReady",[],function(){"use strict";function u(e){var t;for(t=0;t<e.length;t+=1)e[t](s)}function a(){var e=o;i&&e.length&&(o=[],u(e))}function f(){i||(i=!0,n&&clearInterval(n),a())}function c(e){return i?e(s):o.push(e),c}var e,t,n,r=typeof window!="undefined"&&window.document,i=!r,s=r?document:null,o=[];if(r){if(document.addEventListener)document.addEventListener("DOMContentLoaded",f,!1),window.addEventListener("load",f,!1);else if(window.attachEvent){window.attachEvent("onload",f),t=document.createElement("div");try{e=window.frameElement===null}catch(l){}t.doScroll&&e&&window.external&&(n=setInterval(function(){try{t.doScroll(),f()}catch(e){}},30))}document.readyState==="complete"&&f()}return c.version="2.0.1",c.load=function(e,t,n,r){r.isBuild?n(null):c(n)},c}),define("async",[],function(){function n(e){var t,n;t=document.createElement("script"),t.type="text/javascript",t.async=!0,t.src=e,n=document.getElementsByTagName("script")[0],n.parentNode.insertBefore(t,n)}function r(t,n){var r=/!(.+)/,i=t.replace(r,""),s=r.test(t)?t.replace(/.+!/,""):e;return i+=i.indexOf("?")<0?"?":"&",i+s+"="+n}function i(){return t+=1,"__async_req_"+t+"__"}var e="callback",t=0;return{load:function(e,t,s,o){if(o.isBuild)s(null);else{var u=i();window[u]=s,n(r(t.toUrl(e),u))}}}}),define("require.plugin",["text","json","image","css","normalize","domReady","async"],function(){var e={text:!0,json:!0,image:!0,css:!0,normalize:!0,domReady:!0,async:!0};return window.require.plugin=e,require});

var version = 20161024;
try{
    version = AYOU.version || 20161024;
}catch(e){
    version = 20161024;
}

require.config({
    urlArgs : "v="+version,
	baseUrl : "/",//根目录
    paths:{   //这里配置的地址，都是相对于上方的baseUrl的
        //jquery :"webbase/plugin/jquery/jquery-2.1.4.min",
        jquery :["//cdn.bootcss.com/jquery/3.0.0/jquery.min","http://libs.baidu.com/jquery/2.1.4/jquery.min","webbase/plugin/jquery/jquery-2.1.4.min","//cdn.bootcss.com/jquery/1.10.1/jquery.min"],
        jqueryui :["//cdn.bootcss.com/jqueryui/1.12.0/jquery-ui.min","webbase/plugin/jquery/jquery-ui","http://libs.baidu.com/jquery/2.1.4/jquery.min"],
        lazyload :['//cdn.bootcss.com/jquery_lazyload/1.9.7/jquery.lazyload.min'],
        //'jquery-private': 'webbase/plugin/jquery/jquery-private',
        noty : "webbase/plugin/noty/jquery.noty-2.3.8",
        layer : ['//cdn.bootcss.com/layer/2.4/layer.min','webbase/plugin/noty/layer-v2.3/layer-all'],
        //layer : "webbase/plugin/noty/layer-v2.3/layer-all",
        pjax : '//cdn.bootcss.com/jquery.pjax/1.9.6/jquery.pjax.min',
        highlight : 'webbase/plugin/highlight/highlight-9.6',
        
        //query :"plugin/jquery/jquery.query",
        //roller : "plugin/roller/roller",
        
        bootstrap : ['//cdn.bootcss.com/bootstrap/3.3.5/js/bootstrap.min','webbase/plugin/bootstrap/js/bootstrap.min'],
        custom : "webbase/plugin/bootstrap/js/js/custom",
        //markdown : "//cdn.bootcss.com/markdown.js/0.6.0-beta1/markdown.min",
        markdown : "//cdn.bootcss.com/markdown.js/0.5.0/markdown.min",
        util : "webbase/base/js/util",
        ayou : "webbase/base/js/ayou",
        init : "webbase/base/js/init",
        img : "webbase/base/js/img",
        /*
       // require:"plugin/require",
        requireConfig : "plugin/require/require.config",
        domReady : "plugin/require/domReady",
        */
        
        domReady : ['//cdn.bootcss.com/require-domReady/2.0.1/domReady.min','webbase/plugin/require/domReady'],
        text : ['//cdn.bootcss.com/require-text/2.0.12/text.min','webbase/plugin/require/text'],
        css : ['//cdn.bootcss.com/require-css/0.1.8/css.min','webbase/plugin/require/css'],
        json : ['','webbase/plugin/require/json'],
        font : "webbase/plugin/require/font", 
        image : ['//cdn.bootcss.com/imager.js/0.5.0/Imager.min','webbase/plugin/require/image'],
        async : "webbase/plugin/require/async",
        
        //echarts :"plugin/echarts-2.2.3/echarts-all",  
        //sweetalert : "plugin/sweetalert/sweetalert.min",
        //animate : "plugin/animate/animate", 
        
        avalon:['//cdn.bootcss.com/avalon.js/1.4.7.2/avalon.shim',"webbase/plugin/avalon/1.4.7.2/avalon.shim","//cdn.bootcss.com/avalon.js/1.4.6.3/avalon.min","//cdn.bootcss.com/avalon.js/1.4.7.1/avalon","webbase/plugin/avalon/min"],
        avalon2:['//cdn.bootcss.com/avalon.js/2.1.5/avalon'],
        //avalon : "//cdn.bootcss.com/avalon.js/1.5.6/avalon",
        //avalon : "//cdn.bootcss.com/avalon.js/2.0s/avalon",
        oniui : "webbase/plugin/oniui",
        //OniUI :  "plugin/avalon-1.4.4/oniui.packaged.min",
 
        baseCss : "css",
        //basic : "js/basic",
        //constant : "js/basic/constant",
        //resource : "resource",
        //zoneBase : "resource/zone.base.json",
        //zoneMore : "resource/zone.more.json",
        //"basic/lovingtrip" : "js/lovingtrip/lovingtrip",
        
        //wap使用
        //swiper : "plugin/swiper/swiper.min",
        
        //模块
        //manage : "manage/js/manage",//注意在不同项目下加上了对应的名称，或者在调用时
        //tourguidedispatch : "tourguidedispatch/js/tourguidedispatch",
        //trademanage : "trademanage/js/trademanage",
       // publicservice : "publicservice/js/publicservice",
        //dataportprovincial : "dataportprovincial/js/dataportprovincial",
        //marketmonitor : "marketmonitor/js/marketmonitor"
        user:'user/js',
    },
    
    shim:{
        ayou:{exports: "ayou"},
        //"basic/lovingtrip" : { exports: "basic/lovingtrip" },
        /*noty : {
        	deps: ["jquery"],
            exports: "noty"
        },*/
        //query : ["jquery"],
        query:{ exports: "jquery" },
        // roller : ["jquery","css!plugin/roller/roller.css","/plugin/roller/pClock.js"],
        //bootstrap :["css!webbase/plugin/bootstrap/css/bootstrap.min","jquery"],
        bootstrap :["jquery"],//"css!//cdn.bootcss.com/bootstrap/3.3.6/css/bootstrap.min",
        pjax : {deps : ['jquery'],exports : 'pjax'},
        jqueryui : {deps : ['jquery'],exports : 'jqueryui'},
		img : {deps : ['jquery'],exports : 'img'},
        ////cdn.bootcss.com/bootstrap/4.0.0-alpha.2/css/bootstrap.min
        
        //bootstrap : ["jquery","css!webbase/plugin/bootstrap/css/bootstrap.min",],
        //"css!webbase/plugin/bootstrap/css/font-awesome.min"
		// sweetalert : ["css!plugin/sweetalert/sweetalert.css"],
		// swiper : ["css!plugin/swiper/swiper.min.css"]
		layer : {
			deps : ['jquery', 'css!//cdn.bootcss.com/layer/2.4/skin/layer.min'],
			exports : "layer"
		},
		avalon : {
			exports : "avalon"
		},
		avalon2 : {
			exports : "avalon2"
		},
		text: {
			exports : "text"
		},
		highlight : {
			deps : ['css!webbase/plugin/highlight/highlight-9.6'],
			exports : 'highlight',
		},
		lazyload : {
			deps : ['jquery'],
			exports : 'lazyload',
		},
	},
    
    /*
	 * map: { 'layer': { 'jquery': 'layer'} },
	 */
    
    waitSeconds: 0
});