var version = 20170524;
try{
    version = AYOU.version || 20170524;
}catch(e){
    version = 20170524;
}
require.config({
    urlArgs : "v="+version,
	baseUrl : "/",
    paths:{
        jquery :["//cdn.bootcss.com/jquery/3.0.0/jquery.min","http://libs.baidu.com/jquery/2.1.4/jquery.min","webbase/plugin/jquery/jquery-2.1.4.min","//cdn.bootcss.com/jquery/1.10.1/jquery.min"],
        jqueryui :["//cdn.bootcss.com/jqueryui/1.12.0/jquery-ui.min","webbase/plugin/jquery/jquery-ui","http://libs.baidu.com/jquery/2.1.4/jquery.min"],
        lazyload :['//cdn.bootcss.com/jquery_lazyload/1.9.7/jquery.lazyload.min'],
        noty : "webbase/plugin/noty/jquery.noty-2.3.8",
        layer : ['//cdn.bootcss.com/layer/2.4/layer.min','webbase/plugin/noty/layer-v2.3/layer-all'],
        pjax : '//cdn.bootcss.com/jquery.pjax/1.9.6/jquery.pjax.min',
        highlight : 'webbase/plugin/highlight/highlight-9.6',
		viode : [ '//cdn.bootcss.com/video.js/5.13.0/video.min' ],
        bootstrap : ['//cdn.bootcss.com/bootstrap/3.3.5/js/bootstrap.min','webbase/plugin/bootstrap/js/bootstrap.min'],
        custom : "webbase/plugin/bootstrap/js/js/custom",
        markdown : "//cdn.bootcss.com/markdown.js/0.5.0/markdown.min",
        util : "webbase/base/js/util",
        ayou : "webbase/base/js/ayou",
        init : "webbase/base/js/init",
        img : "webbase/base/js/img",
        domReady : ['//cdn.bootcss.com/require-domReady/2.0.1/domReady.min','webbase/plugin/require/domReady'],
        text : ['//cdn.bootcss.com/require-text/2.0.12/text.min','webbase/plugin/require/text'],
        css : ['//cdn.bootcss.com/require-css/0.1.8/css.min','webbase/plugin/require/css'],
        json : ['','webbase/plugin/require/json'],
        font : "webbase/plugin/require/font", 
        image : ['//cdn.bootcss.com/imager.js/0.5.0/Imager.min','webbase/plugin/require/image'],
        async : "webbase/plugin/require/async",
        avalon:['//cdn.bootcss.com/avalon.js/1.4.7.2/avalon.shim',"webbase/plugin/avalon/1.4.7.2/avalon.shim","//cdn.bootcss.com/avalon.js/1.4.6.3/avalon.min","//cdn.bootcss.com/avalon.js/1.4.7.1/avalon","webbase/plugin/avalon/min"],
        avalon2:['//cdn.bootcss.com/avalon.js/2.1.5/avalon'],
        oniui : "webbase/plugin/oniui",
        baseCss : "css",
        user:'user/js',
        canvasparticle:'webbase/plugin/effects/canvasparticles/canvas-particle',
        particles:'webbase/plugin/effects/jquery-particles/particles.min',
    },
    shim:{
        ayou:{exports: "ayou"},
        query:{ exports: "jquery" },
        bootstrap :["jquery"],
        pjax : {deps : [ 'jquery'],exports : 'pjax'},
        jqueryui : {deps : [ 'jquery'],exports : 'jqueryui'},
		img : {deps : [ 'jquery'],exports : 'img'},
		layer : {
			deps : ['jquery', 'css!//cdn.bootcss.com/layer/2.4/skin/layer.min'],
			exports : "layer"
		},
		avalon : {
			exports : "avalon"
		},
		avalon2 : {
			exports : "avalon2"
		},
		text: {
			exports : "text"
		},
		highlight : {
			deps : [ 'css!webbase/plugin/highlight/highlight-9.6'],
			exports : 'highlight',
		},
		lazyload : {
			deps : [ 'jquery'],
			exports : 'lazyload',
		},
		viode:{
			deps : ['jquery', 'css!//cdn.bootcss.com/video.js/5.13.0/alt/video-js-cdn.min'],
			exports : "viode"
		}
	},
    waitSeconds: 0
});