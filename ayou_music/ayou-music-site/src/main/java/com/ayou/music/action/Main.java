package com.ayou.music.action;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.ayou.music.service.WYService;
import com.ayou.music.service.impl.WYServiceImpl;

/**
 * @category Main.java
 * @version 1.0
 * @author AYOU
 * @date 2016年8月1日 下午1:46:50
 */
public class Main {
	static WYService wyservice = new WYServiceImpl();

	public static void main(String[] args) {
		try {
			String str = wyservice.collectStrByID("429995284");
			JSONObject js = new JSONObject(str);
			js = (JSONObject) js.get("result");
			JSONArray tracks = (JSONArray) js.get("tracks");
			JSONObject wyJson = new JSONObject();
			wyJson.put("wyid", "429995284");
			wyJson.put("name", js.get("name"));
			wyJson.put("description", js.get("description"));
			wyJson.put("coverImgUrl", js.get("coverImgUrl"));
			
			System.out.println(js.get("coverImgUrl"));
			JSONObject gd = new JSONObject();
			JSONArray gdarr = new JSONArray();
			for(int i =0 ;i<tracks.length();i++){
				JSONObject t = (JSONObject) tracks.get(i);
				JSONObject album = (JSONObject) t.get("album");
				JSONArray artists = (JSONArray) t.get("artists");
				JSONObject a = (JSONObject) artists.get(0);
				gd.put("albumName", album.get("name"));
				gd.put("albumType", album.get("type"));
				gd.put("albumBlurPicUrl", album.get("blurPicUrl"));
				gd.put("trackName", t.get("name"));
				gd.put("trackMp3Url", t.get("mp3Url"));
				gd.put("artistName", a.get("name"));
				gd.put("singer", a.get("name"));
				gdarr.put(gd);
			}
			wyJson.put("msg", gdarr);
			System.out.println(wyJson.toString());
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

}
