package com.ayou.music.service;

public class WYConstant {
	public static final String BASE_S_URL = "s.music.163.com";
	public static final String BASE_DOMAIN_URL = "music.163.com";
	public static final String SONGURL_ID = "http://music.163.com/api/song/detail/?ids=";
	public static final String SONGURL_NAME = "";

	public static final String POST_POST = "POST" + " ";
	public static final String POST_GET = "GET" + " ";

	public static final Integer PORT_HTTP = 80;

	public static final String URL_BEGIN = "http://s.music.163.com/search/get/?src=lofter&type=1&filterDj=true&s=";
	public static final String URL_END = "&limit=20&offset=0&callback=loft.w.g.cbFuncSearchMusic";

	public static final String PLAY_LIST_ID = "http://music.163.com/api/playlist/detail?id=";
}
