package com.ayou.music.service;

/**
* @author AYOU
* @version 2016年3月30日 下午10:14:15
*/
public interface WYService {
	public String oneSongStrByID(String... ids);
	public String oneSongStrByName(String name);
	public String collectStrByID(String id);
}
