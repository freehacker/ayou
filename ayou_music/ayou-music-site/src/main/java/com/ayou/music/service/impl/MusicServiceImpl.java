package com.ayou.music.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.ayou.module.base.dao.JdbcDao;
import com.ayou.music.dao.RedisDao;
import com.ayou.music.entity.MusicEntity;
import com.ayou.music.param.MusicQueryParams;
import com.ayou.music.service.MusicService;
import com.ayou.music.service.WYService;

/**
 * @author AYOU
 * @version 2016年3月30日 下午10:26:36
 */

@Service
public class MusicServiceImpl implements MusicService {
	Logger logger = LoggerFactory.getLogger(getClass());

	@Resource
	private JdbcDao jdbcDao;

	@Resource
	private WYService wyservice;

	@Resource
	private RedisDao<MusicEntity> redisDao;

	@Override
	public Map<String, Object> getMusicByCollects(MusicQueryParams p) {
		List<MusicEntity> musics = list(p);
		Map<String, Object> map = new HashMap<String, Object>();
		for (int i = 0; i < musics.size(); i++) {
			Integer id = musics.get(i).getCode();
			String result = wyservice.collectStrByID(id + "");
			map.put("ok", result);
		}
		return map;
	}

	@Override
	public List<MusicEntity> list(MusicQueryParams p) {
		List<Object> params = new ArrayList<Object>();
		String sql = p.buildSql(params);
		Object[] args = params.toArray(new Object[0]);

		return jdbcDao.queryForObjectList(MusicEntity.class, sql, args);
	}

	@Override
	public String collectStrByID(String id) {
		return wyservice.collectStrByID(id);
	}

	@Override
	public String byIdStr(String id) {
		String str = redisDao.get("music_" + id);
		logger.info(id + "Redis->" + str);
		if (str == null) {
			str = wyservice.collectStrByID(id);
			logger.info(id + "WYService->" + str);
			if (StringUtils.isNotBlank(str)) {
				try {
					JSONObject js = new JSONObject(str);
					str = js.get("result").toString();
					redisDao.add("music_" + id, str);
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}
		return str;
	}

	@Override
	public String lbyIdStr(Long index) {
		return redisDao.lget("music", index);
	}

	@Override
	public Boolean addIdStr(String id) {
		if (id == null) {
			return false;
		}
		try {
			String str = wyservice.collectStrByID(id);
			JSONObject js = new JSONObject(str);
			str = js.get("result").toString();
			return redisDao.add("music_" + id, str);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public Long laddIdStr(String id) {
		try {
			/*String str = wyservice.collectStrByID(id);
			JSONObject js = new JSONObject(str);
			str = js.get("result").toString();*/
			
			String str = wyservice.collectStrByID(id);
			JSONObject js = new JSONObject(str);
			js = (JSONObject) js.get("result");
			JSONArray tracks = (JSONArray) js.get("tracks");
			JSONObject wyJson = new JSONObject();
			wyJson.put("wyid", id);
			wyJson.put("name", js.get("name"));
			wyJson.put("description", js.get("description"));
			wyJson.put("coverImgUrl", js.get("coverImgUrl"));
			JSONArray gdarr = new JSONArray();
			for (int i = 0; i < tracks.length(); i++) {
				JSONObject gd = new JSONObject();
				JSONObject t = (JSONObject) tracks.get(i);
				JSONObject album = (JSONObject) t.get("album");
				JSONArray artists = (JSONArray) t.get("artists");
				JSONObject a = (JSONObject) artists.get(0);
				gd.put("albumName", album.get("name"));
				gd.put("albumType", album.get("type"));
				gd.put("albumBlurPicUrl", album.get("blurPicUrl"));
				gd.put("trackName", t.get("name"));
				gd.put("trackMp3Url", t.get("mp3Url"));
				gd.put("artistName", a.get("name"));
				//gd.put("singer", a.get("name"));
				gdarr.put(gd);
			}
			wyJson.put("msg", gdarr);
			return redisDao.lpush("music", wyJson.toString());
		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public List<String> getListId(Long start, Long end) {
		return redisDao.get("music", start, end);
	}

	@Override
	public Map<Integer, String> lget(Long start, Long end) {
		return redisDao.lget("music", start, end);
	}

	@Override
	public Boolean delByIndex(Long index) {
		return redisDao.lrem("music", index);
	}

	@Override
	public Long size(String k) {
		return redisDao.size(k);
	}
}
