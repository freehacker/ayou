package com.ayou.music.dao.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.stereotype.Repository;
import org.springframework.util.Assert;

import com.ayou.core.cache.redis.AbstractBaseRedisDao;
import com.ayou.core.commons.json.JsonUtil;
import com.ayou.module.base.entity.BaseEntity;
import com.ayou.music.dao.RedisDao;

/**
 * @category 登录状态
 * @author AYOU
 * @version 2016年5月10日 下午10:29:46
 * @param <T>
 */
@Repository
public class RedisDaoImpl<T extends BaseEntity> extends AbstractBaseRedisDao<String, T> implements RedisDao<T> {
	protected final Logger logger = LoggerFactory.getLogger(this.getClass());

	/**
	 * 保存对象T
	 * 
	 * @param user
	 * @return
	 */
	public boolean add(final T t) {
		if (hasKey(t.getUuid())) {
			logger.info("key:" + t.getUuid() + " is already exists, will be replaced... ");
		}
		boolean result = redisTemplate.execute(new RedisCallback<Boolean>() {
			public Boolean doInRedis(RedisConnection connection) throws DataAccessException {
				RedisSerializer<String> serializer = getRedisSerializer();
				byte[] key = serializer.serialize(t.getUuid());
				byte[] value = serializer.serialize(JsonUtil.toJson(t));
				// return connection.setNX(key, name);
				try {
					connection.setEx(key, 60, value);
					return hasKey(t.getUuid());
				} catch (Exception e) {
					e.printStackTrace();
					return false;
				}
			}
		});
		return result;
	}

	/**
	 * 保存对象T 带时间
	 * 
	 * @param t
	 * @param time
	 * @return
	 */
	public boolean add(final T t, Long time) {
		if (hasKey(t.getUuid())) {
			logger.info("key:" + t.getUuid() + " is already exists, will be replaced... ");
		}
		boolean result = redisTemplate.execute(new RedisCallback<Boolean>() {
			public Boolean doInRedis(RedisConnection connection) throws DataAccessException {
				RedisSerializer<String> serializer = getRedisSerializer();
				byte[] key = serializer.serialize(t.getUuid());
				byte[] value = serializer.serialize(JsonUtil.toJson(t));
				// return connection.setNX(key, name);
				try {
					connection.setEx(key, time, value);
					if (hasKey(t.getUuid())) {
						return true;
					} else {
						return false;
					}
				} catch (Exception e) {
					e.printStackTrace();
					return false;
				}
			}
		});
		return result;
	}

	/**
	 * 新增 字符串
	 * 
	 * @param k
	 * @param v
	 * @param time
	 * @return
	 * @since 2016-07-13
	 */
	public boolean add(String k, String v) {
		if (hasKey(k)) {
			logger.info("key:" + k + " is already exists, will be replaced... ");
		}
		boolean result = redisTemplate.execute(new RedisCallback<Boolean>() {
			public Boolean doInRedis(RedisConnection connection) throws DataAccessException {
				RedisSerializer<String> serializer = getRedisSerializer();
				byte[] key = serializer.serialize(k);
				byte[] value = serializer.serialize(v);
				try {
					connection.setNX(key, value);
					return hasKey(k);
				} catch (Exception e) {
					e.printStackTrace();
					return false;
				}
			}
		});
		return result;
	}

	/**
	 * 新增 字符串 带时间
	 * 
	 * @param k
	 * @param v
	 * @param time
	 * @return
	 * @since 2016-07-13
	 */
	public boolean add(String k, String v, Long time) {
		if (hasKey(k)) {
			logger.info("key:" + k + " is already exists, will be replaced... ");
		}
		boolean result = redisTemplate.execute(new RedisCallback<Boolean>() {
			public Boolean doInRedis(RedisConnection connection) throws DataAccessException {
				RedisSerializer<String> serializer = getRedisSerializer();
				byte[] key = serializer.serialize(k);
				byte[] value = serializer.serialize(v);
				try {
					connection.setEx(key, time, value);
					return hasKey(k);
				} catch (Exception e) {
					e.printStackTrace();
					return false;
				}
			}
		});
		return result;
	}

	/**
	 * 批量新增 使用pipeline方式
	 * 
	 * @param list
	 * @return
	 */
	public boolean add(final List<T> list) {
		Assert.notEmpty(list);
		boolean result = redisTemplate.execute(new RedisCallback<Boolean>() {
			public Boolean doInRedis(RedisConnection connection) throws DataAccessException {
				RedisSerializer<String> serializer = getRedisSerializer();
				for (T t : list) {
					byte[] key = serializer.serialize(t.getUuid());
					byte[] name = serializer.serialize(JsonUtil.toJson(t));
					connection.setNX(key, name);
				}
				return true;
			}
		}, false, true);
		return result;
	}

	@Override
	public Long lpush(String k, String v) {
		return redisTemplate.execute(new RedisCallback<Long>() {
			public Long doInRedis(RedisConnection connection) throws DataAccessException {
				RedisSerializer<String> serializer = getRedisSerializer();
				byte[] key = serializer.serialize(k);
				byte[] value = serializer.serialize(v);
				try {
					// connection.multi();//开始事务
					connection.lPush(key, value);
					connection.save();// DB saved on disk (持久化 )
					// connection.exec();//提交事务
					return connection.lLen(key) - 1;
				} catch (Exception e) {
					e.printStackTrace();
					return null;
				}
			}
		});
	}

	/**
	 * 删除
	 * 
	 * @param key
	 */
	public boolean delete(String key) {
		try {
			List<String> list = new ArrayList<String>();
			list.add(key);
			delete(list);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * 删除多个
	 * 
	 * @param keys
	 */
	@SuppressWarnings("unchecked")
	public boolean delete(List<String> keys) {
		try {
			redisTemplate.delete((T) keys);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * 删除所有匹配的v
	 * 
	 * @param keys
	 */
	@Override
	public Boolean lrem(String k, String v) {
		return redisTemplate.execute(new RedisCallback<Boolean>() {
			public Boolean doInRedis(RedisConnection connection) throws DataAccessException {
				return lrem(k, 0L, v);
			}
		});
	}

	/**
	 * 删除count个匹配的v
	 * 
	 * @param keys
	 */
	@Override
	public Boolean lrem(String k, Long count, String v) {
		return redisTemplate.execute(new RedisCallback<Boolean>() {
			public Boolean doInRedis(RedisConnection connection) throws DataAccessException {
				RedisSerializer<String> serializer = getRedisSerializer();
				byte[] key = serializer.serialize(k);
				byte[] value = serializer.serialize(v);
				// count 0删除所有符合
				// n当n>0从左到右删除n个 n<0从右到左删除n个
				connection.lRem(key, count, value);
				logger.info("lrem->" + count + "," + k + "," + "," + v);
				return true;
			}
		});
	}

	/**
	 * 删除index匹配的v
	 * 
	 * @param keys
	 */
	@Override
	public Boolean lrem(String k, Long index) {
		return redisTemplate.execute(new RedisCallback<Boolean>() {
			public Boolean doInRedis(RedisConnection connection) throws DataAccessException {
				RedisSerializer<String> serializer = getRedisSerializer();
				String v = lget(k, index);
				byte[] key = serializer.serialize(k);
				byte[] value = serializer.serialize(v);
				connection.lRem(key, 0, value);
				logger.info("lrem->" + k + "," + "," + v);
				return true;
			}
		});
	}

	/**
	 * 修改
	 * 
	 * @param user
	 * @return
	 */
	public boolean update(final T t) {
		String key = t.getUuid();
		if (!hasKey(key)) {
			logger.info("key:" + t.getUuid() + " is not exists... ");
			// throw new NullPointerException("key not exists..., key = " +
			// key);
		}
		boolean result = redisTemplate.execute(new RedisCallback<Boolean>() {
			public Boolean doInRedis(RedisConnection connection) throws DataAccessException {
				RedisSerializer<String> serializer = getRedisSerializer();
				byte[] key = serializer.serialize(t.getUuid());
				byte[] value = serializer.serialize(JsonUtil.toJson(t));
				byte[] is = connection.get(key);
				if (is == null) {
					return null;
				} else {
					try {
						connection.setEx(key, 60, value);
						return true;
					} catch (Exception e) {
						e.printStackTrace();
						return false;
					}
				}
			}
		});
		return result;
	}

	/**
	 * 通过key获取 对象<br>
	 * 
	 * @param keyId
	 * @return
	 * @return
	 */
	public T get(final String keyId, T t) {
		T t1 = redisTemplate.execute(new RedisCallback<T>() {
			@SuppressWarnings("unchecked")
			public T doInRedis(RedisConnection connection) throws DataAccessException {
				RedisSerializer<String> serializer = getRedisSerializer();
				byte[] key = serializer.serialize(keyId);
				byte[] value = connection.get(key);
				if (value == null) {
					return null;
				}
				String u = serializer.deserialize(value);
				logger.info("getKey:" + keyId + " --- " + u);
				return (T) JsonUtil.fromJson(u, t.getClass());
			}
		});
		return t1;
	}

	/**
	 * 通过key获取 字符串<br>
	 * 
	 * @param keyId
	 * @return
	 * @return
	 */
	public String get(String keyId) {
		String str = redisTemplate.execute(new RedisCallback<String>() {
			public String doInRedis(RedisConnection connection) throws DataAccessException {
				RedisSerializer<String> serializer = getRedisSerializer();
				byte[] key = serializer.serialize(keyId);
				byte[] value = connection.get(key);
				if (value == null) {
					return null;
				}
				String reStr = serializer.deserialize(value);
				logger.info("getKey:" + keyId + " --- " + reStr);
				return reStr;
			}
		});
		return str;
	}

	/**
	 * 通过key获取
	 * 
	 * @param keyId
	 * @return
	 * @return
	 */
	public List<String> get(String name, Long start, Long end) {
		ArrayList<String> listS = new ArrayList<String>();
		return redisTemplate.execute(new RedisCallback<List<String>>() {
			public List<String> doInRedis(RedisConnection connection) throws DataAccessException {
				RedisSerializer<String> serializer = getRedisSerializer();
				byte[] key = serializer.serialize(name);
				List<byte[]> value = connection.lRange(key, start, end);
				if (value.isEmpty()) {
					// logger.info("集合为空" + value.toString());
					return null;
				} else {
					// logger.info("集合不为空！" + value.toString());
					Iterator<byte[]> iter = value.iterator();
					while (iter.hasNext()) {
						listS.add(serializer.deserialize(iter.next()));
					}
				}
				return listS;
			}
		});
	}

	// 通过index 获取Str
	@Override
	public String lget(String key, Long index) {
		String str = redisTemplate.execute(new RedisCallback<String>() {
			public String doInRedis(RedisConnection connection) throws DataAccessException {
				RedisSerializer<String> serializer = getRedisSerializer();
				byte[] keyV = serializer.serialize(key);
				byte[] value = connection.lIndex(keyV, index);
				if (value == null) {
					return null;
				}
				String reStr = serializer.deserialize(value);
				logger.info("lget:" + key + "->" + reStr);
				return reStr;
			}
		});
		return str;
	}

	/**
	 * 通过name 获取map
	 * 
	 * @param keyId
	 * @return
	 * @return
	 */
	public Map<Integer, String> lget(String name, Long start, Long end) {
		Map<Integer, String> map = new HashMap<>();
		return redisTemplate.execute(new RedisCallback<Map<Integer, String>>() {
			public Map<Integer, String> doInRedis(RedisConnection connection) throws DataAccessException {
				RedisSerializer<String> serializer = getRedisSerializer();
				byte[] key = serializer.serialize(name);
				List<byte[]> value = connection.lRange(key, start, end);
				if (value.isEmpty()) {
					logger.info("lget->isEmpty!");
					return null;
				} else {
					Iterator<byte[]> iter = value.iterator();
					Integer index = new Long(start).intValue();
					while (iter.hasNext()) {
						String vl = serializer.deserialize(iter.next());
						map.put(index, vl);
						// logger.info("map->" + index + ":" + vl);
						index++;
					}
				}
				return map;
			}
		});
	}

	public boolean hasKey(final String uuid) {
		boolean is = redisTemplate.execute(new RedisCallback<Boolean>() {
			public Boolean doInRedis(RedisConnection connection) throws DataAccessException {
				RedisSerializer<String> serializer = getRedisSerializer();
				byte[] key = serializer.serialize(uuid);
				byte[] value = connection.get(key);
				if (value == null) {
					return false;
				} else {
					return true;
				}
			}
		});
		return is;
	}

	/**
	 * 返回list长度
	 * 
	 * @param k
	 * @return
	 */
	@Override
	public Long size(final String k) {
		return redisTemplate.execute(new RedisCallback<Long>() {
			public Long doInRedis(RedisConnection connection) throws DataAccessException {
				RedisSerializer<String> serializer = getRedisSerializer();
				byte[] key = serializer.serialize(k);
				// return connection.bitCount(key);
				return connection.lLen(key);
			}
		});
	}
}