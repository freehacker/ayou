package com.ayou.music.dao;

import java.util.List;
import java.util.Map;

import com.ayou.music.entity.MusicEntity;
import com.ayou.music.param.MusicQueryParams;


/**
 * @author AYOU
 * @version 2016年3月30日 下午10:05:21
 */
public interface MusicDao {

	public List<Map<String, Object>> list(MusicQueryParams p);

	public MusicEntity view();

}
