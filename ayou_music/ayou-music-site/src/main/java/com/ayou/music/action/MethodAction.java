package com.ayou.music.action;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @category HTTP方法测试
 * @version 1.0
 * @author AYOU
 * @date 2016年8月4日 下午3:42:47
 */
@RestController()
@RequestMapping("/mt")
public class MethodAction {
	Logger logger = LoggerFactory.getLogger(getClass());

	@RequestMapping(value = "/m", produces = { "text/json;charset=UTF-8" }, method = RequestMethod.GET)
	@ResponseBody
	public String get(ModelMap map, HttpServletRequest request) {
		logger.info("GET成功！");
		return "GET成功！";
	}

	@RequestMapping(value = "/m", produces = { "text/json;charset=UTF-8" }, method = RequestMethod.PUT)
	@ResponseBody
	public String put(ModelMap map, HttpServletRequest request) {
		logger.info("哈哈哈哈哈哈哈，进入了PUT方法！");
		return "哈哈哈哈哈哈哈，进入了PUT方法！";
	}

	@RequestMapping(value = "/m", produces = { "text/json;charset=UTF-8" }, method = RequestMethod.DELETE)
	public String del(ModelMap map, HttpServletRequest request) {
		logger.info("哈哈哈哈哈哈哈，进入了DELETE方法。。。了不起");
		return "哈哈哈哈哈哈哈，进入了DELETE方法。。。了不起";
	}

	@RequestMapping(value = "/m", produces = { "text/json;charset=UTF-8" }, method = RequestMethod.OPTIONS)
	public String options(ModelMap map, HttpServletRequest request) {
		logger.info("哈哈哈哈哈哈哈，进入了OPTIONS，好高端啊。。。");
		return "哈哈哈哈哈哈哈，进入了OPTIONS，好高端啊。。。";
	}

	@RequestMapping(value = "/m", produces = { "text/json;charset=UTF-8" }, method = RequestMethod.TRACE)
	public String trace(ModelMap map, HttpServletRequest request) {
		logger.info("哈哈哈哈哈哈哈，进入了TRACE，TRACETRACETRACETRACETRACE。。。");
		return "哈哈哈哈哈哈哈，进入了TRACE，TRACETRACETRACETRACETRACE。。。";
	}

	@RequestMapping(value = "/del", produces = { "text/json;charset=UTF-8" }, method = RequestMethod.PATCH)
	public String patch(ModelMap map, HttpServletRequest request) {
		logger.info("哈哈哈哈哈哈哈，进入了PATCHPATCHPATCHPATCHPATCHPATCHPATCHPATCHPATCHPATCHPATCHPATCHPATCHPATCHPATCHPATCHPATCHPATCHPATCHPATCHPATCHPATCHPATCHPATCHPATCHPATCH");
		return "哈哈哈哈哈哈哈，进入了PATCHPATCHPATCHPATCHPATCHPATCHPATCHPATCHPATCHPATCHPATCHPATCHPATCHPATCHPATCHPATCHPATCHPATCHPATCHPATCHPATCHPATCHPATCHPATCHPATCHPATCH";
	}
}
