package com.ayou.music.dao;

import java.util.List;
import java.util.Map;

import com.ayou.module.base.entity.BaseEntity;

/**
 * @category 登录状态
 * @author AYOU
 * @version 2016年5月10日 下午10:29:32
 * @param <T>
 */
public interface RedisDao<T extends BaseEntity> {

	/**
	 * 新增
	 * 
	 * @param user
	 * @return
	 */
	boolean add(T t);

	/**
	 * 批量新增 使用pipeline方式
	 * 
	 * @param list
	 * @return
	 */
	boolean add(List<T> list);
	
	boolean add(String k, String v);
	
	boolean add(String k, String v, Long time);
	
	Long lpush(String k, String v);

	/**
	 * 删除
	 * 
	 * @param key
	 */
	boolean delete(String key);

	/**
	 * 删除多个
	 * 
	 * @param keys
	 */
	boolean delete(List<String> keys);
	
	Boolean lrem(String k, Long count, String v);

	Boolean lrem(String k, Long index);

	Boolean lrem(String k, String v);
	/**
	 * 修改
	 * 
	 * @param use
	 * @return
	 */
	boolean update(T t);

	/**
	 * 通过key获取T
	 * 
	 * @param keyId
	 * @return
	 */
	T get(String keyId ,T t);
	
	/**
	 * 通过key获取字符串
	 * 
	 * @param keyId
	 * @return
	 */
	String get(String keyId);
	
	/**
	 * 分页
	 * 
	 * @param keyId
	 * @return
	 */
	public List<String> get(String name, Long start, Long end);
	
	public Map<Integer, String> lget(String name, Long start, Long end);
	
	public String lget(String name, Long index);

	Long size(String k);
	
}
