package com.ayou.music.service.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Socket;

import org.springframework.stereotype.Service;

import com.ayou.core.commons.utils.socket.SocketUtil;
import com.ayou.music.service.WYConstant;
import com.ayou.music.service.WYService;


/**
 * @author AYOU
 * @version 2016年3月30日 下午10:46:45
 */

@Service
public class WYServiceImpl implements WYService {

	public String oneSongStrByID(String... ids) {
		return oneSongStrByID(WYConstant.BASE_DOMAIN_URL, WYConstant.PORT_HTTP, WYConstant.SONGURL_ID, WYConstant.POST_GET, ids);
	}

	public String oneSongStrByName(String name) {
		return oneSongStrByName(WYConstant.BASE_DOMAIN_URL, WYConstant.PORT_HTTP, WYConstant.URL_BEGIN, WYConstant.URL_END, name, WYConstant.POST_GET);
	}

	public String collectStrByID(String id) {
		return collectStrByID(WYConstant.BASE_S_URL, WYConstant.PLAY_LIST_ID, WYConstant.PORT_HTTP, id,WYConstant.POST_GET);
	}

	public String oneSongStrByName(String url, int port, String bUrl, String eUrl, String name, String post) {
		InputStream is = null;
		String songStr = "";
		Socket socket = SocketUtil.getSocket(url, port);
		try {
			OutputStream out = socket.getOutputStream();
			out.write((post + bUrl + name + eUrl).getBytes());
			// System.out.println(post + bUrl + name + eUrl);
			out.write("\r\n".getBytes());
			out.flush();
			is = socket.getInputStream();
			BufferedReader bf = new BufferedReader(new InputStreamReader(is, "utf-8"));
			if (bf != null) {
				songStr = bf.readLine();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		return songStr;
	}

	public String oneSongStrByID(String url, int port, String sUrl, String post, String... ids) {
		InputStream is = null;
		String songStr = "";
		Socket socket = SocketUtil.getSocket(url, port);
		try {
			OutputStream out = socket.getOutputStream();
			out.write((post + sUrl + ids).getBytes());
			// System.out.println(post + sUrl + id);
			out.write("\r\n".getBytes());
			out.flush();
			is = socket.getInputStream();
			BufferedReader bf = new BufferedReader(new InputStreamReader(is, "utf-8"));
			if (bf != null) {
				songStr = bf.readLine();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return songStr;
	}

	public String collectStrByID(String url, String sUrl, int port, String id, String post) {
		InputStream is = null;
		String songStr = "";
		Socket socket = SocketUtil.getSocket(url, port);
		try {
			OutputStream out = socket.getOutputStream();
			out.write((post + sUrl + id).getBytes());
			out.write("\r\n".getBytes());
			out.flush();
			is = socket.getInputStream();
			BufferedReader bf = new BufferedReader(new InputStreamReader(is, "utf-8"));
			if (bf != null) {
				songStr = bf.readLine();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return songStr;
	}

}
