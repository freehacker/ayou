package com.ayou.music.action;

import java.io.File;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ayou.music.entity.MusicEntity;
import com.ayou.music.entity.Tools;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

/**
 * @category xml到对象的转换
 * @version 1.0
 * @author AYOU
 * @date 2016年7月14日 下午2:27:56
 */
public class Test {
	Logger logger = LoggerFactory.getLogger(getClass());

	public void ofxml() {
		File f = new File("E:/JAVA/ayou/ayou_music/ayou-music-site/src/main/java/com/ayou/music/action/music.xml");

		XStream xStream = new XStream(new DomDriver());
		xStream.processAnnotations(MusicEntity.class);

		MusicEntity m = (MusicEntity) xStream.fromXML(f);
		logger.info(m.getName());
		logger.info(m.getLike().toString());
		logger.info(m.getTools().getColor());
		logger.info(m.getTools().getTest().get(0).getName());
		logger.info(m.getTools().getTest().get(1).getName());
	}

	public void ofxmlo() {
		File f = new File("E:/JAVA/ayou/ayou_music/ayou-music-site/src/main/java/com/ayou/music/action/tools.xml");

		XStream xStream = new XStream(new DomDriver());
		xStream.processAnnotations(Tools.class);

		Tools t = (Tools) xStream.fromXML(f);
		logger.info(t.getColor());
		logger.info(t.getTest().get(0).getName());
		logger.info(t.getTests().size() + "");
	}
}
