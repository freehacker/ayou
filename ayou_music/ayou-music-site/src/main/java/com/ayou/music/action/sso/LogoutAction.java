package com.ayou.music.action.sso;

import javax.annotation.Resource;

import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ayou.module.identity.service.IdentityService;
import com.baomidou.kisso.SSOConfig;
import com.baomidou.kisso.SSOHelper;
import com.baomidou.kisso.Token;
import com.baomidou.kisso.common.SSOProperties;

/**
 * @category 退出登录
 */
@RestController
@RequestMapping("/sso")
public class LogoutAction extends BaseAction {
	@Resource
	private IdentityService identityService;
	/**
	 * 退出所有系统
	 */
	@RequestMapping("/logout")
	public String logout(ModelMap map) {
		try {
			Token token = SSOHelper.getToken(request);
			SSOProperties prop = SSOConfig.getSSOProperties();
			if (null != token.getUid()) {
				identityService.LogOut(token.getUid());
			}
			map.put("r_url", prop.get("sso.logout.url"));
			return renderJsonMsg(map, "ok");
		} catch (Exception e) {
			return renderJsonError(map, "err", e);
		} finally {
			SSOHelper.clearLogin(request, response);
		}
	}

	/**
	 * 退出当前系统
	 */
	@RequestMapping("/out")
	public String logsso(ModelMap map) {
		try {
			SSOHelper.clearLogin(request, response);
			return renderJsonMsg(map, "ok");
		} catch (Exception e) {
			e.printStackTrace();
			return renderJsonError(map, "退出失败", e);
		}finally {
			SSOHelper.clearLogin(request, response);
		}
		//return redirectTo("/login.html");
	}
}
