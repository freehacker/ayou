package com.ayou.music.action;

import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ayou.core.commons.utils.JsonUtil;
import com.ayou.music.param.MusicQueryParams;
import com.ayou.music.service.MusicService;
import com.ayou.site.base.action.SimpleActionSupport;

/**
 * @author AYOU
 * @version 2016年3月30日 下午11:14:31
 * @category 音乐接口
 */

@RestController
@RequestMapping("/music")
public class MusicAction extends SimpleActionSupport {

    @Resource
    MusicService musicService;

    @RequestMapping(value = "/collects", produces = {"text/json;charset=UTF-8"})
    public String getMusicByCollects(ModelMap map, HttpServletRequest request, MusicQueryParams p) {
        try {
            return JsonUtil.toJson(musicService.getMusicByCollects(p));
        } catch (Exception e) {
            return renderJsonError(map, "异常", e);
        }
    }

    @RequestMapping(value = "/colid/{index}", produces = {"text/json;charset=UTF-8"}, method = RequestMethod.GET)
    public String getMusicByIndex(ModelMap map, HttpServletRequest request, @PathVariable("index") Long index) {
        if (StringUtils.isBlank(index.toString())) {
            return "index is blank!";
        }
        try {
            return musicService.lbyIdStr(index);
        } catch (Exception e) {
            return renderJsonError(map, "异常", e);
        }
    }

    @RequestMapping(value = "/colid/{index}", produces = {"text/json;charset=UTF-8"}, method = RequestMethod.DELETE)
    public String delMusicByIndex(ModelMap map, HttpServletRequest request, @PathVariable("index") Long index) {
        if (StringUtils.isBlank(index.toString())) {
            return "index is blank!";
        }
        try {
            return musicService.delByIndex(index).toString();
        } catch (Exception e) {
            return renderJsonError(map, "异常", e);
        }
    }

    @RequestMapping(value = "/colid/{id}", produces = {"text/json;charset=UTF-8"}, method = RequestMethod.POST)
    public String addMusicByID(ModelMap map, HttpServletRequest request, @PathVariable("id") String id) {
        if (StringUtils.isBlank(id)) {
            return "id is blank!";
        }
        try {
            return musicService.laddIdStr(id).toString();
        } catch (Exception e) {
            return renderJsonError(map, "异常", e);
        }
    }

    @RequestMapping(value = "/colid/list", produces = {"text/json;charset=UTF-8"})
    public String listById(ModelMap map, HttpServletRequest request, Long start, Long end) {
        if (StringUtils.isBlank(start.toString()) && StringUtils.isBlank(end.toString())) {
            return "start and end is not blank!";
        }
        if ((end - start) > 501) {
            return "wocao!";
        }
        try {
            Map<Integer, String> mapStr = musicService.lget(start, end);
            /*
			 * Iterator<?> it = mapStr.entrySet().iterator(); while
			 * (it.hasNext()) {
			 * 
			 * @SuppressWarnings("rawtypes") Map.Entry entry = (Map.Entry)
			 * it.next(); Object key = entry.getKey(); Object value =
			 * entry.getValue(); logger.info("key=" + key + " value=" + value);
			 * }
			 */
            map.put("result", mapStr);
            map.put("count", musicService.size("music"));
            return renderJsonMsg(map, "ok");

        } catch (Exception e) {
            return renderJsonError(map, "异常", e);
        }
    }
}
