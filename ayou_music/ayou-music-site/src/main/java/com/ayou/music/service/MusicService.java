package com.ayou.music.service;

import java.util.List;
import java.util.Map;

import com.ayou.music.entity.MusicEntity;
import com.ayou.music.param.MusicQueryParams;

/**
 * @author AYOU
 * @version 2016年3月30日 下午10:21:50
 */
public interface MusicService {

	public List<MusicEntity> list(MusicQueryParams p);

	public Map<String, Object> getMusicByCollects(MusicQueryParams p);

	public String collectStrByID(String id);

	/**
	 * 首先从redis里面找没有再调socket
	 * 
	 * @param id
	 * @return
	 */
	public String byIdStr(String id);

	public String lbyIdStr(Long index);

	/**
	 * 添加WYServic返回的字符串到redis
	 * 
	 * @param id
	 * @return
	 */
	public Boolean addIdStr(String id);

	public Long laddIdStr(String id);

	/**
	 * @param id
	 * @return
	 */
	public List<String> getListId(Long start, Long end);

	public Map<Integer, String> lget(Long start, Long end);

	public Boolean delByIndex(Long index);

	Long size(String k);

}
