package com.ayou.music.action.sso;

import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.kisso.AuthToken;
import com.baomidou.kisso.SSOConfig;
import com.baomidou.kisso.SSOHelper;
import com.baomidou.kisso.common.SSOProperties;

/**
 * @category 代理登录，跨域中间状态处理
 */
@RestController
@RequestMapping("/sso")
public class ProxyLoginAction extends BaseAction {
	/**
	 * 跨域登录
	 */
	@RequestMapping("/sso_login")
	public String proxylogin(ModelMap map) {
		/*Properties pps = new Properties();
		try {
			//InputStream in = new BufferedInputStream(new FileInputStream("E:/JAVA/ayou/ayou_user/ayou-user-site/src/main/resources/properties/sso.properties"));
			InputStream in = this.getClass().getClassLoader().getResourceAsStream("sso.properties");
			pps.load(in);
			pps.getProperty("sso.defined.my_private_key");
		} catch (IOException e) {
			e.printStackTrace();
		}*/
		/**
		 * 用户自定义配置获取, 由于不确定性，kisso 提倡，用户自己定义配置。
		 */
		SSOProperties prop = SSOConfig.getSSOProperties();

		// 业务系统私钥签名 authToken 自动设置临时会话 cookie 授权后自动销毁
		//AuthToken at = SSOHelper.askCiphertext(request, response, prop.get("sso.defined.user_private_key"));
		AuthToken at = SSOHelper.askCiphertext(request, response, prop.get("sso.defined.music_private_key"));

		// at.getUuid() 作为 key 设置 authToken 至分布式缓存中，然后 sso 系统二次验证

		// askurl 询问 sso 是否登录地址
		map.put("askurl", prop.get("sso.defined.askurl"));

		// askTxt 询问 token 密文
		map.put("askData", at.encryptAuthToken());

		// my 确定是否登录地址
		map.put("okurl", prop.get("sso.defined.oklogin"));

		return renderJsonMsg(map, "ok");
	}

}