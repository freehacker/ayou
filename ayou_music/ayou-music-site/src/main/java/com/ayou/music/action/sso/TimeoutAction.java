package com.ayou.music.action.sso;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class TimeoutAction extends BaseAction {

	/**
	 * 跨域登录超时
	 */
	@RequestMapping("/timeout")
	public String timeout() {
		return "timeout";
	}
}