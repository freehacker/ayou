package com.ayou.music.param;

import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.ayou.core.commons.param.SqlQueryParams;


/**
 * @author AYOU
 * @version 2016年3月30日 下午9:25:20
 */
public class MusicQueryParams extends SqlQueryParams {
	private static final long serialVersionUID = -7888497987717650542L;
	
	private String name;
	private String description;
	private String title;
	private Integer like;
	private Integer listen;
	private Integer user_id;
	private Integer code;
	private Integer type;
	private Integer status;
	private Integer limit_start;
	private Integer limit_counts;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Integer getLike() {
		return like;
	}

	public void setLike(Integer like) {
		this.like = like;
	}

	public Integer getListen() {
		return listen;
	}

	public void setListen(Integer listen) {
		this.listen = listen;
	}

	public Integer getUser_id() {
		return user_id;
	}

	public void setUser_id(Integer user_id) {
		this.user_id = user_id;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Integer getLimit_start() {
		return limit_start;
	}

	public void setLimit_start(Integer limit_start) {
		this.limit_start = limit_start;
	}

	public Integer getLimit_counts() {
		return limit_counts;
	}

	public void setLimit_counts(Integer limit_counts) {
		this.limit_counts = limit_counts;
	}

	@Override
	public String buildSql(List<Object> params) {
		String sql = "SELECT m.id,m.UUID,m.createDate,m.name,m.description,m.title,m.like,m.listen,m.from,m.user_id,m.image,m.code FROM music m INNER JOIN (SELECT id FROM music ORDER BY createDate LIMIT ?, ?) AS page USING(id) WHERE STATUS=1";
		
		if(null != getLimit_start()){
			params.add(getLimit_start()*getLimit_counts());
		}else{
			params.add(0);
		}
		if(null != getLimit_counts()){
			params.add(getLimit_counts());
		}else {
			params.add(10);
		}
		
		if(StringUtils.isNotBlank(getName())){
			sql+=" and name=?";
			params.add(getName());
		}
		if(StringUtils.isNotBlank(getDescription())){
			sql+=" and description=?";
			params.add(getDescription());
		}
		if(StringUtils.isNotBlank(getTitle())){
			sql+=" and title=?";
			params.add(getTitle());
		}
		if(null!=getLike()){
			sql+=" and like=?";
			params.add(getLike());
		}
		if(null!=getListen()){
			sql+=" and listen=?";
			params.add(getListen());
		}
		if(null!=getUser_id()){
			sql+=" and user_id=?";
			params.add(getUser_id());
		}
		if(null!=getCode()){
			sql+=" and code=?";
			params.add(getCode());
		}
		if(null!=getStatus()){
			sql+=" and status=?";
			params.add(getStatus());
		}
		if(null!=getType()){
			sql+=" and type=?";
			params.add(getType());
		}
		
		return sql;
	}
}
