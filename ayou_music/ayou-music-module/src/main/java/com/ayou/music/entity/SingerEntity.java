package com.ayou.music.entity;

import com.ayou.module.base.entity.BaseEntity;

/**
 * @category 歌手
 * @version 1.0
 * @author AYOU
 * @date 2016年7月27日 上午9:23:52
 */
public class SingerEntity extends BaseEntity{
	private static final long serialVersionUID = 2998702744398521028L;
	private String name;

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
