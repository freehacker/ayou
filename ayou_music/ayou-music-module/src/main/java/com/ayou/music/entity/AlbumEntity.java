package com.ayou.music.entity;

import com.ayou.module.base.entity.BaseEntity;

/**
 * @category 专辑
 * @version 1.0
 * @author AYOU
 * @date 2016年7月27日 上午9:21:46
 */
public class AlbumEntity extends BaseEntity {
	private static final long serialVersionUID = 4796788847754962964L;
	private String name;

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
