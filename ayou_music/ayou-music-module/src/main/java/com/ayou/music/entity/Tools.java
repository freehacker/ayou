package com.ayou.music.entity;

import java.util.List;
import java.util.Set;

import com.ayou.module.base.entity.BaseEntity;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

/**
 * @category Tools.java
 * @version 1.0
 * @author AYOU
 * @date 2016年7月7日 下午4:23:16
 */
public class Tools extends BaseEntity {
	private static final long serialVersionUID = -2589156268344710770L;
	private String big;
	private String color;
	private String fit;
	
	@XStreamImplicit(itemFieldName = "test")
	private List<Test> test;
	
	@XStreamImplicit(itemFieldName = "tests")
	private Set<Test> tests;
	
	public String getBig() {
		return big;
	}
	public void setBig(String big) {
		this.big = big;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public String getFit() {
		return fit;
	}
	public void setFit(String fit) {
		this.fit = fit;
	}
	public List<Test> getTest() {
		return test;
	}
	public void setTest(List<Test> test) {
		this.test = test;
	}
	public Set<Test> getTests() {
		return tests;
	}
	public void setTests(Set<Test> tests) {
		this.tests = tests;
	}
}
