package com.ayou.music.entity;

import java.util.List;

import com.ayou.module.base.entity.BaseEntity;

/**
 * @category WYEntity.java
 * @version 1.0
 * @author AYOU
 * @date 2016年8月1日 下午2:30:49
 */
public class WYEntity extends BaseEntity{
	private static final long serialVersionUID = -7839017964455642977L;
	private String wyid;
	private String name;
	private String description;
	private List<TrackEntity> msg;
	public String getWyid() {
		return wyid;
	}
	public void setWyid(String wyid) {
		this.wyid = wyid;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public List<TrackEntity> getMsg() {
		return msg;
	}
	public void setMsg(List<TrackEntity> msg) {
		this.msg = msg;
	}
	
	
}
