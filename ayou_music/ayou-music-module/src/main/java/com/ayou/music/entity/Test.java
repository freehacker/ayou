package com.ayou.music.entity;

import com.ayou.module.base.entity.BaseEntity;

/**
 * @category Test.java
 * @version 1.0
 * @author AYOU
 * @date 2016年7月7日 下午4:55:33
 */
public class Test extends BaseEntity {
	private static final long serialVersionUID = 6423690201892522021L;
	private String name;
	private String code;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
}
