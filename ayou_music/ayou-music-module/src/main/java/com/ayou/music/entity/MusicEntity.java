package com.ayou.music.entity;

import com.ayou.core.commons.annotation.DBExclude;
import com.ayou.module.base.entity.BaseEntity;

/**
 * @author AYOU
 * @version 2016年3月30日 下午9:02:36
 */
public class MusicEntity extends BaseEntity {
	@DBExclude
	private static final long serialVersionUID = 1066148257576277442L;

	private String name;
	private String description;
	private String title;
	private Integer like;
	private Integer listen;
	private String from;
	private String image;
	private Integer user_id;
	private Integer code;
	private Integer type;
	private Integer status;
	private Tools tools;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Integer getLike() {
		return like;
	}

	public void setLike(Integer like) {
		this.like = like;
	}

	public Integer getListen() {
		return listen;
	}

	public void setListen(Integer listen) {
		this.listen = listen;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public Integer getUser_id() {
		return user_id;
	}

	public void setUser_id(Integer user_id) {
		this.user_id = user_id;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Tools getTools() {
		return tools;
	}

	public void setTools(Tools tools) {
		this.tools = tools;
	}
}
