package com.ayou.music.entity;

/**
 * @category TrackEntity.java
 * @version 1.0
 * @author AYOU
 * @date 2016年8月1日 下午3:07:37
 */
public class TrackEntity {
	private String albumName;
	private String albumType;
	private String albumBlurPicUrl;
	
	private String trackName;
	private String trackMp3Url;
	
	private String artistName;
	private String singer;
	public String getAlbumName() {
		return albumName;
	}
	public void setAlbumName(String albumName) {
		this.albumName = albumName;
	}
	public String getAlbumType() {
		return albumType;
	}
	public void setAlbumType(String albumType) {
		this.albumType = albumType;
	}
	public String getAlbumBlurPicUrl() {
		return albumBlurPicUrl;
	}
	public void setAlbumBlurPicUrl(String albumBlurPicUrl) {
		this.albumBlurPicUrl = albumBlurPicUrl;
	}
	public String getTrackName() {
		return trackName;
	}
	public void setTrackName(String trackName) {
		this.trackName = trackName;
	}
	public String getTrackMp3Url() {
		return trackMp3Url;
	}
	public void setTrackMp3Url(String trackMp3Url) {
		this.trackMp3Url = trackMp3Url;
	}
	public String getArtistName() {
		return artistName;
	}
	public void setArtistName(String artistName) {
		this.artistName = artistName;
	}
	public String getSinger() {
		return singer;
	}
	public void setSinger(String singer) {
		this.singer = singer;
	}
}
