package com.ayou.music.bo;

import com.ayou.music.entity.AlbumEntity;
import com.ayou.music.entity.SingerEntity;

/**
 * @category Music.java
 * @version 1.0
 * @author AYOU
 * @date 2016年7月27日 上午9:32:01
 */
public class Music {
	
	private String name;
	private String description;
	private AlbumEntity albumEntity;
	private SingerEntity singerEntity;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public AlbumEntity getAlbumEntity() {
		return albumEntity;
	}
	public void setAlbumEntity(AlbumEntity albumEntity) {
		this.albumEntity = albumEntity;
	}
	public SingerEntity getSingerEntity() {
		return singerEntity;
	}
	public void setSingerEntity(SingerEntity singerEntity) {
		this.singerEntity = singerEntity;
	}
}
