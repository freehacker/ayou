define([ 'ayou','init'], function(ayou, $) {
	var URL = {
		base : '/music/colid/',
		list : '/music/colid/list',
		add : '',
		del : '',
		edit : '',
		bycolid : '/music/bycolid/',
	};

	var view = function(index, success) {
		var config = {
			url : URL.base + index,
			async : true,
			type : 'get',
			success : success
		};
		ayou.ajax(config);
	}

	var list = function(start, end, success) {
		var config = {
			url : URL.list,
			async : true,
			type : 'get',
			loading : 'self',
			data : {
				start : start,
				end : end,
			},
			success : success
		};
		ayou.ajax(config);
	}

	var add = function(id, success) {
		var config = {
			url : URL.base + id,
			type : 'post',
			success : success
		};
		ayou.ajax(config);
	}

	return {
		view : view,
		list : list,
		add : add,
	}
})