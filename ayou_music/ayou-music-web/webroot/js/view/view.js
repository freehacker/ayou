require([ "jquery", "avalon", "js/bs", "js/jaudio.min", "bootstrap", "css!/css/view.css", "domReady!" ], function($, avalon, bs) {
	var listVM = avalon.define({
		$id : "model",
		tbody : [],
		index : 0,
		model : {
			tracks : [],
		},
		listen : function($index) {
			alert($index);
		}
	});

	avalon.scan();

	(function() {
		var index = getParam('index');
		var t = {playlist : []}
		bs.view(index, function(data) {
			if (data) {
				listVM.model = data;
				$(document).attr("title",listVM.model.name+"-齐天都");
				$("#ml").css("background-image", "url(" + listVM.model.coverImgUrl + ")");
				$.each(data.tracks,function(i,e){
					var m ={};
					m.file = data.tracks[i].mp3Url;
					m.thumb = data.tracks[i].album.blurPicUrl;
					m.trackName = data.tracks[i].name;
					m.trackArtist = data.tracks[i].album.artists[0].name;
					m.trackAlbum = data.tracks[i].album.type;
					t.playlist.push(m);
				});
				
			}
		});
		$(".jAudio--player").jAudio(t);
	})();
});