require([ "jquery", "avalon", "js/bs","layer", "pjax", "bootstrap", "css!/css/view.css", "domReady!" ], function($, avalon, bs, layer) {
	var listVM = avalon.define({
		$id : "model",
		tbody : [],
		index : 0,
		model : {
			tracks : [],
		},
		listen : function($index) {
			alert($index);
		},
		L1:function(){
			$.pjax({
				url: '/music/colid/1',
			    container: '#container', //内容替换的容器
			    show: 'fade',  //展现的动画，支持默认和fade, 可以自定义动画方式，这里为自定义的function即可。
			    cache: true,  //是否使用缓存
			    storage: true,  //是否使用本地存储
			    titleSuffix: 'ss', //标题后缀
			    filter: function(){
			    	alert("filter");
			    },
			    callback: function(){
			    	alert("callback");
			    	console.log(data);
			    }
	        });
		},
		L2:function(){
			$.pjax({
				url: '/music/colid/2',
				replace:'/music/colid/sdfsdfsdfds',
			    container: '#container', //内容替换的容器
			    show: 'fade',  //展现的动画，支持默认和fade, 可以自定义动画方式，这里为自定义的function即可。
			    cache: true,  //是否使用缓存
			    storage: true,  //是否使用本地存储
			    titleSuffix: 'ss', //标题后缀
			    filter: function(data){
			    	alert(data);
			    },
			    callback: function(data){
			    	alert(data);
			    	console.log(data);
			    }
	        });
		}
	});

	var ayoup_media = document.getElementById('ayoup-audio');
	var ayoupM = avalon.define({
		$id : 'ayoupM',
		tbody : [],
		listShow : false,
		model : {
			ayoup_CT:'',//当前时间
			ayoup_AT:'',//总时间
		},
		tips:function(s){
			if(s.title==""){
				if(ayoup_media.paused){//暂停
					layer.tips("播放", s, {
						tips : [ 3, '#0FA6D8' ]
					});
				}else{
					layer.tips("暂停", s, {
						tips : [ 1, '#0FA6D8' ]
					});
				}
				return;
			}
			layer.tips(s.title, s, {
			  tips: [1, '#0FA6D8']
			});
		},
		ayoup_play : function() {//播放暂停
			if(ayoup_media.paused){
				ayoup_media.play();
				document.getElementById('ayoup_play').className = 'glyphicon glyphicon-pause';
				document.getElementById('ayoup-sigerimg').className = 'ayoup-sigerimg-play';
			}else{
				ayoup_media.pause();
				document.getElementById('ayoup_play').className = 'glyphicon glyphicon-play';
				document.getElementById('ayoup-sigerimg').className = 'ayoup-sigerimg-pause';
			}
		},
		ayoup_pre : function() {//上一首
			ayoup_media.play();
		},
		ayoup_backward:function(){//快退
			if(ayoup_media.currentTime >= ayoup_media.duration){
				document.getElementById('ayoup_play').className = 'glyphicon glyphicon-play';
				document.getElementById('ayoup-sigerimg').className = 'ayoup-sigerimg-pause';
				return;
			}
			ayoup_media.currentTime -= ayoup_media.duration*0.1;
		},
		ayoup_next : function() {//下一首
			alert(ayoup_media.duration);
		},
		ayoup_forward:function(){//快进
			if(ayoup_media.currentTime <= 0){
				document.getElementById('ayoup_play').className = 'glyphicon glyphicon-play';
				document.getElementById('ayoup-sigerimg').className = 'ayoup-sigerimg-pause';
				return;
			}
			ayoup_media.currentTime += ayoup_media.duration*0.1;
		},
		ayoup_list : function() {//列表
			if(ayoupM.listShow == true){
				ayoupM.listShow = false
			}else{
				ayoupM.listShow = true
			}
		},
	});
	
	setInterval(function() {//时间
        ayoupM.model.ayoup_CT = (ayoup_media.currentTime).toFixed(4);
        document.getElementById('ayoup_prog').value = ayoupM.model.ayoup_CT;
    }, 10);
	
	var playFinsh = setInterval(function() {
        if(ayoup_media.ended){//播放结束
        	document.getElementById('ayoup_play').className = 'glyphicon glyphicon-play';
			document.getElementById('ayoup-sigerimg').className = 'ayoup-sigerimg-pause';
			window.clearInterval(playFinsh);
        }
    }, 10);
	
	avalon.scan();
	
	var cgindex = 0;
	function cg(){
		cgindex+=3;
		console.log("wocao!"+cgindex);
	}
	
	(function() {
		var state="";
		for(var i=1;i<((1024*1024)*1+1)*0.99999999999999995;i++){
			state+='a';
			if(i==((1024*1024)*0.99999999999999995)){
				console.log(state);
				window.history.pushState(state, "", window.location.href);
			}
		}
		return;
		document.getElementById('ca').addEventListener('keyup', cg);
		
		ayoupM.model.ayoup_CT = (ayoup_media.currentTime).toFixed(4);
		ayoupM.model.ayoup_AT = (ayoup_media.duration).toFixed(4);
		document.getElementById('ayoup_prog').max = ayoupM.model.ayoup_AT;
			
		return;
		if (!$.support.pjax) {
			layer.msg('本网站不支持老的浏览器，<br>请使用最新的Firefox，Chrome，Edge...', {
				icon : 5
			});
		}
		
		//浏览器回退
		window.addEventListener('popstate', function(e) {
			if (e.state) {
				document.title = e.state.title;
				$("#container").html(e.state.html);
				//window.load = ; // 重新加载列表
			}
		});
		var index = getParam('index');
		var t = {playlist : []}
		bs.view(1, function(data) {
			if (data) {
				listVM.model = data;
				$(document).attr("title",listVM.model.name+"-齐天都");
				$("#ml").css("background-image", "url(" + listVM.model.coverImgUrl + ")");
				return;
				var href = window.location.href;
				$.each(data.tracks,function(i,e){
					var state = {
	                    url: href+"?index="+i,
	                    title: data.tracks[i].name,
	                    html: data.tracks[i].album.artists[0].name,
	                };
					window.history.pushState(state, $(data).filter("title").text(), href+"?index="+i);
				});
				
			}
		});
		//$(".jAudio--player").jAudio(t);
	})();
});