require([ "jquery", "avalon", 'layer', "js/bs", "bootstrap", "domReady!" ], function($, avalon, layer, bs) {
	var modelVM = avalon.define({
		$id : "model",
		tip : '',
		tipShow : false,
		model : {
			id : '',
		},
		add : function() {
			var id = modelVM.model.id;
			if (!id) {
				layer.msg("无ID，不音乐~", {icon : 0,});
				modelVM.tip = "无ID，不音乐~";
				modelVM.tipShow = true;
			}
			bs.add(id, function(data) {
				if (data) {
					layer.msg("添加成功~~好开心~~~，嗨起来~~~", {icon : 6,});
					modelVM.model.id = '';//清空
					layer.msg(id+"添加好了~", {
						offset : 'rb',
						shift : 6,
						icon : 1,
						time : 10000, //
						btn : [ '现在就去听~', '再来几个'],
						yes : function() {
							window.location.href = "/view/view.html?index=0";
							//window.location.href = "/view/view.html?index="+data;
						},
						btn2 : function() {
							layer.closeAll();
						},
						/*btn3 : function() {
							layer.msg("AYOU需要你这种人才~~~", {
								icon : 6,
							});
						}*/
					});
				}else{
					layer.msg("桑心~，可能没有这个歌单~~~", {icon : 5,});
				}
			});
		}
	});
	avalon.scan();
	function getLocalTime(nS) {
		return new Date(parseInt(nS) * 1000).toLocaleString().replace(/年|月/g, "-").replace(/日/g, " ");
	} 
	
	(function(){
		
		return;
		function isBigDateS(old, now){
			var o = new Date(old);//标准时间
			var n = new Date(now);//标准时间
			
			var oc = Date.parse(o);//时间戳
			var nc = Date.parse(n);//时间戳
			
			var oc365 = o.setDate(o.getDate() + 365);//过去的时间+365的时间戳
	
			var differ_c = nc - oc; //相差的时间戳
			var differ_365c = nc - oc365; //相差的365时间戳
			
			/*
			 * 推导
			 * nc - oc < 365;
			 * nc - oc - 365 < 365-365;
			 * nc - (oc+365) < 0;
			*/
			if(differ_365c < 0){//相差365,一年 ,算新投保
				return true;
			}
			if(differ_c < 0){//过去大于现在，类似单交强，单商业分开投
				return true;
			}
			return false;
		}
		//2016-07-14T00:00:00+08:00
		//1468857600000
		alert(isBigDateS('2017-07-21T00:00:00+08:00',1468857600000));
		
	})()
});