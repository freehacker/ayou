require([ "jquery", "avalon", "js/bs", "js/img", "bootstrap", "css!/css/index.css", "domReady!" ], function($, avalon, bs) {
	var listVM = avalon.define({
		$id : "model",
		tbody : [],
		index : 0,
		loadcss : false,
		model : {
			result : [],
		},
		listen : function($index) {
			window.open("/view/view.html?index=" + $index);
		}
	});

	avalon.scan();

	avalon.filters.Fd = function(de) {
		if (de && de.length > 21) {
			return de.substr(0, 20) + '...';
		}
		return de;
	};

	var stop = true;

	(function() {
		$(document).attr("title","MUSIC-齐天都");
		
		listVM.loadcss = true;
		bs.list(0, 20, function(data) {
			if (data && (data.success == true)) {
				listVM.model = data;
				$.each(data.result, function(i, e) {
					listVM.loadcss = false;
					var obj = eval('(' + data.result[i] + ')');
					listVM.tbody.push(obj);
					stop = false;
				});
				$('#da-thumbs > li').each(function() {
					$(this).hoverdir();
				});
			}
		});
	})();

	$(window).scroll(function() {
		totalheight = parseFloat($(window).height()) + parseFloat($(window).scrollTop());
		if ($(document).height() <= totalheight) {
			if (stop == false) {
				stop = true;
				listVM.loadcss = true;
				var start = listVM.tbody.length;
				bs.list(start, start + 13, function(data) {
					listVM.loadcss = false;
					if (data.result) {
						listVM.model = data;
						var len = 0;
						$.each(data.result, function(i, e) {
							var obj = eval('(' + data.result[i] + ')');
							listVM.tbody.push(obj);
							len++;
						});
						if (len == 14) {
							stop = false;
						}
						$('#da-thumbs > li').each(function() {
							$(this).hoverdir();
						});
					}
				});
			}
		}
	});
});