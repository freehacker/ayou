require([ "jquery", "avalon", "js/bs", "layer", "img", "jqueryui", "domReady!" ], function($, avalon, bs) {
	var index = getParam('index');
	var PL = [];
	var PL_NOW = 0;
	var P_PAGE="", L_PAGE="";//存放播放页和首页
	var listVM = avalon.define({
		$id : "listVM",
		show:true,
		tbody : [],
		index : 0,
		loadcss : false,
		model : {
			result : [],
		},
		listen:function(index){
			bs.view(index, function(data) {
				if (data) {
					allVM.listtmp="";
					allVM.listtmp="view/playlist.html";
					playVM.model={msg:[]};
					playVM.tbody=[];
					playVM.model = data;
					$(document).attr("title",playVM.model.name+"-七听都");
					$(".playerMain").css("background-image","url("+playVM.model.coverImgUrl+")");
				}
			});
			setTimeout(function() {//可能是avalon include指令处理时间问题
				var href = "http://music.qtdu.com/play.html?index=";
				var state = {
					url : href + index,
					title : playVM.model.name+"-七听都",
					//html : $("#statecontainer").html()
				};
				window.history.pushState(state, "", href + index);
				P_PAGE = $("#statecontainer").html();//存放播放页html
			}, 800)
		}
	});
	var playVM = avalon.define({
		$id : "playVM",
		show : true,
		tbody : [],
		index : 0,
		model : {
			msg : [],
		},
		listen : function(index) {
			PL.push(playVM.model.msg[index]);
			var src = PL[PL.length-1].trackMp3Url;
			PL_NOW = PL.length - 1;
			ayouPlay(src);
			$(".songName").html(PL[PL_NOW].trackName);
			$(".songPlayer").html(PL[PL_NOW].artistName);
		}
	});
	var allVM = avalon.define({
		$id : "list",
		listtmp : "view/indexlist.html"
	});
	
	$(function(){
		/*双击播放*/
		$(".songList").on("dblclick",function(){
			var sid = $(this).find(".start em").html();
			$(".start em[sonN="+sid+"]").click();
		});
		/*底部进度条控制*/
		$( ".dian" ).draggable({
			containment:".pro2",
			drag: function() {
				var le=parseInt($(".dian").css("left"));
				var pw = parseInt($(".pro2").width());
				audio.currentTime = audio.duration*(le/pw);
	      	}
		});
		/*音量控制*/
		$( ".dian2" ).draggable({
			containment:".volControl",
			drag: function() {
				var l=$(".dian2").css("left");
				var le = parseInt(l);
				audio.volume=(le/80);
				if(audio.volume>0.8){
					layer.msg('嗨起来吧~', {icon : 6});
				}
			}
		});
		/*底部播放按钮*/
		$(".playBtn").on("click",function(){
			var p = $(this).attr("isplay");
			if (p==0) {
				$(this).css("background-position","0 -30px");
				$(this).attr("isplay","1");
			};
			if (p==1) {
				$(this).css("background-position","");
				$(this).attr("isplay","0");
			};
			if (PL.length == 0 && playVM.model.msg.length > 1) {// 如果播放列表为空就把当前列表加进去
				PL = playVM.model.msg;
			}
			if(audio.paused){
				audio.play();
			}else{
				audio.pause();
			}
		});
		/*切歌*/
		$(".prevBtn").on("click",function(){
			PL_NOW += 1;
			if(PL_NOW>(PL.length-1)){//越界
				layer.msg('跳到最后一首', {icon : 6});
				PL_NOW = 0;
			}
			ayouPlay(PL[PL_NOW].trackMp3Url);
			$(".songName").html(PL[PL_NOW].trackName);
			$(".songPlayer").html(PL[PL_NOW].artistName);
		});
		$(".nextBtn").on("click",function(){
			PL_NOW -= 1;
			if (PL_NOW < 0) {// 越界
				layer.msg('自动循环播放', {icon : 6});
				PL_NOW = PL.length-1;
			}
			ayouPlay(PL[PL_NOW].trackMp3Url);
			$(".songName").html(PL[PL_NOW].trackName);
			$(".songPlayer").html(PL[PL_NOW].artistName);
		});
	});
	
	function ayouPlay(src){
		$("#audio").attr("src",src);
		var audio = document.getElementById("audio");//获得音频元素
		/*显示歌曲总长度*/
		if(audio.paused){audio.play();}
		else audio.pause();
        
		audio.addEventListener('timeupdate',updateProgress,false);
		audio.addEventListener('play',audioPlay,false);
		audio.addEventListener('pause',audioPause,false);
		audio.addEventListener('ended',audioEnded,false);
		audio.addEventListener('error',audioError,false);

		/*底部显示歌曲信息*/
	/*	var songName=$(this).parent().parent().find(".colsn").html();
		var singerName =$(this).parent().parent().find(".colcn").html();
		$(".blur").css("opacity","0");
		$(".blur").animate({opacity:"1"},1000);*/
	}

	/*时间计算*/
	function calcTime(time){
		var hour,minute,second;
		hour = String ( parseInt ( time / 3600 , 10 ));
		if (hour.length ==1 )   hour='0'+hour;
		minute=String(parseInt((time%3600)/60,10));
		if(minute.length==1) minute='0'+minute;
		second=String(parseInt(time%60,10));
		if(second.length==1)second='0'+second;
		return minute+":"+second;
	}
	/*进度条*/
	function updateProgress(ev){
		var songTime = calcTime(Math.floor(audio.duration));
		$(".duration").html(songTime);/*显示歌曲总长度*/
		var curTime = calcTime(Math.floor(audio.currentTime));/*显示歌曲当前时间*/
		$(".position").html(curTime);
		var progressWidth = $(".pro2").width();/*进度条*/
		var llef = progressWidth*(audio.currentTime/audio.duration)+"px";
		$(".dian").css("left",llef);
	}
	function audioPlay(ev){
		$(".iplay").css("background",'url("css/images/T1oHFEFwGeXXXYdLba-18-18.gif") 0 0');
		$(".playBtn").css("background-position","0 -30px");
		$(".playBtn").attr("isplay","1");
	}
	function audioPause(ev){
		$(".iplay").css("background","");
	}
	function audioEnded(ev){
		PL_NOW -= 1;
		if (PL_NOW < 0) {// 越界
			layer.msg('自动循环播放', {icon : 6});
			PL_NOW = PL.length-1;
		}
		ayouPlay(PL[PL_NOW].trackMp3Url);
		$(".songName").html(PL[PL_NOW].trackName);
		$(".songPlayer").html(PL[PL_NOW].artistName);
	}
	function audioError(ev){
		if(PL.length==1){
			layer.msg('歌曲加载失败，请重新添加歌曲~', {icon : 5});
		}else{
			layer.msg('歌曲加载失败，播放下一首~', {icon : 6});
			PL_NOW -= 1;
			ayouPlay(PL[PL_NOW].trackMp3Url);
			$(".songName").html(PL[PL_NOW].trackName);
			$(".songPlayer").html(PL[PL_NOW].artistName);
		}
	}
	
	/*加载更多*/
	function loadmore(){
		$("#loadcss").show(500);
		var start = listVM.tbody.length;
		bs.list(start, start + 7, function(data) {
			$("#loadcss").hide(500);
			var len = 0;
			if (data.result) {
				listVM.model = data;
				for(var i in data.result){
					listVM.tbody.push($.parseJSON(data.result[i]));
					len++;
				}
				//$("img.lazy").lazyload({effect: "slideDown", threshold :180});
				$('#da-thumbs > li').each(function() {
					$(this).hoverdir();
				});
			}
			if(len<8){
				$("#more").css("display","none");//没数据了
			}
		});
	}

	avalon.scan();
	
	//浏览器前进后退
	window.addEventListener('popstate', function(e) {
		if (e.state) {
			document.title = e.state.title;
			var haveIndex = e.state.url.indexOf('index');//判断是否包含index，减少dome操作
			if(haveIndex != -1){//播放页
				$("#statecontainer").html(P_PAGE);
				$('.start > em').each(function() {//播放列表事件重新绑定
					$(this).on("click",function(){
						playVM.listen($(this).attr("value"));
					});
				});
				$("#more").on("click",function(){//加载更多重新绑定
					loadmore();
			    });
			}else{
				$("#statecontainer").html(L_PAGE);
				$('#da-thumbs > li').each(function() {//首页事件重新绑定
					$(this).hoverdir();
					$(this).on("click",function(){
						listVM.listen($(this).find("input").val());
					});
				});
			}
		}
	});
	//中间半透明
	$("#tmd").on("click",function(){
		if($(".middle").css("background-color")=='rgb(255, 255, 255)'){
			$(".middle").css("background-color","rgba(254, 254, 254, 0.44)")
		}else{
			$(".middle").css("background-color","rgb(255, 255, 255)")
		}
	});
	
	avalon.filters.Fd = function(de) {
		if (de && de.length > 21) {
			return de.substr(0, 20) + '...';
		}
		return de;
	};
	function list(start, step) {
		bs.list(start, start + step, function(data) {
			if (data && (data.success == true)) {
				$("#more").css("display","none");//文字
				listVM.model = data;
				listVM.tbody=[];
				for(var i in data.result){
					listVM.tbody.push($.parseJSON(data.result[i]));
				}
				//$("img.lazy").lazyload({effect: "fadeIn", threshold :180});
				setTimeout(function(){
					$("#loadcss").css("display","none");//隐藏
					$("#more").css("display","");//文字
					$('#da-thumbs > li').each(function() {
						$(this).hoverdir();
					});
					$("#more").on("click",function(){//加载更多重新绑定
						loadmore();
				    });
					var state = {
						url : window.location.href,
						title : '七天都有音乐-七听都',
						//html : $("#statecontainer").html(),
					};
					window.history.pushState(state, $(data).filter("title").text(), window.location.href);
					L_PAGE = $("#statecontainer").html();
				},800)
				$(".playerMain").css("background-image","url("+listVM.tbody[0].coverImgUrl+")");
			};
			
		});
	}
    (function() {
    	if(index){//播放页
    		listVM.listen(index);
    	}else{//列表页
    		$("#loadcss").css("display","");
    		list(0,31);
    	}
    	/*默认黄金分割点音量*/
    	var audio = document.getElementById("audio");
    	audio.volume = 0.618;
    	$(".dian2").css("left","61.8%");
	})();
});