require([ "jquery", "avalon", "js/bs", "img", "bootstrap", "css!/css/index.css", "domReady!" ], function($, avalon, bs) {
	var listVM = avalon.define({
		$id : "model",
		tbody : [],
		index : 0,
		loadcss : false,
		model : {
			result : [],
		},
		listen : function($index) {
			window.open("/view/view.html?index=" + $index);
		}
	});

	avalon.scan();

	avalon.filters.Fd = function(de) {
		if (de && de.length > 21) {
			return de.substr(0, 20) + '...';
		}
		return de;
	};

	function list(start, step) {
		bs.list(start, start + step, function(data) {
			if (data && (data.success == true)) {
				listVM.model = data;
				$.each(data.result, function(i, e) {
					var obj = eval('(' + data.result[i] + ')');
					listVM.tbody.push(obj);
				});
			}
			;
			if (listVM.tbody.length == 28) {
				listVM.loadcss = false;
				stop = false;
				$('#da-thumbs > li').each(function() {
					$(this).hoverdir();
				});
			}
		});
	}

	var stop = true;
	(function() {
		$(document).attr("title", "MUSIC-齐天都");
		listVM.loadcss = true;
		function recursive(index) {
			if (index == 28) {
				return;
			}
			list(index, 1);
			index += 2;
			recursive(index);
		}
		recursive(0);
	})();

	$(window).scroll(function() {
		totalheight = parseFloat($(window).height()) + parseFloat($(window).scrollTop());
		if ($(document).height() <= totalheight) {
			if (stop == false) {
				stop = true;
				listVM.loadcss = true;
				var start = listVM.tbody.length;
				bs.list(start, start + 6, function(data) {
					listVM.loadcss = false;
					if (data.result) {
						listVM.model = data;
						var len = 0;
						$.each(data.result, function(i, e) {
							var obj = eval('(' + data.result[i] + ')');
							listVM.tbody.push(obj);
							len++;
						});
						if (len == 7) {
							stop = false;
						}
						$('#da-thumbs > li').each(function() {
							$(this).hoverdir();
						});
					}
				});
			}
		}
	});
});