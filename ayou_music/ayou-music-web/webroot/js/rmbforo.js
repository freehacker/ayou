require([ "jquery" ], function($) {

	(function() {
		$();
	})();

}, function(err) {
	layer.msg('请刷新页面！', {
		icon : 5
	});
	var failedId = err.requireModules && err.requireModules[0];
	if (failedId === 'jquery') {
		requirejs.undef(failedId);
	}
});
