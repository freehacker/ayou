package com.ayou.sso.luosimao;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.SSLSocketFactory;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import com.ayou.core.commons.utils.socket.SocketUtil;

public class Luosimao {
	protected static final String api_key = "c933b60eb8e60576e184e92c9e0d8697";
	protected static final String verify_url = "https://captcha.luosimao.com/api/site_verify";

	/**
	 * 通过HttpClient
	 * @param res
	 * @return
	 */
	public static String validateByHttp(String res) {
		HttpClient client = HttpClients.createDefault();

		HttpPost post = new HttpPost(verify_url);

		List<BasicNameValuePair> prams = new ArrayList<>();
		prams.add(new BasicNameValuePair("api_key", api_key));
		prams.add(new BasicNameValuePair("response", res));

		try {
			post.setEntity(new UrlEncodedFormEntity(prams, "UTF-8"));
			HttpResponse response = client.execute(post);
			HttpEntity entity = response.getEntity();
			return EntityUtils.toString(entity, "UTF-8");
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 通过socket
	 * 此方法还未完善
	 * @param res
	 * @return
	 */
	public static String validateBySocket(String res) {
		String p = "POST " + verify_url + "?api_key=" + api_key + "&response=" + res;
		InputStream is = null;
		String line = "";
		Socket socket = SocketUtil.getSocket("captcha.luosimao.com", 80);
		try {
			((SSLSocketFactory) SSLSocketFactory.getDefault()).createSocket("captcha.luosimao.com", 443);
			OutputStream out = socket.getOutputStream();
			out.write(p.getBytes());
			out.write("Content-Type: application/x-www-form-urlencoded".getBytes());
			out.write("\r\n".getBytes());
			out.flush();
			is = socket.getInputStream();
			BufferedReader bf = new BufferedReader(new InputStreamReader(is, "utf-8"));

			while ((line = bf.readLine()) != null) {
				System.out.println(line);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		return line;
	}
}
