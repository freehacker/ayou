package com.ayou.sso.action;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ayou.core.commons.utils.security.AES;
import com.ayou.core.commons.utils.validation.UserUtil;
import com.ayou.module.identity.entity.LoginInfo;
import com.ayou.module.identity.entity.User;
import com.ayou.module.identity.service.IdentityService;
import com.ayou.sso.luosimao.Luosimao;
import com.baomidou.kisso.SSOHelper;
import com.baomidou.kisso.SSOToken;
import com.baomidou.kisso.web.waf.request.WafRequestWrapper;

/**
 * @category 用户注册
 * @version 1.0
 * @author AYOU
 * @date 2016年7月23日 下午5:15:15
 */
@RestController
@RequestMapping("/sso")
public class RegisterAction extends BaseAction {
	@Resource
	IdentityService identityService;

	@RequestMapping("/reg")
	public String reg(ModelMap map) {
		WafRequestWrapper wr = new WafRequestWrapper(request);

		String lsm_code = wr.getParameter("lsm_code");
		String lsm_return = Luosimao.validateByHttp(lsm_code);
		logger.info("LSM_SUCCESS:" + lsm_return);
		JSONObject js = new JSONObject(lsm_return);
		String error = js.get("error").toString();
		String res = js.get("res").toString();
		if (!"success".equals(res)) {
			map.put("error", error);
			return renderJsonMsg(map, "failed validate lsmcode!");
		}

		String userid = wr.getParameter("userid");
		String pass = wr.getParameter("password");
		if (StringUtils.isBlank(pass) && StringUtils.isBlank(userid)) {
			return renderJsonMsg(map, "用户名或密码不能为空！");
		} else if (userid.length() < 6 && pass.length() < 6) {
			return renderJsonMsg(map, "用户名和密码长度不小于6位！");
		}
		if (!identityService.isRegister(userid) && identityService.register(userid, pass)) {// 注册成功！
			if (UserUtil.getUserId(userid).equals("email")) {// 邮箱
				map.put("username", "email");
			} else if (UserUtil.getUserId(userid).equals("phone")) {// 手机
				map.put("username", "phone");
			}
			LoginInfo li = identityService.getLoginInfo(userid);
			// 登录
			SSOToken st = new SSOToken(request, li.getUuid());
			SSOHelper.setSSOCookie(request, response, st, true);
			if (true) {// redis缓存6h 缓存开关 待实现
				identityService.setLoginInfo(li, "user", new Long(60 * 60 * 6));
			}
			return renderJsonMsg(map, "true");
		} else {
			return renderJsonMsg(map, "false");
		}
	}

	/*
	 * 检查注册与否
	 */
	@RequestMapping("/checkreg")
	public String checkreg(ModelMap map) {
		WafRequestWrapper wr = new WafRequestWrapper(request);
		String userid = wr.getParameter("userid");
		return renderJsonMsg(map, identityService.isRegister(userid).toString());
	}

	/*
	 * 激活
	 */
	@RequestMapping(value = "/active", method = RequestMethod.POST)
	public String active(ModelMap map) {
		WafRequestWrapper wr = new WafRequestWrapper(request);
		String useruuid = wr.getParameter("u");
		String key = wr.getParameter("key");
		// 解密
		String k1 = AES.mailDecrypt1(key);
		String k2 = AES.mailDecrypt2(key);
		User user = new User();
		user.setUuid(useruuid);
		user.setEmail_code(k1);

		if (identityService.getUserByDB(user) == null) { //如果k1 没有那么用k2 我怕 加密的时候对utf-8 和ascci处理不同
			user.setEmail_code(k2);
			if (identityService.getUserByDB(user) == null) {
				map.put("active", false);
			}
		} else {
			//更新用户激活状态
			user = new User();
			user.setUuid(useruuid);
			user.setActive(1);
			identityService.update(user);
			map.put("active", true);
		}
		return renderJsonMsg(map, "active");
	}
}
