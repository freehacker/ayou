package com.ayou.sso.geetest;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ayou.site.base.action.SimpleActionSupport;

/**
 * @category 使用post方式
 * 返回验证结果
 * request表单中必须包含challenge, validate, seccode
 */
@RestController
public class VerifyLoginServlet extends SimpleActionSupport {
	protected Logger logger = LoggerFactory.getLogger(this.getClass());

	@RequestMapping(value = "/geetest/validate", method = RequestMethod.POST)
	public String postG(ModelMap map, HttpServletRequest request, String geetest_challenge, String geetest_validate, String geetest_seccode) throws IOException {
		GeetestLib gtSdk = new GeetestLib(GeetestConfig.getGeetest_id(), GeetestConfig.getGeetest_key());
		String challenge = geetest_challenge;
		String validate = geetest_validate;
		String seccode = geetest_seccode;
		// 从session中获取gt-server状态
		int gt_server_status_code = (Integer) request.getSession().getAttribute(gtSdk.gtServerStatusSessionKey);
		// 从session中获取userid
		String geetestid = (String) request.getSession().getAttribute("geetestid");
		int gtResult = 0;
		if (gt_server_status_code == 1) {// gt-server正常，向gt-server进行二次验证
			gtResult = gtSdk.enhencedValidateRequest(challenge, validate, seccode, geetestid);
			logger.info(gtResult + "");
		} else {// gt-server非正常情况下，进行failback模式验证
			logger.info("failback:use your own server captcha validate");
			gtResult = gtSdk.failbackValidateRequest(challenge, validate, seccode);
			logger.info(gtResult + "");
		}

		if (gtResult == 1) {
			map.put("status", "success");
			map.put("version", gtSdk.getVersionInfo());
			return renderJsonMsg(map, "验证通过！");
		} else {
			map.put("status", "fail");
			map.put("version", gtSdk.getVersionInfo());
			return renderJsonMsg(map, "验证失败！");
		}
	}
}
