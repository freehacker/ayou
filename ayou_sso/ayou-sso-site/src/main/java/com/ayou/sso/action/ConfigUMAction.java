package com.ayou.sso.action;

import javax.annotation.Resource;

import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ayou.module.identity.entity.LoginInfo;
import com.ayou.module.identity.service.IdentityService;
import com.baomidou.kisso.SSOHelper;
import com.baomidou.kisso.SSOToken;

/**
 * 配置用户信息
 */
@RestController
@RequestMapping("/sso")
public class ConfigUMAction extends BaseAction {
	@Resource
	IdentityService identityService;

	@RequestMapping("/user_config")
	public String index(ModelMap map) {
		SSOToken token = SSOHelper.getToken(request);
		if (token == null) {
			return renderJsonMsg(map, "no access!");
		} else {
			LoginInfo li = new LoginInfo();
			li = identityService.getLoginInfoForRedis(token.getUid());
			if (null != li) {// 缓存中有对象
				map.put("user", li);
			} else {
				/**
				 * 之前的想法是缓存中没有的话去数据库里面查
				 * 但是kisso本身的退出登录要实现SSOCache才能实现退出一个地方 全都退出（自带反射工具类有问题）
				 * so这里改为如果缓存里面没有这个用户的信息了 就当是他已经退出了
				 * 所以清空登录token
				 */
				SSOHelper.clearLogin(request, response);
				return renderJsonMsg(map, "no access!");
				//到数据库查
				//map.put("user", identityService.getLoginInfoByUuid(token.getUid()));
			}
			return renderJsonMsg(map, "ok");
		}
	}
}