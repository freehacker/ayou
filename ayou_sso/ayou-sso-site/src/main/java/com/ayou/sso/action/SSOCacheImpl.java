package com.ayou.sso.action;

import javax.annotation.Resource;

import com.ayou.module.identity.service.IdentityService;
import com.baomidou.kisso.SSOCache;
import com.baomidou.kisso.Token;

/**
 * @category 实现KISSO的SSOCache
 * @version 1.0
 * @author AYOU
 * @date 2016年7月24日 下午1:22:23z
 */
public class SSOCacheImpl implements SSOCache {
	@Resource
	protected IdentityService identityService;

	@Override
	public Token get(String key, int expires) {
		return identityService.getToken(key, expires);
	}

	@Override
	public boolean set(String key, Token token, int expires) {
		return identityService.setToken(key, token, expires);
	}

	@Override
	public boolean delete(String key) {
		return identityService.deleteToken(key);
	}

}
