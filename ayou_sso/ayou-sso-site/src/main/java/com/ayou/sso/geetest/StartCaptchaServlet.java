package com.ayou.sso.geetest;

import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @category 使用Get的方式
 * 返回challenge和capthca_id
 * 此方式以实现前后端完全分离的开发模式
 */
@RestController
public class StartCaptchaServlet {
	@RequestMapping(value = "/geetest/register", method = RequestMethod.GET)
	public String getG(HttpServletRequest request, HttpServletResponse response) {
		GeetestLib gtSdk = new GeetestLib(GeetestConfig.getGeetest_id(), GeetestConfig.getGeetest_key());
		String userid = UUID.randomUUID() + ""; // 自定义userid
		int gtServerStatus = gtSdk.preProcess(userid);// 进行验证预处理
		request.getSession().setAttribute(gtSdk.gtServerStatusSessionKey, gtServerStatus);// 将服务器状态设置到session中
		request.getSession().setAttribute("geetestid", userid);// 将userid设置到session中
		return gtSdk.getResponseStr();
	}
}