package com.ayou.sso.action;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.json.JSONObject;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ayou.module.identity.entity.LoginInfo;
import com.ayou.module.identity.service.IdentityService;
import com.ayou.sso.luosimao.Luosimao;
import com.baomidou.kisso.SSOConfig;
import com.baomidou.kisso.SSOHelper;
import com.baomidou.kisso.SSOToken;
import com.baomidou.kisso.Token;
import com.baomidou.kisso.annotation.Action;
import com.baomidou.kisso.annotation.Login;
import com.baomidou.kisso.common.util.HttpUtil;
import com.baomidou.kisso.web.waf.request.WafRequestWrapper;

/**
 * @category 登录
 */
@RestController
@RequestMapping("/sso")
public class LoginAction extends BaseAction {
	@Resource
	IdentityService identityService;

	@Login(action = Action.Skip)
	@RequestMapping("/login")
	public String login(ModelMap map) {
		WafRequestWrapper wr = new WafRequestWrapper(request);

		String lsm_code = wr.getParameter("lsm_code");
		String lsm_return = Luosimao.validateByHttp(lsm_code);
		logger.info("LSM_SUCCESS:" + lsm_return);
		JSONObject js = new JSONObject(lsm_return);
		String error = js.get("error").toString();
		String res = js.get("res").toString();
		if (!"success".equals(res)) {
			map.put("error", error);
			return renderJsonMsg(map, "failed validate lsmcode!");
		}

		String returnUrl = request.getParameter(SSOConfig.getInstance().getParamReturl());
		Token token = SSOHelper.getToken(request);
		if (token == null) {// 未登录

			String userId = wr.getParameter("userid");
			String pass = wr.getParameter("password");
			Integer login = identityService.Login(userId, pass);
			if (login == 0) {// 用户不存在
				map.put("s", 0);
				return renderJsonMsg(map, "none");
			} else if (login == 1) { // 登录成功
				LoginInfo li = identityService.getLoginInfo(userId);
				SSOToken st = new SSOToken(request, li.getUuid());
				SSOHelper.setSSOCookie(request, response, st, true);
				if (true) {// redis缓存6h 缓存开关 待实现
					identityService.setLoginInfo(li, "user", new Long(60 * 60 * 6));
				}
				if (StringUtils.isEmpty(returnUrl)) {
					map.put("ReturnURL", "http://www.qtdu.com");
					return renderJsonMsg(map, "ok");
				} else {
					map.put("ReturnURL", HttpUtil.decodeURL(returnUrl));
					return renderJsonMsg(map, "ok");
				}
			} else if (login == -1) {// 密码错误
				map.put("s", -1);
				return renderJsonMsg(map, "wrong");
				/*
				 * if (StringUtils.isNotEmpty(returnUrl)) { map.put("ReturnURL",
				 * HttpUtil.decodeURL(returnUrl)); return renderJsonMsg(map,
				 * "ok"); } map.put("ReturnURL",
				 * "https://passport.qtdu.com/sso/login.html");
				 */
			}
		} else {// 已登录
			if (StringUtils.isEmpty(returnUrl)) {
				map.put("ReturnURL", "http://www.qtdu.com");
				return renderJsonMsg(map, "ok");
			}
			map.put("ReturnURL", HttpUtil.decodeURL(returnUrl));
			return renderJsonMsg(map, "ok");
		}
		return renderJsonMsg(map, "ok");
	}
}