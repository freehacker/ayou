require([ "jquery", "avalon", "js/view/bs", "bootstrap", "domReady!" ], function($, avalon, bs) {
	var modelVM = avalon.define({
		$id : "model",
		tip : '',
		tipShow : false,
		model : {
			userid : '',
			password : '',
			ReturnURL : '',
		},
		login : function() {
			if (!modelVM.model.userid || !modelVM.model.password) {
				modelVM.tip = "用户名或密码不能为空！";
				modelVM.tipShow = true;
				return;
			}
			bs.login(modelVM.model.$model, function(data) {
				if (data.success) {
					//window.location.href = data.ReturnURL;
				}
			});
		}
	});

	avalon.scan();

	(function() {
		
	})();
});