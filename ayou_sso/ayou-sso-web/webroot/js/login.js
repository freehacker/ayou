require([ "jquery", "avalon", "js/view/bs", "bootstrap", "layer", "//captcha.luosimao.com/static/js/api.js", "domReady!" ], function($, avalon, bs) {
	var ReturnURL = getParam("ReturnURL");
	var modelVM = avalon.define({
		$id : "model",
		tip : '',
		tipShow : false,
		model : {
			userid : '',
			password : '',
			ReturnURL : '',
			lsm_code : '',
		},
		login : function() {

			if (!modelVM.model.userid || !modelVM.model.password) {
				modelVM.tip = "用户名或密码不能为空！";
				modelVM.tipShow = true;
				return;
			}

			if (modelVM.model.userid.length < 6) {
				modelVM.tip = "用户名不能小于6位！";
				modelVM.tipShow = true;
				return;
			}
			if (modelVM.model.password.length < 8) {
				modelVM.tip = "密码不能小于8位！";
				modelVM.tipShow = true;
				return;
			}

			modelVM.model.lsm_code = document.getElementById('lc-captcha-response').value;
			if (!modelVM.model.lsm_code) {// 重置验证码
				modelVM.tip = "请点击验证码！";
				modelVM.tipShow = true;
				LUOCAPTCHA.reset();
				return;
			}

			bs.login(modelVM.model.$model, function(data) {
				modelVM.tipShow = false;
				if(data.error){
					layer.msg('验证码调用失败，请联系客服！', {icon : 4});
				}
				if (data.success && data.s == 0) {
					layer.msg('用户不存在！', {icon : 2});
					LUOCAPTCHA.reset();
				} else if (data.success && data.s == -1) {
					layer.msg('用户名或密码错误！', {icon : 4});
					LUOCAPTCHA.reset();
				} else {
					window.location.href = data.ReturnURL;
				}
			});
		},
		reg : function() {
			if (ReturnURL) {
				window.location.href = "register.html?ReturnURL=" + ReturnURL;
			} else {
				window.location.href = "register.html";
			}
		}
	});

	avalon.scan();

	(function() {
		if (ReturnURL) {
			modelVM.model.ReturnURL = ReturnURL;
		}
		$.ajax({
			type : "get",
			timeout : 30000,
			async : true,
			cache : true,
			dataType : "json",
			url : "sso/user_config",
			success : function(data) {
				if (data.user) {
					if (ReturnURL) {
						window.location.href = decodeURIComponent(ReturnURL);
					} else {
						window.location.href = "https://www.qtdu.com";
					}
				}
			},
			error : function(data) {
				console.log(data);
			}
		});
	})();
});