require([ "jquery", "avalon", "js/view/user.bs", 'layer', 'oniui/kindeditor/avalon.kindeditor', 'oniui/datepicker/avalon.datepicker',
		'oniui/datepicker/avalon.coupledatepicker', 'oniui/checkboxlist/avalon.checkboxlist', 'oniui/dropdownlist/avalon.dropdownlist', 'oniui/button/avalon.button',
		'domReady!' ], function($, avalon, bs, layer) {
	var uuid = getParam("uuid");
	var modelVM = avalon.define({
		$id : "model",
		showResult : false,
		tip : '',
		tipShow : false,
		model : {},
		txt : '',
		val : [ 'TUESDAY' ],
		vals:[],
		$checkboxlistOpt:{
			width: 1024,
		}
	});

	avalon.scan();

	(function() {
		/*var oni = $("div[class^='oni']");
		console.log(oni);
		$.each(oni, function(i,e) {
			//$(oni[i]).attr('class',"oni-bootstrap");
			
			//$(oni[i]).removeAttr("-webkit-box-sizing");
			//$(oni[i]).removeAttr("-moz-box-sizing");
			//$(oni[i]).removeAttr("box-sizing");
			
			//$(oni[i]).attr("style",{"-webkit-box-sizing":""});
			//$(oni[i]).attr("style",{"-moz-box-sizing":""});
			//$(oni[i]).attr("style",{"box-sizing":""});
			//$(oni[i]).addClass("oni-bootstrap");
			
			//$(oni[i]).css('-webkit-box-sizing', 'inherit');
			//$(oni[i]).css('-moz-box-sizing', 'inherit');
			//$(oni[i]).css('box-sizing', 'inherit');
			
			//console.log($(oni[i]).attr('class'));
		});*/

		// layer.tips('Hello tips!', '#test5');
		/*
		 * layer.tips('我是另外一个tips，只不过我长得跟之前那位稍有些不一样。', '吸附元素选择器', { tips : [ 1,
		 * '#3595CC' ], time : 4000 });
		 */
		/*
		 * layer.open({ type : 1, area : [ '600px', '360px' ], shadeClose :
		 * true, // 点击遮罩关闭 content : '\<\div style="padding:20px;">自定义内容\<\/div>'
		 * });
		 */

		bs.view(uuid, function(data) {
			// $("div[class^='oni']");

			if (data.result) {
				modelVM.model = data.result;
			} else {
				modelVM.model = data
				layer.msg(modelVM.model.errorMsg, {
					offset : 'rb',
					shift : 6,
					icon : 5,
					time : 60000, // 60s后自动关闭
					btn : [ '刷新页面', '通知管理', '继续浏览' ],
					yes : function() {
						location.reload();
					},
					btn2 : function() {
						layer.closeAll();
					},
					btn3 : function() {
						layer.msg("AYOU需要你这种人才~~~", {
							icon : 6,
						});
					}
				});
			}

			$(document).attr("title", modelVM.model.name + "的详细信息");// 修改title值
			/*
			 * setTimeout(function() { layer.msg('欢迎回来'+
			 * modelVM.model.name+'！'); layer.open({ title:'提示！', type : 1, skin :
			 * 'layui-layer-demo', // 样式类名 closeBtn : 0, // 不显示关闭按钮 shift : 2,
			 * shadeClose : true, // 开启遮罩关闭 content : '\<\div
			 * style="padding:20px;">欢迎回来'+ modelVM.model.name+'！\<\/div>' }); },
			 * 3000);
			 */
		});
	})();

}, function(err) {
	layer.msg('请刷新页面！', {
		icon : 5
	});
	var failedId = err.requireModules && err.requireModules[0];
	if (failedId === 'jquery') {
		requirejs.undef(failedId);
	}
});

