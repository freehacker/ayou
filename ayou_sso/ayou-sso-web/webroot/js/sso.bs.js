define([ 'ayou', 'jquery', 'noty', 'bootstrap' ], function(ayou, $, noty) {
	var URL = {
		view : '/user/view/',
		list : '',
		login : '/sso/login',
		add : '',
		del : '',
		edit : '',
	};

	var view = function(uuid, success) {
		var config = {
			url : URL.view + uuid,
			type : 'get',
			success : success
		};
		ayou.ajax(config);
	}

	var login = function(model, success) {
		var config = {
			url : URL.login,
			data : model,
			success : success
		};
		ayou.ajax(config);
	}
	return {
		view : view,
		login : login,
	}

})