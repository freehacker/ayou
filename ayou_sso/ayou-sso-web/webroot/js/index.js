require([ "jquery", "avalon", "js/view/bs", 'layer', 'bootstrap', 'domReady!' ], function($, avalon, bs, layer) {
	var uuid = getParam("uuid");
	var modelVM = avalon.define({
		$id : "model",
		showResult : false,
		tip : '',
		tipShow : false,
		model : {},
		txt : '',
		val : [ 'TUESDAY' ],
		vals:[],
		$checkboxlistOpt:{
			width: 1024,
		}
	});

	avalon.scan();

	(function() {
		
	})();

}, function(err) {
	layer.msg('请刷新页面！', {
		icon : 5
	});
	var failedId = err.requireModules && err.requireModules[0];
	if (failedId === 'jquery') {
		requirejs.undef(failedId);
	}
});

