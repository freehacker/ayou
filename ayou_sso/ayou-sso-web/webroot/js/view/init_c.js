var UM = null;
define(["avalon","jquery"], function(avalon,$) {
	var sysLoginVM = avalon.define({
		$id : "sysLogin",
		show : false,
		model:{},
		logout:function(){
			$.ajax({
				type : "get",
				timeout : 30000,
				async : true,
				cache : true,
				dataType : "json",
				url : "sso/logout",
				success : function(data){
					if(data.success){
						if(login && data.r_url){//如果是需要登录的页面就跳出
							window.location.href = decodeURIComponent(data.r_url);
						}
						UM = {};
						sysLoginVM.model=UM;
					}
				},
				error : function(data){
					console.log(data);
				}
			});
		},
		login:function(){
			window.location.href = "https://passport.qtdu.com/login.html?ReturnURL=" + encodeURIComponent(window.location.href);
		},
		reg:function(){
			window.location.href = "https://passport.qtdu.com/register.html?ReturnURL=" + encodeURIComponent(window.location.href);
		}
	});
	avalon.filters.FTopName = function(str) {
		if (str) {
			return str;
		}else if(sysLoginVM.model.email){
			return sysLoginVM.model.email;
		}else if(sysLoginVM.model.phone){
			return sysLoginVM.model.phone;
		}
	};
	function configUM(){
		$.ajax({
			type : "get",
			timeout : 30000,
			async : true,
			cache : true,
			dataType : "json",
			url : "sso/user_config",
			success : function(data){
				if(data.user){
					UM = data.user;
					sysLoginVM.model=UM;
				};
				if(login && !UM){
					window.location = "login.html?ReturnURL="+encodeURIComponent(window.location.href);
				}
			},
			error : function(data){
				/*var href = encodeURIComponent(window.location.href);
				window.location = "/user/login.html?reDirect="+href;*/
				console.log(data);
			}
		});
	}
	(function(){
		configUM();
	})();
});