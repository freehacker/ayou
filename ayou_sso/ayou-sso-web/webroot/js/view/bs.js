define([ 'ayou', 'jquery', 'js/view/init_c', 'bootstrap'], function(ayou, $) {
	var URL = {
		login : 'sso/login',
		list : '',
		reg : 'sso/reg',
		check : 'sso/checkreg',
		del : '',
		edit : '',
	};
	var reg = function(model, success) {
		var config = {
			url : URL.reg,
			type : 'post',
			data : model,
			success : success
		};
		ayou.ajax(config);
	}
	var check = function(model, success) {
		var config = {
			url : URL.check,
			type : 'post',
			data : model,
			success : success
		};
		ayou.ajax(config);
	}
	var login = function(model, success) {
		var config = {
			url : URL.login,
			type : 'post',
			data : model,
			success : success
		};
		ayou.ajax(config);
	}
	return {
		reg : reg,
		check : check,
		login : login,
	}
})