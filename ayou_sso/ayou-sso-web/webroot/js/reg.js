require([ "jquery", "avalon", "js/view/bs", "layer", "//captcha.luosimao.com/static/js/api.js", "domReady!" ], function($, avalon, bs) {
	var ReturnURL = getParam("ReturnURL");
	var modelVM = avalon.define({
		$id : "model",
		tip : '',
		tipShow : false,
		regok : false,
		model : {
			userid : '',
			password : '',
			passwords : '',
			ReturnURL : '',
			lsm_code : '',
		},
		checkU : function() {
			if (modelVM.model.userid.length < 6) {
				modelVM.tip = "用户名长度需要6到25之间！";
				$("#tip").css('color', '#d81358');
				modelVM.tipShow = true;
				return;
			} else {
				modelVM.tip = "";
				modelVM.tipShow = false;
			}
			bs.check(modelVM.model.$model, function(data) {
				if (data.resultMsg == 'true') {
					modelVM.tip = "此用户已被注册！";
					$("#tip").css('color', '#f99d43');
					modelVM.tipShow = true;
				} else {
					modelVM.tip = "可注册！";
					modelVM.tipShow = true;
					$("#tip").css('color', '#5cb85c');
				}
			})
		},
		checkP : function() {
			if (modelVM.model.password.length < 8) {
				modelVM.tip = "密码长度不能小于8！";
				$("#tip").css('color', '#f99d43');
				modelVM.tipShow = true;
			} else {
				modelVM.tip = "";
				modelVM.tipShow = false;
			}
		},
		checkPs : function() {
			if (modelVM.model.password != modelVM.model.passwords) {
				modelVM.tip = "两次输入密码不一致！";
				$("#tip").css('color', '#f99d43');
				modelVM.tipShow = true;
			} else {
				modelVM.tip = "";
				modelVM.tipShow = false;
			}
		},
		reg : function() {
			if (!modelVM.model.userid || !modelVM.model.password) {
				modelVM.tip = "用户名或密码不能为空！";
				$("#tip").css('color', '#f99d43');
				modelVM.tipShow = true;
				return;
			}

			modelVM.model.lsm_code = document.getElementById('lc-captcha-response').value;
			if (!modelVM.model.lsm_code) {// 重置验证码
				layer.msg('请点击验证码！', {icon : 4});
				LUOCAPTCHA.reset();
				return;
			}

			bs.reg(modelVM.model.$model, function(data) {
				if (data.error) {
					layer.msg('验证码调用失败，请联系客服！', {
						icon : 4
					});
				}
				if (data.resultMsg) {
					if (ReturnURL) {
						window.location.href = decodeURIComponent(ReturnURL);
					}
					window.location.href = "/";
				}
			});
		},
		login : function() {
			if (ReturnURL) {
				window.location.href = "login.html?ReturnURL=" + ReturnURL;
			} else {
				window.location.href = "login.html";
			}
		}
	});

	avalon.scan();

	(function() {
		if (ReturnURL) {
			modelVM.model.ReturnURL = decodeURIComponent(ReturnURL);
		}
	})();
});