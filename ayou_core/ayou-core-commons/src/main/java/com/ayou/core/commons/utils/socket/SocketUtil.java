package com.ayou.core.commons.utils.socket;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 * @author AYOU
 * @version 2016年3月30日 下午10:20:44
 */
public class SocketUtil {
	public static Socket getSocket(String url, int port) {
		Socket sk = null;
		try {
			sk = new Socket();
			sk.connect(new InetSocketAddress(url, port));
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return sk;
	}
}
