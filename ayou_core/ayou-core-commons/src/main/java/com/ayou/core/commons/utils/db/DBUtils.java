package com.ayou.core.commons.utils.db;


import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Table;

import com.ayou.core.commons.annotation.DBExclude;
import com.ayou.core.commons.utils.ArrayUtil;
import com.ayou.core.commons.utils.DateUtil;
import com.ayou.core.commons.utils.StringUtil;


public class DBUtils {
	public enum SqlTypes{
		INSERT,UPDATE,DELETE,QUERY
	}
	/*	
	 * 将 bean 封装成 sql 语句
	 * bean 对应的class 必须使用注解 @Table(name="xxxxx")
	 * @param sqlType sql 类型
	 * @param clz 对象的类型
	 * @param obj 需要的对象
	 * @return sql 语句
	 */
	public static <T> String getSqlByObject(SqlTypes sqlType, T obj) {
		Class<?> clz = obj.getClass();

		// 如果使用了注解，则获取注解的tablename
		if (clz.isAnnotationPresent(Table.class)) {
			Table table = (Table) clz.getAnnotation(Table.class);
			String tableName = table.name();
			return getSqlByObject(sqlType, obj, tableName);
		} else {
			System.err.println("表没有使用@Table注解,此方法只用于有@Table(name=\"xxxxx\")注解的实体");
			return null;
		}
	}
		
	/**
	 * 将 bean 封装成 sql 语句
	 * @param sqlType sql 类型
	 * @param clz 对象的类型
	 * @param obj 需要的对象
	 * @param tableName 表名
	 * @return sql语句
	 */
	public static <T> String getSqlByObject(SqlTypes sqlType , T obj , String tableName){
		Class<?> clz = obj.getClass();
		String[] intTypes = {"int","java.lang.Integer","short","java.lang.Short","long","java.lang.Long","BigDecimal","java.math.BigDecimal"};
		String[] dateFields = {"Date","java.util.Date"};
		StringBuffer sql = new StringBuffer("");
		StringBuffer cols = new StringBuffer("");
		StringBuffer values = new StringBuffer("");
		
		//获取所有私有成员变量
		Field[] privateFields = clz.getDeclaredFields(); 
		Field[] publicFields = clz.getFields(); 
		Field[] fields = ArrayUtil.concatAll(privateFields, publicFields);
		
		if(sqlType == SqlTypes.INSERT){
			sql.append("insert into ").append(tableName).append(" (");
			int index = 1;
			for(Field field : fields){
				//如果标注为DBExclude，则忽略
				if(field.isAnnotationPresent(DBExclude.class)){
					DBExclude exclude = field.getAnnotation(DBExclude.class);
	                //获取元素值  
					boolean isExclude = exclude.value();
	                if(isExclude){
	                	continue;
	                }
				}
				
				//设置成员变量可访问
				field.setAccessible(true);
				String fieldName = field.getName();
				Class<?> type = field.getType();
				
				//跳过serialVersionUID
				if(fieldName.equals("serialVersionUID")){
					continue;
				}
				
				Object value = null;
				//获取value
				try{
					PropertyDescriptor pd = new PropertyDescriptor(fieldName,clz);
					Method getMethod = pd.getReadMethod(); //获得get方法
					value = getMethod.invoke(obj);
				} catch (Exception e){
					e.printStackTrace();
				}
				
				//如果有注解，则使用注解的字段名，没有注解，则使用字段名
				if(field.isAnnotationPresent(Column.class)){  
	                Column column = field.getAnnotation(Column.class);
	                //获取元素值  
	                fieldName = column.name();
	                if(fieldName.equals("")){
	                	continue;
	                }
	            }
				
				//如果没有值，则不需要插入此字段，只需使用db默认值即可
				if(null == value || "".equals(value)){
					continue;
				}
				
				boolean isNumber = false;//是否是数字类型
				boolean isDate = false;  //是否是date类型
				if (StringUtil.contains(intTypes, type.getName())) {
					isNumber = true;
				} else if (StringUtil.contains(dateFields, type.getName())) {
					isDate = true;
				}
				
				if (index == 1) {
					cols.append(fieldName);
					if (isNumber) {
						values.append(value);
					} else if (isDate) {
						values.append("to_date('" + DateUtil.formatTime((Date) value) + "','yyyy-mm-dd hh24:mi:ss')");
					} else {
						values.append("'").append(value).append("'");
					}
				} else {
					cols.append(",").append(fieldName);
					if (isNumber) {
						values.append(",").append(value);
					} else if (isDate) {
						values.append(",to_date('" + DateUtil.formatTime((Date) value) + "','yyyy-mm-dd hh24:mi:ss')");
					} else {
						values.append(",'").append(value).append("'");
					}

				}
				index ++;
			}
			
			sql.append(cols.toString()).append(") values (").append(values).append(")");
		}
		
		if(sqlType == SqlTypes.UPDATE){
			sql.append("UPDATE ").append(tableName).append(" SET");
			int index = 1;
			for(Field field : fields){
				
				//设置成员变量可访问
				field.setAccessible(true);
				String fieldName = field.getName();
				Class<?> type = field.getType();
				
				//跳过serialVersionUID
				if(fieldName.equals("serialVersionUID")){
					continue;
				}
				
				Object value = null;
				//获取value
				try{
					PropertyDescriptor pd = new PropertyDescriptor(fieldName,clz);
					Method getMethod = pd.getReadMethod(); //获得get方法
					value = getMethod.invoke(obj);
				} catch (Exception e){
					e.printStackTrace();
				}
				
				//如果有注解，则使用注解的字段名，没有注解，则使用字段名
				if(field.isAnnotationPresent(Column.class)){  
	                Column column = field.getAnnotation(Column.class);
	                //获取元素值  
	                fieldName = column.name();
	                if(fieldName.equals("")){
	                	continue;
	                }
	            }
				
				//如果没有值，则不需要插入此字段，只需使用db默认值即可
				if(null == value || "".equals(value)){
					continue;
				}
				
				boolean isNumber = false;//是否是数字类型
				boolean isDate = false;  //是否是date类型
				if (StringUtil.contains(intTypes, type.getName())) {
					isNumber = true;
				} else if (StringUtil.contains(dateFields, type.getName())) {
					isDate = true;
				}
				
				if (index == 1) {
					cols.append(fieldName);
					if (isNumber) {
						values.append(value);
					} else if (isDate) {
						values.append("to_date('" + DateUtil.formatTime((Date) value) + "','yyyy-mm-dd hh24:mi:ss')");
					} else {
						values.append("'").append(value).append("'");
					}
				} else {
					cols.append(",").append(fieldName);
					if (isNumber) {
						values.append(",").append(value);
					} else if (isDate) {
						values.append(",to_date('" + DateUtil.formatTime((Date) value) + "','yyyy-mm-dd hh24:mi:ss')");
					} else {
						values.append(",'").append(value).append("'");
					}

				}
				index ++;
			}
			
			sql.append(cols.toString()).append(") values (").append(values).append(")");
		}
		
		return sql.toString();
	}
	
}

