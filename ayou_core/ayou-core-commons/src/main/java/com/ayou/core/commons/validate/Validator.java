package com.ayou.core.commons.validate;

/**
 * 校验器
 *
 * @param <T>
 */
public abstract class Validator<V extends Object> {

	/**
	 * 待检查对象或属性的获取器
	 * 
	 * @param <V>
	 */
	public static abstract class ItemGeter<V> {
		/**
		 * 获取待验证的对象或属性
		 * 
		 * @return
		 */
		public abstract V getToCheckItem();
	}

	private static final class DirectGeter<V> extends ItemGeter<V> {
		private V item;

		public DirectGeter(V item) {
			this.item = item;
		}

		@Override
		public V getToCheckItem() {
			return item;
		}
	}

	protected String desc;

	protected boolean isValid = true;

	protected String errorMsg;

	private final ItemGeter<V> itemGeter;

	/**
	 * 构造器
	 * 
	 * @param toCheckItem
	 *            - 待检查对象或属性
	 * @param desc
	 *            - 待检查对象或属性描述
	 */
	public Validator(V toCheckItem, String desc) {
		this.itemGeter = new DirectGeter<V>(toCheckItem);
		this.desc = desc;
	}

	/**
	 * 构造器
	 * 
	 * @param toCheckItemGeter
	 *            - 待检查对象或属性的获取器
	 * @param desc
	 *            - 待检查对象或属性描述
	 */
	public Validator(ItemGeter<V> toCheckItemGeter, String desc) {
		this.itemGeter = toCheckItemGeter;
		this.desc = desc;
	}

	/**
	 * 检查字段值是否满足约束条件
	 */
	public abstract boolean validate();

	/**
	 * 获得检查结果
	 * 
	 * @return
	 */
	public boolean isValid() {
		return isValid;
	}

	/**
	 * 验证不通过的错误提示
	 * 
	 * @return
	 */
	public String getErrorMsg() {
		return errorMsg;
	}

	protected V getToCheckItem() {
		return itemGeter.getToCheckItem();
	}
}
