package com.ayou.core.commons.validate;

import com.ayou.core.commons.exception.ProjectException;

@SuppressWarnings("serial")
public class ValidateException extends ProjectException {
	public ValidateException() {
		super();
	}

	public ValidateException(String msg) {
		super(msg);
	}

	public ValidateException(String message, Throwable cause) {
		super(message, cause);
	}

	public ValidateException(Throwable cause) {
		super(cause);
	}
}
