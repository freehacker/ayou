package com.ayou.core.commons.exception;

/**
* @author AYOU
* @version 2016年3月28日 下午8:32:51
*/
@SuppressWarnings("serial")
public class DbException extends ProjectException{
	public DbException() {
		super("数据库异常.");
	}

	public DbException(String message, Throwable cause) {
		super(message, cause);
	}

	public DbException(String msg) {
		super(msg);
	}

	public DbException(Throwable cause) {
		super(cause);
	}
}
