package com.ayou.core.commons.utils;

import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

/**
 * @author AYOU
 * @version 2016年3月28日 下午7:43:59
 */
public class JsonUtil {
	private static Gson defaultGson = new GsonBuilder().excludeFieldsWithModifiers(Modifier.TRANSIENT)
			.setDateFormat(Constants.DEFAULT_DATETIME_FORMAT).create();

	public static Gson ExposeAnnotationGson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation()
			.setDateFormat(Constants.DEFAULT_DATETIME_FORMAT).create();

	public static String toJson(Object o) {
		return toJson(o, null);
	}

	public static String toJson(Object o, Gson gson) {
		if (gson == null) {
			gson = defaultGson;
		}
		return gson.toJson(o);
	}

	public static <T> T fromJson(String json, Class<T> clazz) {
		json = removeSerialVersionUID(json);
		return defaultGson.fromJson(json, clazz);
	}

	private static String removeSerialVersionUID(String json) {
		int b = json.indexOf("\"serialVersionUID\":");
		int e;
		while (b > 0) {
			e = json.indexOf(",", b);
			if (e < 0) {
				e = json.indexOf("}", b);
			} else {
				e++;
			}
			String s = json.substring(b, e);
			json = json.replace(s, "");
			b = json.indexOf("\"serialVersionUID\":", e);
		}
		return json;
	}

	public static <T> List<T> fromJsonArray(String json, final Class<T> clazz) {
		JsonParser jp = new JsonParser();
		JsonArray ja = (JsonArray) jp.parse(json);
		int n = ja.size();
		final List<T> res = new ArrayList<T>(n);
		Consumer<JsonElement> action = new Consumer<JsonElement>() {
			public void accept(JsonElement t) {
				T obj = defaultGson.fromJson(t, clazz);
				res.add(obj);
			}
		};
		ja.forEach(action);
		return res;
	}

	public static Map<String, Object> toMap(JSONObject json) throws JSONException {
		Map<String, Object> map = new HashMap<String, Object>();
		Iterator<String> iter = json.keys();
		String key = null;
		Object value = null;
		while (iter.hasNext()) {
			key = iter.next();
			value = json.get(key);
			map.put(key, value);
		}
		return map;
	}
}
