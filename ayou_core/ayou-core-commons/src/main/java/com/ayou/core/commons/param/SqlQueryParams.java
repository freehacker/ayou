package com.ayou.core.commons.param;

import java.util.List;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

/**
 * Sql查询参数和表达式的封装
 */
@SuppressWarnings("serial")
public abstract class SqlQueryParams extends QueryParams {

	protected String buildQSql(List<Object> params) {
		String sql = "";
		if(StringUtils.isNotBlank(getQ()) && ArrayUtils.isNotEmpty(getQFields())) {
			sql = " and (1=2";
			for(String field : getQFields()) {
				sql += " or " + field + " like ?";
				params.add("%" + getQ() + "%");
			}
			sql += ") ";
		}
		return sql;
	}

	protected String buildExtSql(List<Object> params) {
		return "";
	}

	public abstract String buildSql(List<Object> params);
}
