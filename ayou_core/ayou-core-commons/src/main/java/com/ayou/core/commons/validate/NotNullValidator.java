package com.ayou.core.commons.validate;


/**
 * 非空校验
 *
 */
public class NotNullValidator extends Validator<Object> {
	public NotNullValidator(Object toCheckItem, String desc) {
		super(toCheckItem, desc);
	}

	@Override
	public boolean validate() {
		Object toCheckItem = getToCheckItem();
		if(toCheckItem == null) {
			isValid = false;
			errorMsg = "字段[" + desc + "]值不能为空";
		}
		return isValid;
	}
}
