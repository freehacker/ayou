package com.ayou.core.commons.utils;

public interface Sortable<T> extends Comparable<T> {
	public static final int DefaultIndex = 9999;

	public Integer getSortIdx();

	public void setSortIdx(Integer sortIdx);

	/**
	 * 比较功能的默认实现，子类应该只覆盖customCompareTo方法
	 * @param anotherSortObj
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@Override
	default int compareTo(T obj) {
		if(obj instanceof Sortable) {
			Sortable<T> anotherSortObj = (Sortable<T>) obj;
			if(getSortIdx() == null || anotherSortObj == null || anotherSortObj.getSortIdx() == null) {
				return customCompareTo(obj);
			}
			if(getSortIdx() >= 0 && anotherSortObj.getSortIdx() >= 0) {
				return getSortIdx() - anotherSortObj.getSortIdx();
			}
		}
		return Integer.MAX_VALUE;
	}

	/**
	 * 自定义的比较方法，用于子类覆盖实现，仅当getSortIdx() == null时有效
	 * @param anotherSortObj
	 * @return
	 */
	default int customCompareTo(T obj) {
		return Integer.MAX_VALUE;
	}
}
