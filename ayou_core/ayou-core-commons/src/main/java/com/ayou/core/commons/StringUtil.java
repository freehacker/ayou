package com.ayou.core.commons;

/**
 * @author AYOU
 * @version 2016年5月10日 上午10:04:57
 */
public class StringUtil {

	public static String toUpperCase(String upStr) {
		return upStr.toUpperCase();
	}

	public static String toLowerCase(String lowStr) {
		return lowStr.toLowerCase();
	}
}
