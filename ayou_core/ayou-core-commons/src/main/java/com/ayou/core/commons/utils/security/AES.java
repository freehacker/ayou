package com.ayou.core.commons.utils.security;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;

import com.ayou.core.commons.utils.UUIDUtil;

/**
 * @category AES
 * @author AYOU
 * @version 1.0
 * @since JDK 1.8
 * @date 2016年12月2日 上午8:48:49
 */
public class AES {

	public static String encrypt(String key, String initVector, String value) {
		try {
			IvParameterSpec iv = new IvParameterSpec(initVector.getBytes("UTF-8"));
			SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes("UTF-8"), "AES");

			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
			cipher.init(Cipher.ENCRYPT_MODE, skeySpec, iv);

			byte[] encrypted = cipher.doFinal(value.getBytes());
			// System.out.println("encrypted string: " + Base64.encodeBase64String(encrypted));

			return Base64.encodeBase64String(encrypted);
		} catch (Exception ex) {
			return null;
			//ex.printStackTrace();
		}
	}

	public static String decrypt(String key, String initVector, String encrypted) {
		try {
			IvParameterSpec iv = new IvParameterSpec(initVector.getBytes("UTF-8"));
			SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes("UTF-8"), "AES");

			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
			cipher.init(Cipher.DECRYPT_MODE, skeySpec, iv);

			byte[] original = cipher.doFinal(Base64.decodeBase64(encrypted));

			return new String(original);
		} catch (Exception ex) {
			return null;
			//ex.printStackTrace();
		}
	}

	/**
	 * 加密
	 * @return
	 */
	public static String mailEncrypt(String e) {
		String key = "七听都万岁!";
		String initVector = "世友的加密!";
		if (key.getBytes().length != 16) {
			key = "abcCdefghijklmnX";
		}
		if (initVector.getBytes().length != 16) {
			initVector = "?nq#xn!8x09@q<~q";
		}
		return encrypt(key, initVector, e);
	}

	/**
	 * 解密1
	 * @return
	 */
	public static String mailDecrypt1(String aes) {
		String key = "七听都万岁!";
		String initVector = "世友的加密!";
		return decrypt(key, initVector, aes);
	}

	/**
	 * 解密1
	 * @return
	 */
	public static String mailDecrypt2(String aes) {
		String key = "abcCdefghijklmnX";
		String initVector = "?nq#xn!8x09@q<~q";
		return decrypt(key, initVector, aes);
	}
	
	public static void main(String[] args) {
		String key = "七听都万岁!"; // UUIDUtil.generate().substring(0, 16);
		String initVector = "世友的加密!"; // UUIDUtil.generate().substring(0, 16);
		if (key.length() != 16) {
			key = "abcCdefghijklmnX";
		}
		if (initVector.length() != 16) {
			initVector = "?nq#xn!8x09@q<~q";
		}
		String x = encrypt(key, initVector, UUIDUtil.generate() + "禁止任何未授权的黑客行为！");
		System.out.println(x);

		System.out.println(decrypt(key, initVector, "QzaEsdE8H1q2ENo/zLqip1cZiFfX4tzOZICwrBxVtD1V5hxfXu3Py4+XK7aliss/XQ/WcpbwExo0v0+rRTe1vJI5uAC2DvalsVN16vjCbYo="));
		//System.out.println(mailDecrypt1("QzaEsdE8H1q2ENo/zLqip1cZiFfX4tzOZICwrBxVtD1V5hxfXu3Py4+XK7aliss/XQ/WcpbwExo0v0+rRTe1vJI5uAC2DvalsVN16vjCbYo="));
		System.out.println(mailDecrypt2("QzaEsdE8H1q2ENo/zLqip1cZiFfX4tzOZICwrBxVtD1V5hxfXu3Py4+XK7aliss/XQ/WcpbwExo0v0+rRTe1vJI5uAC2DvalsVN16vjCbYo="));
	}
}
