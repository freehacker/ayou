package com.ayou.core.commons.exception;

/**
 * @author AYOU
 * @version 2016年5月10日 下午1:11:23
 */
public class ServiceException extends RuntimeException {
	private static final long serialVersionUID = 150856064564623681L;

	public ServiceException(String message, Throwable root) {
		super(message, root);
	}

	public ServiceException(String message) {
		super(message);
	}
}
