package com.ayou.core.commons.validate;

import org.apache.commons.lang3.StringUtils;

/**
 * 字符串字段非空约束
 * 
 * @param <T>
 */
public class NotNullStringValidator extends Validator<String> {
	public NotNullStringValidator(String toCheckItem, String desc) {
		super(toCheckItem, desc);
	}

	@Override
	public boolean validate() {
		String toCheckItem = getToCheckItem();
		if (StringUtils.isBlank(toCheckItem)) {
			isValid = false;
			errorMsg = "字段[" + desc + "]值不能为空";
		}
		return isValid;
	}
}
