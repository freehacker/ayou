package com.ayou.core.commons.exception;

public class ExpressionSyntaxException extends RuntimeException {
	private static final long serialVersionUID = 3273620985440875711L;

	public ExpressionSyntaxException() {
		super("Expression syntax error.");
	}

	public ExpressionSyntaxException(String message, Throwable cause) {
		super(message, cause);
	}

	public ExpressionSyntaxException(String msg) {
		super(msg);
	}

	public ExpressionSyntaxException(Throwable cause) {
		super(cause);
	}
}
