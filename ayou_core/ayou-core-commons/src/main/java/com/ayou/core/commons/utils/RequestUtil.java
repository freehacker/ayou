package com.ayou.core.commons.utils;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.util.UrlPathHelper;

public class RequestUtil {
	/**
	 * Convenience method to get the application's URL based on request
	 * variables.
	 */
	public static String getAppURL(HttpServletRequest request) {
		StringBuffer url = new StringBuffer();
		int port = request.getServerPort();
		if (port < 0) {
			port = 80; // Work around java.net.URL bug
		}
		String scheme = request.getScheme();
		url.append(scheme);
		url.append("://");
		url.append(request.getServerName());
		if ((scheme.equals("http") && (port != 80)) || (scheme.equals("https") && (port != 443))) {
			url.append(':');
			url.append(port);
		}
		url.append(request.getContextPath());
		return url.toString();
	}

	public static String getParameter(HttpServletRequest request, String param) {
		return request.getAttribute(param) != null ? (String) request.getAttribute(param) : request.getParameter(param);
	}

	public static Map<String, String> getParameters(HttpServletRequest request) {
		HashMap<String, String> params = new HashMap<>();
		Enumeration<String> paramNames = request.getParameterNames();
		while (paramNames.hasMoreElements()) {
			String paramName = paramNames.nextElement();
			String value = request.getParameter(paramName).trim();// 把参数进行trim，防止sql注入
			params.put(paramName, value);
		}
		/*
		 * Enumeration<String> attrs = request.getAttributeNames();
		 * while(attrs.hasMoreElements()) { String key = attrs.nextElement();
		 * if(!params.containsKey(key)) { String value = (String)
		 * request.getAttribute(key); params.put(key, value); } }
		 */
		return params;
	}

	@SuppressWarnings("unchecked")
	public static <T> Map<String, T> getAttributes(HttpServletRequest request) {
		Map<String, T> atrr = new HashMap<>();
		Enumeration<String> names = request.getAttributeNames();
		if (names != null) {
			while (names.hasMoreElements()) {
				String name = names.nextElement();
				atrr.put(name, (T) request.getAttribute(name));
			}
		}
		return atrr;
	}

	public static RequestDispatcher getRequestDispatcher(HttpServletRequest request, String url) {
		ServletContext context = request.getSession(true).getServletContext();
		if (context != null) {
			return context.getRequestDispatcher(url);
		} else {
			return request.getRequestDispatcher(url);
		}
	}

	public static <T> void setRequestAttributes(HttpServletRequest request, Map<String, T> map) {
		Set<String> set = map.keySet();
		Iterator<String> keys = set.iterator();
		while (keys.hasNext()) {
			String key = keys.next();
			Object value = map.get(key);

			if (key != null) {
				request.setAttribute(key, value);
			}
		}
	}

	/**
	 * Retrieves the current request servlet path. Deals with differences
	 * between servlet specs (2.2 vs 2.3+)
	 *
	 * @param request
	 *            the request
	 * @return the servlet path
	 */
	public static String getServletPath(HttpServletRequest request) {
		String servletPath = request.getServletPath();

		if (servletPath != null && !servletPath.equals("")) {
			return servletPath;
		}

		String requestUri = request.getRequestURI();
		int startIndex = request.getContextPath().equals("") ? 0 : request.getContextPath().length();
		int endIndex = request.getPathInfo() == null ? requestUri.length()
				: requestUri.lastIndexOf(request.getPathInfo());

		if (startIndex > endIndex) { // this should not happen
			endIndex = startIndex;
		}

		return requestUri.substring(startIndex, endIndex);
	}

	public static boolean isMultiPart(HttpServletRequest request) {
		String content_type = request.getContentType();
		return content_type != null && content_type.indexOf("multipart/form-data") != -1;
	}

	/**
	 * 获取访问者IP
	 *
	 * 在一般情况下使用Request.getRemoteAddr()即可，但是经过nginx等反向代理软件后，这个方法会失效。
	 *
	 * 本方法先从Header中获取X-Real-IP，如果不存在再从X-Forwarded-For获得第一个IP(用,分割)，
	 * 如果还不存在则调用Request .getRemoteAddr()。
	 *
	 * @param request
	 * @return
	 */
	public static String getIpAddr(HttpServletRequest request) {
		String ip = request.getHeader("X-Real-IP");
		if (StringUtils.isNotBlank(ip) && !"unknown".equalsIgnoreCase(ip)) {
			return ip;
		}
		ip = request.getHeader("X-Forwarded-For");
		if (StringUtils.isNotBlank(ip) && !"unknown".equalsIgnoreCase(ip)) {
			// 多次反向代理后会有多个IP值，第一个为真实IP。
			int index = ip.indexOf(',');
			if (index != -1) {
				return ip.substring(0, index);
			} else {
				return ip;
			}
		} else {
			return request.getRemoteAddr();
		}
	}

	/**
	 * 获得当的访问路径
	 *
	 * HttpServletRequest.getRequestURL+"?"+HttpServletRequest.getQueryString
	 *
	 * @param request
	 * @return
	 */
	public static String getLocation(HttpServletRequest request) {
		UrlPathHelper helper = new UrlPathHelper();
		StringBuffer buff = request.getRequestURL();
		String uri = request.getRequestURI();
		String origUri = helper.getOriginatingRequestUri(request);
		buff.replace(buff.length() - uri.length(), buff.length(), origUri);
		String queryString = helper.getOriginatingQueryString(request);
		if (queryString != null) {
			buff.append("?").append(queryString);
		}
		return buff.toString();
	}

	/**
	 * 获得请求的session id，但是HttpServletRequest#getRequestedSessionId()方法有一些问题。
	 * 当存在部署路径的时候，会获取到根路径下的jsessionid。
	 *
	 * @see HttpServletRequest#getRequestedSessionId()
	 *
	 * @param request
	 * @return
	 */
	public static String getRequestedSessionId(HttpServletRequest request) {
		HttpSession session = request.getSession();
		String sid = request.getRequestedSessionId();
		if (StringUtils.isBlank(sid)) {
			sid = session.getId();
		}
		String ctx = request.getContextPath();
		// 如果session id是从url中获取，或者部署路径为空，那么是在正确的。
		if (request.isRequestedSessionIdFromURL() || StringUtils.isBlank(ctx)) {
			return sid;
		} else {
			// 手动从cookie获取
			Cookie cookie = CookieUtil.getCookie(request, Constants.JSESSION_COOKIE);
			if (cookie != null) {
				return cookie.getValue();
			} else {
				return sid;
			}
		}

	}
}
