package com.ayou.core.commons.utils;

import java.util.Arrays;

/**
* @author AYOU
* @version 2016年3月29日 下午8:33:06
*/
public class ArrayUtil {
	
	@SuppressWarnings("unchecked")
	public static <T> T[] concatAll(T[] first, T[]... rest) {
		
		int totalLength = first.length;
		for (T[] array : rest) {
			totalLength += array.length;
		}
		
		T[] result = Arrays.copyOf(first, totalLength);
		int offset = first.length;
		for (T[] array : rest) {
			System.arraycopy(array, 0, result, offset, array.length);
			offset += array.length;
		}
		
		return result;
	}
}
