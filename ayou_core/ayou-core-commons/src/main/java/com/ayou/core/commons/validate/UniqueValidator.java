package com.ayou.core.commons.validate;

/**
 * 字段值唯一校验
 *
 * @param <T>
 */
public abstract class UniqueValidator<T> extends Validator<T> {
	public UniqueValidator(T toCheckItem, String desc) {
		super(toCheckItem, desc);
	}

	@Override
	public boolean validate() {
		T toCheckItem = getToCheckItem();
		if (existInOther(toCheckItem)) {
			isValid = false;
			errorMsg = "字段[" + desc + "]值违反唯一约束";
		}
		return isValid;
	}

	/**
	 * 判断此属性是否在其它实体中存在
	 * 
	 * @param value
	 * @return
	 */
	protected abstract boolean existInOther(T value);
}
