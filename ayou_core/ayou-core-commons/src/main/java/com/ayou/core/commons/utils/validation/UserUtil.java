package com.ayou.core.commons.utils.validation;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @category User
 * @author AYOU
 * @version 1.0
 * @since JDK 1.8
 * @date 2016年12月2日 下午12:05:28
 */
public class UserUtil {

	/**
	 * 判断当前 用户名
	 * @param userid
	 * @return
	 *  email phone username
	 */
	public static String getUserId(String userid) {
		if (userid == null || userid == "") {
			return null;
		}
		String regExp = "^[1]([3][0-9]{1}|59|58|88|89)[0-9]{8}$";
		Pattern p = Pattern.compile(regExp);
		Matcher m = p.matcher(userid);
		if (userid.contains("@")) {// 邮箱
			return "email";
		} else if (m.find()) {// 手机
			return "phone";
		} else { // 用户名
			return "username";
		}
	}
}
