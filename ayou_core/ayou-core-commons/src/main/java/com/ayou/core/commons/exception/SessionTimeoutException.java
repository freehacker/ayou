package com.ayou.core.commons.exception;

/**
 * @author AYOU
 * @version 2016年3月29日 上午2:57:29
 */
public class SessionTimeoutException extends RuntimeException{
	private static final long serialVersionUID = 1884179285349541059L;

	public SessionTimeoutException() {
		super("Session Timeout.");
	}

	public SessionTimeoutException(String msg) {
		super(msg);
	}

	public SessionTimeoutException(Throwable cause) {
		super(cause);
	}

	public SessionTimeoutException(String message, Throwable cause) {
		super(message, cause);
	}
}
