package com.ayou.core.commons.exception;

/**
 * @author AYOU
 * @version 2016年3月29日 上午2:55:41
 */
public class NoPrivilegeException extends ProjectException {
	private static final long serialVersionUID = 704135330955821848L;

	public NoPrivilegeException() {
		super("Not privilege!");
	}

	public NoPrivilegeException(String message, Throwable cause) {
		super(message, cause);
	}

	public NoPrivilegeException(String message) {
		super(message);
	}

	public NoPrivilegeException(Throwable cause) {
		super(cause);
	}
}
