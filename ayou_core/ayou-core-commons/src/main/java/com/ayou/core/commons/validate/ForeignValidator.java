package com.ayou.core.commons.validate;

/**
 * 外键约束
 * @param <T>
 */
public abstract class ForeignValidator<T> extends Validator<T> {

	public ForeignValidator(T toCheckItem, String desc) {
		super(toCheckItem, desc);
	}

	@Override
	public boolean validate() {
		T toCheckItem = getToCheckItem();
		if (!exists(toCheckItem)) {
			isValid = false;
			errorMsg = "违反外键约束:" + desc + ",value:" + toCheckItem;
		}
		return isValid;
	}

	/**
	 * 判断此属性是否存在
	 * 
	 * @param value
	 * @return
	 */
	protected abstract boolean exists(T value);

}
