package com.ayou.core.commons.param;

import com.ayou.core.commons.exception.ProjectException;

public class QueryException extends ProjectException {
	private static final long serialVersionUID = 7828548999982239467L;

	public QueryException() {
		super("查询异常.");
	}

	public QueryException(String message, Throwable cause) {
		super(message, cause);
	}

	public QueryException(String msg) {
		super(msg);
	}

	public QueryException(Throwable cause) {
		super(cause);
	}
}
