package com.ayou.core.commons.exception;

/**
* @author AYOU
* @version 2016年3月28日 下午7:53:22
*/
public class ProjectException extends RuntimeException {
	private static final long serialVersionUID = 4032987548596443646L;

	public ProjectException(String msg) {
		super(msg);
	}

	public ProjectException() {
		super();
	}

	public ProjectException(String message, Throwable cause) {
		super(message, cause);
	}

	public ProjectException(Throwable cause) {
		super(cause);
	}
}
