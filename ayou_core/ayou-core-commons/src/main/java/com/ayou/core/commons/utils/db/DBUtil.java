package com.ayou.core.commons.utils.db;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Table;

import com.ayou.core.commons.annotation.DBExclude;
import com.ayou.core.commons.utils.ArrayUtil;

/**
 * @author AYOU
 * @version 2016年4月21日 下午8:21:50
 */
public class DBUtil {
	public enum SqlTypes {
		INSERT, UPDATE, DELETE, QUERY
	}

	public static <T> String getSqlByObject(SqlTypes sqlType, T obj, List<Object> params) {
		Class<?> clz = obj.getClass();
		StringBuffer sql = new StringBuffer("");
		StringBuffer cols = new StringBuffer("");
		StringBuffer values = new StringBuffer("");

		// 如果使用了注解，则获取注解的tablename
		String tableName = "";
		if (clz.isAnnotationPresent(Table.class)) {
			Table table = (Table) clz.getAnnotation(Table.class);
			tableName = table.name();
		} else {
			System.err.println("表没有使用@Table注解,此方法只用于有@Table(name=\"xxxxx\")注解的实体");
			return null;
		}
		// 获取所有私有成员变量
		Field[] privateFields = clz.getDeclaredFields();
		Field[] publicFields = clz.getFields();
		Field[] fields = ArrayUtil.concatAll(privateFields, publicFields);

		if (sqlType == SqlTypes.INSERT) {
			sql.append("insert into ").append(tableName).append(" (");
			int index = 1;
			for (Field field : fields) {
				// 如果标注为DBExclude，则忽略
				if (field.isAnnotationPresent(DBExclude.class)) {
					DBExclude exclude = field.getAnnotation(DBExclude.class);
					// 获取元素值
					boolean isExclude = exclude.value();
					if (isExclude) {
						continue;
					}
				}

				// 设置成员变量可访问
				field.setAccessible(true);
				String fieldName = field.getName();

				// 跳过serialVersionUID
				if (fieldName.equals("serialVersionUID")) {
					continue;
				}

				Object value = null;
				// 获取value
				try {
					PropertyDescriptor pd = new PropertyDescriptor(fieldName, clz);
					Method getMethod = pd.getReadMethod(); // 获得get方法
					value = getMethod.invoke(obj);
				} catch (Exception e) {
					e.printStackTrace();
				}

				// 如果有注解，则使用注解的字段名，没有注解，则使用字段名
				if (field.isAnnotationPresent(Column.class)) {
					Column column = field.getAnnotation(Column.class);
					// 获取元素值
					fieldName = column.name();
					if (fieldName.equals("")) {
						continue;
					}
				}

				// 如果没有值，则不需要插入此字段，只需使用db默认值即可
				if (null == value || "".equals(value)) {
					continue;
				}
				if (index == 1) {
					cols.append(fieldName);
					values.append("?");
					params.add(value);
				} else {
					cols.append(", ").append(fieldName);
					values.append(", ").append("?");
					params.add(value);
				}
				index++;
			}
			sql.append(cols.toString()).append(") values(").append(values).append(")");
		}

		/**
		 * update
		 */
		if (sqlType == SqlTypes.UPDATE) {
			Long id = null;
			String uuid = null;
			sql.append("update ").append(tableName).append(" set ");
			int index = 1;
			for (Field field : fields) {
				if (field.isAnnotationPresent(DBExclude.class)) {
					DBExclude exclude = field.getAnnotation(DBExclude.class);
					boolean isExclude = exclude.value();
					if (isExclude) {
						continue;
					}
				}

				field.setAccessible(true);
				String fieldName = field.getName();

				if (fieldName.equals("serialVersionUID")) {
					continue;
				}

				Object value = null;
				try {
					PropertyDescriptor pd = new PropertyDescriptor(fieldName, clz);
					Method getMethod = pd.getReadMethod();
					value = getMethod.invoke(obj);
				} catch (Exception e) {
					e.printStackTrace();
				}

				if (field.isAnnotationPresent(Column.class)) {
					Column column = field.getAnnotation(Column.class);
					fieldName = column.name();
					if (fieldName.equals("")) {
						continue;
					}
				}

				if (fieldName.equals("id")) {
					id = (Long) value;
					continue;
				}
				if (fieldName.equals("uuid")) {
					uuid = (String) value;
					continue;
				}
				if (null == value || "".equals(value)) {
					continue;
				}

				if (index == 1) {
					sql.append(fieldName + "=?");
					params.add(value);
				} else {
					sql.append(", ").append(fieldName + "=?");
					params.add(value);

				}
				index++;
			}
			if (null != id && !"".equals(id)) {
				sql.append(" where id=?");
				params.add(id);
			} else {
				sql.append(" where id=?");
				params.add(-1);
			}
			
			if (null != uuid && !"".equals(uuid)) {
				sql.append(" where uuid=?");
				params.add(uuid);
			} else {
				sql.append(" where uuid=?");
				params.add(-1);
			}
		}

		/**
		 * delete
		 */
		if (sqlType == SqlTypes.DELETE) {
			sql.append("DELETE FROM ").append(tableName).append(" where 1=1 ");
			for (Field field : fields) {
				if (field.isAnnotationPresent(DBExclude.class)) {
					DBExclude exclude = field.getAnnotation(DBExclude.class);
					boolean isExclude = exclude.value();
					if (isExclude) {
						continue;
					}
				}

				field.setAccessible(true);
				String fieldName = field.getName();

				if (fieldName.equals("serialVersionUID")) {
					continue;
				}

				Object value = null;
				try {
					PropertyDescriptor pd = new PropertyDescriptor(fieldName, clz);
					Method getMethod = pd.getReadMethod(); // 获得get方法
					value = getMethod.invoke(obj);
				} catch (Exception e) {
					e.printStackTrace();
				}

				if (field.isAnnotationPresent(Column.class)) {
					Column column = field.getAnnotation(Column.class);
					fieldName = column.name();
					if (fieldName.equals("")) {
						continue;
					}
				}
				if (null == value || "".equals(value)) {
					continue;
				}
				sql.append("AND " + fieldName + "=? ");
				params.add(value);
			}
		}

		/**
		 * query
		 */
		if (sqlType == SqlTypes.QUERY) {
			sql.append("SELECT * FROM ").append(tableName).append(" where 1=1 ");
			for (Field field : fields) {
				if (field.isAnnotationPresent(DBExclude.class)) {
					DBExclude exclude = field.getAnnotation(DBExclude.class);
					boolean isExclude = exclude.value();
					if (isExclude) {
						continue;
					}
				}

				field.setAccessible(true);
				String fieldName = field.getName();

				if (fieldName.equals("serialVersionUID")) {
					continue;
				}

				Object value = null;
				try {
					PropertyDescriptor pd = new PropertyDescriptor(fieldName, clz);
					Method getMethod = pd.getReadMethod(); // 获得get方法
					value = getMethod.invoke(obj);
				} catch (Exception e) {
					e.printStackTrace();
				}

				if (field.isAnnotationPresent(Column.class)) {
					Column column = field.getAnnotation(Column.class);
					fieldName = column.name();
					if (fieldName.equals("")) {
						continue;
					}
				}

				if (null == value || "".equals(value)) {
					continue;
				}

				sql.append("AND " + fieldName + "=? ");
				params.add(value);
			}
		}

		return sql.toString();
	}

}
