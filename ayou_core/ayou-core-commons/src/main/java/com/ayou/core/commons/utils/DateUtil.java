package com.ayou.core.commons.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.Date;

@SuppressWarnings("unused")
public class DateUtil {

	public static final String C_TIME_PATTON_DEFAULT = "yyyy-MM-dd HH:mm:ss";

	private static final Logger logger = LoggerFactory.getLogger(DateUtil.class);

	public static final String BUNDLE_KEY = "ApplicationResources";

	private static String defaultDatePattern = null;

	public static final int WeekSpan = 7;

	public static final int SCOND_ONE_DAY = 86400; // 24*60*60*1000

	public static final int MILLISECOND_ONE_DAY = 86400000; // 24*60*60*1000

	private DateUtil() {
	}

	/**
	 * 将Timestamp类型的日期转换为系统参数定义的格式的字符串。
	 * 
	 * @param aTs_Datetime
	 *            需要转换的日期。
	 * @return 转换后符合给定格式的日期字符串
	 */
	public static String formatTime(Date aTs_Datetime) {
		return format(aTs_Datetime, C_TIME_PATTON_DEFAULT);
	}

	/**
	 * 将Date类型的日期转换为系统参数定义的格式的字符串。
	 * 
	 * @param aTs_Datetime
	 * @param as_Pattern
	 * @return
	 */
	public static String format(Date aTs_Datetime, String as_Pattern) {
		if (aTs_Datetime == null || as_Pattern == null)
			return null;
		SimpleDateFormat dateFromat = new SimpleDateFormat();
		dateFromat.applyPattern(as_Pattern);

		return dateFromat.format(aTs_Datetime);
	}

	/**
	 * This method generates a string representation of a date's date/time in
	 * the format you specify on input
	 *
	 * @param aMask
	 *            the date pattern the string is in
	 * @param aDate
	 *            a date object
	 * @return a formatted string representation of the date
	 *
	 * @see java.text.SimpleDateFormat
	 */
	public static final String getDateTime(String mask, Date date) {
		SimpleDateFormat df = null;
		String returnValue = "";

		if (date == null) {
			logger.error("aDate is null!");
		} else {
			df = new SimpleDateFormat(mask);
			returnValue = df.format(date);
		}
		return (returnValue);
	}
}
