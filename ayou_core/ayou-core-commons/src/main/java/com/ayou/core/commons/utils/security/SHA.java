package com.ayou.core.commons.utils.security;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * @category SHA256.java
 * @version 1.0
 * @author AYOU
 * @date 2016年7月2日 下午10:31:03
 */
public class SHA {
	public SHA() {
	}

	/**
	 * @param strSrc
	 * @param encName
	 *            null MD5 SHA-1 SHA-256 可加密汉子
	 * @return
	 */
	public static String encrypt(String strSrc, String encName) {
		// parameter strSrc is a string will be encrypted,
		// parameter encName is the algorithm name will be used.
		// encName dafault to "MD5"
		MessageDigest md = null;
		String strDes = null;

		byte[] bt = strSrc.getBytes();
		try {
			if (encName == null || encName.equals("")) {
				encName = "MD5";
			}
			md = MessageDigest.getInstance(encName);
			md.update(bt);
			strDes = bytes2Hex(md.digest()); // to HexString
		} catch (NoSuchAlgorithmException e) {
			System.out.println("Invalid algorithm.");
			return null;
		}
		return strDes;
	}

	public static String bytes2Hex(byte[] bts) {
		String des = "";
		String tmp = null;
		for (int i = 0; i < bts.length; i++) {
			tmp = (Integer.toHexString(bts[i] & 0xFF));
			if (tmp.length() == 1) {
				des += "0";
			}
			des += tmp;
		}
		return des;
	}
}
