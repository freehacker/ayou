package com.ayou.core.commons.utils;

/**
* @author AYOU
* @version 2016年3月28日 下午7:44:57
*/
public class Constants {
	public static final String Separator1 = "|";
	public static final String Separator2 = ":";
	public static final String Separator3 = "^";
	public static final String DEFAULT_DATE_FORMAT = "yyyy-MM-dd";
	public static final String DEFAULT_DATETIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
	
	/**
	 * cookie中的JSESSIONID名称
	 */
	public static final String JSESSION_COOKIE = "JSESSIONID";
	
	public static final Integer REG_OK = 0;
	public static final Integer REG_IS_REG = 1;
	public static final Integer REG_DB_ERR = 3;
}
