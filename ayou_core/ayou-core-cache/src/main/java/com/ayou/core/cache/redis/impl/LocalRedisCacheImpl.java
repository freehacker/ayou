package com.ayou.core.cache.redis.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.ayou.core.cache.redis.RedisCache;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

/**
 * @author AYOU
 * @version 2016年4月26日 下午10:29:25
 * @param <T>
 */

@Service
public class LocalRedisCacheImpl<T> implements RedisCache<T> {
	String add209 = "118.193.168.209";
	String add154 = "222.33.37.154";

	int port = 6379;
	int timeout = 2000;
	String password = "109ysy!";
	JedisPoolConfig jPoolConfig = new JedisPoolConfig();
	JedisPool pool = null;
	
	@Override
	public Jedis getJedis(String ip) {
		try {
			pool = new JedisPool(jPoolConfig, ip, port, timeout, password);
			return pool.getResource(); 
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public void test() {
		Jedis jedis = getJedis("118.193.168.209");
		System.out.println("有ayou：" + jedis.exists("ayou"));
		List<String> list = jedis.lrange("person", 0, 5);
		
		for (int i = 0; i < list.size(); i++) {
			System.out.println("主机209::" + list.get(i));
		}
	}

	@Override
	public boolean add(T t) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean add(List<T> list) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean delete(String key) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean delete(List<String> keys) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean update(T t) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public T get(String keyId, T t) {
		// TODO Auto-generated method stub
		return null;
	}

}
