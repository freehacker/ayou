package com.ayou.core.cache.service;

/**
 * Cache操作接口
 *
 */
public interface CacheService {
	/**
	 * 设置cache的kv
	 * @param cacheName
	 * @param key
	 * @param value
	 * @return
	 */
	public boolean set(String cacheName, Object key, Object value);

	/**
	 * 当key不存在时设置cache的kv
	 * @param cacheName
	 * @param key
	 * @param value
	 * @param type
	 * @return
	 */
	public <T> boolean setIfNull(String cacheName, Object key, Object value, Class<T> type);

	/**
	 * 当key存在时更新cache的kv
	 * @param cacheName
	 * @param key
	 * @param value
	 * @return
	 */
	public boolean update(String cacheName, Object key, Object value);

	/**
	 * 获取cache缓存的kv
	 * @param cacheName
	 * @param key
	 * @param type
	 * @return
	 */
	public <T> T get(String cacheName, Object key, Class<T> type);

	/**
	 * 清除指定cache的所有缓存
	 * @param cacheName
	 * @return
	 */
	public boolean flushAll(String cacheName);

	/**
	 * 删除指定cache缓存的kv
	 * @param cacheName
	 * @param keys key数组
	 * @return
	 */
	public boolean delete(String cacheName, Object... keys);

	/**
	 * 删除cache缓存的kv
	 * @param cacheName
	 * @param keys key数组
	 * @return
	 */
	public boolean delete(String cacheName, String... keys);
}
