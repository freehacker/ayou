package com.ayou.core.cache.service;

/**
 * @author AYOU
 * @version 2016年4月26日 下午10:32:09
 */
public interface RedisCacheService {
	String get();
	Boolean set();
}
