package com.ayou.core.cache.service.impl;

import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.support.AbstractCacheManager;

import com.ayou.core.cache.memcached.MemcachedCache;
import com.ayou.core.cache.memcached.impl.MemcachedCacheImpl;
import com.ayou.core.cache.service.CacheService;

import net.rubyeye.xmemcached.MemcachedClient;

/**
* @author AYOU
* @version 2016年3月29日 上午1:48:11
*/
public class CacheServiceImpl extends AbstractCacheManager implements CacheService {
	private static final Logger logger = LoggerFactory.getLogger(CacheServiceImpl.class);

	private Collection<MemcachedCache> caches;

	private MemcachedClient client = null;

	public CacheServiceImpl() {
	}

	public CacheServiceImpl(MemcachedClient client) {
		setClient(client);
	}

	@Override
	protected Collection<? extends MemcachedCache> loadCaches() {
		return caches;
	}

	public void setCaches(Collection<MemcachedCache> caches) {
		this.caches = caches;
	}

	public void setClient(MemcachedClient client) {
		this.client = client;
		updateCaches();
	}

	@Override
	public MemcachedCache getCache(String name) {
		checkState();

		MemcachedCache cache = (MemcachedCache) super.getCache(name);
		if(cache == null) {
			cache = new MemcachedCacheImpl(name, client);
			addCache(cache);
		}
		return cache;
	}

	private void checkState() {
		if(client == null) {
			throw new IllegalStateException("MemcacheClient must not be null.");
		}
	}

	private void updateCaches() {
		if(caches != null) {
			for(MemcachedCache cache : caches) {
				if(cache instanceof MemcachedCacheImpl) {
					MemcachedCacheImpl memcacheCache = (MemcachedCacheImpl) cache;
					memcacheCache.setClient(client);
				}
			}
		}
	}

	@Override
	public boolean set(String cacheName, Object key, Object value) {
		try {
			MemcachedCache cache = getCache(cacheName);
			cache.put(key, value);
			return true;
		} catch(Exception e) {
			logger.warn(e.getMessage(), e);
			return false;
		}
	}

	@Override
	public <T> boolean setIfNull(String cacheName, Object key, Object value, Class<T> type) {
		try {
			MemcachedCache cache = getCache(cacheName);
			if(get(cacheName, key, type) == null) {
				cache.put(key, value);
			}
			return true;
		} catch(Exception e) {
			logger.warn(e.getMessage(), e);
			return false;
		}
	}

	@Override
	public boolean update(String cacheName, Object key, Object value) {
		try {
			MemcachedCache cache = getCache(cacheName);
			return cache.replace(key, value);
		} catch(Exception e) {
			logger.warn(e.getMessage(), e);
			return false;
		}
	}

	@Override
	public <T> T get(String cacheName, Object key, Class<T> type) {
		try {
			MemcachedCache cache = getCache(cacheName);
			long start = System.currentTimeMillis();
			T value = cache.get(key, type);
			logger.debug("get[{}:{}] in {}ms" , type.getSimpleName(), key, (System.currentTimeMillis() - start));
			return value;
		} catch(Exception e) {
			return null;
		}
	}

	@Override
	public boolean flushAll(String cacheName) {
		try {
			MemcachedCache cache = getCache(cacheName);
			cache.clear();
			return true;
		} catch(Exception e) {
			logger.warn(e.getMessage(), e);
			return false;
		}
	}

	@Override
	public boolean delete(String cacheName, Object... keys) {
		try {
			MemcachedCache cache = getCache(cacheName);
			for(Object key : keys) {
				cache.evict(key);
			}
			return true;
		} catch(Exception e) {
			logger.warn(e.getMessage(), e);
			return false;
		}
	}

	@Override
	public boolean delete(String cacheName, String... keys) {
		Object[] objKeys = new Object[keys.length];
		for(int i = 0; i < keys.length; i++) {
			objKeys[i] = keys[i];
		}
		return delete(cacheName, objKeys);
	}
}
