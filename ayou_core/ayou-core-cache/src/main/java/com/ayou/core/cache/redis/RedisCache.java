package com.ayou.core.cache.redis;

import redis.clients.jedis.Jedis;
import java.util.List;

/**
 * @author AYOU
 * @version 2016年4月26日 下午10:29:00
 * @param <T>
 */
public interface RedisCache<T> {
	Jedis getJedis(String ip);

	/**
	 * 新增
	 * 
	 * @param user
	 * @return
	 */
	boolean add(T t);

	/**
	 * 批量新增 使用pipeline方式
	 * 
	 * @param list
	 * @return
	 */
	boolean add(List<T> list);

	/**
	 * 删除
	 * 
	 * @param key
	 */
	boolean delete(String key);

	/**
	 * 删除多个
	 * 
	 * @param keys
	 */
	boolean delete(List<String> keys);

	/**
	 * 修改
	 * 
	 * @param use
	 * @return
	 */
	boolean update(T t);

	/**
	 * 通过key获取
	 * 
	 * @param keyId
	 * @return
	 */
	T get(String keyId ,T t);
}
