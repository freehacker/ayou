package com.ayou.core.cache.redis;

import javax.annotation.Resource;

import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.RedisSerializer;

/**
 * @category Redis模板
 * @author AYOU
 * @version 2016年6月1日 下午1:43:33
 * @param <V>
 * @param <K>
 */
/*@ContextConfiguration(value = { "classpath:redis.xml" })*/
public abstract class AbstractBaseRedisDao<V, K> {
	
	@Resource
	public RedisTemplate<K, V> redisTemplate;
	
    /**
     * 设置redisTemplate
     * @param redisTemplate the redisTemplate to set
     */
    public void setRedisTemplate(RedisTemplate<K, V> redisTemplate) {
        this.redisTemplate = redisTemplate;
    }

    /**
     * 获取 RedisSerializer
     */
    public RedisSerializer<String> getRedisSerializer() {
        return redisTemplate.getStringSerializer();
    }
}  
