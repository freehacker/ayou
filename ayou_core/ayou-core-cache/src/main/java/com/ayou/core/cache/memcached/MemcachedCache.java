package com.ayou.core.cache.memcached;

import org.springframework.cache.Cache;

/**
 * Cache的实现之 - MemcachedCache
 *
 */
public interface MemcachedCache extends Cache {
	public boolean replace(Object key, Object value);
}
