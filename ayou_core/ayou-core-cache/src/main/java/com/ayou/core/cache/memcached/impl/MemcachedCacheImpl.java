package com.ayou.core.cache.memcached.impl;

import java.util.concurrent.TimeoutException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.support.SimpleValueWrapper;

import com.ayou.core.cache.memcached.MemcachedCache;

import net.rubyeye.xmemcached.MemcachedClient;
import net.rubyeye.xmemcached.exception.MemcachedException;

/**
* @author AYOU
* @version 2016年3月29日 上午1:46:12
*/
public class MemcachedCacheImpl implements MemcachedCache {

	private static final Logger logger = LoggerFactory.getLogger(MemcachedCacheImpl.class);

	private MemcachedClient client;

	private String name;

	private int expire;

	public MemcachedCacheImpl() {
	}

	public MemcachedCacheImpl(String name, MemcachedClient client) {
		this.client = client;
		this.name = name;
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public Object getNativeCache() {
		return this.client;
	}

	public void setClient(MemcachedClient client) {
		this.client = client;
	}

	public MemcachedClient getClient() {
		return client;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getExpire() {
		return expire;
	}

	public void setExpire(int expire) {
		this.expire = expire;
	}

	@Override
	public ValueWrapper get(Object key) {
		Object value = null;
		try {
			value = this.client.get(objectToString(key));
		} catch(Exception e) {
			logger.error("Cache get [" + key + "] failed.", e);
		}
		//logger.debug("Cache get [" + key + "]");
		return (value != null ? new SimpleValueWrapper(value) : null);
	}

	@Override
	@SuppressWarnings("unchecked")
	public <T> T get(Object key, Class<T> type) {
		Object value = null;
		try {
			value = this.client.get(objectToString(key));
		} catch(Exception e) {
			logger.error("Cache get [" + key + "] failed.", e);
		}
		if(type != null && !type.isInstance(value)) {
			throw new IllegalStateException("Cached value is not of required type [" + type.getName() + "]: " + value);
		}
		//logger.debug("Cache get [" + key + "]");
		return (T) value;
	}

	@Override
	public void put(Object key, Object value) {
		try {
			this.client.set(objectToString(key), getExpire(), value);
			//logger.debug("Cache put [" + key + "]");
		} catch(TimeoutException | InterruptedException | MemcachedException e) {
			throw new IllegalStateException("MemCached cannot set key [" + objectToString(key) + "]", e);
		}
	}

	@Override
	public ValueWrapper putIfAbsent(Object key, Object value) {
		try {
			Object existValue = this.client.get(objectToString(key));
			if(existValue == null) {
				existValue = value;
				this.client.set(objectToString(key), getExpire(), existValue);
			}
			//logger.debug("Cache put [" + key + "]");
			return new SimpleValueWrapper(existValue);
		} catch(TimeoutException | InterruptedException | MemcachedException e) {
			throw new IllegalStateException("MemCached cannot set key [" + objectToString(key) + "]", e);
		}
	}

	@Override
	public boolean replace(Object key, Object value) {
		try {
			//logger.debug("Cache replace [" + key + "]");
			return this.client.replace(objectToString(key), getExpire(), value);
		} catch(TimeoutException | InterruptedException | MemcachedException e) {
			throw new IllegalStateException("MemCached cannot set key [" + objectToString(key) + "]", e);
		}
	}

	@Override
	public void evict(Object key) {
		try {
			this.client.delete(objectToString(key));
		} catch(TimeoutException | InterruptedException | MemcachedException e) {
			throw new IllegalStateException("MemCached cannot delete key [" + objectToString(key) + "]", e);
		}
	}

	@Override
	public void clear() {
		try {
			this.client.flushAll();
		} catch(TimeoutException | InterruptedException | MemcachedException e) {
			throw new IllegalStateException("MemCached cannot clear all cache", e);
		}
	}

	private static String objectToString(Object object) {
		if(object == null) {
			return null;
		} else if(object instanceof String) {
			return (String) object;
		} else {
			return object.toString();
		}
	}

}
