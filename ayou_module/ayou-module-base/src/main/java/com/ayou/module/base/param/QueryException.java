package com.ayou.module.base.param;

import com.ayou.core.commons.exception.ProjectException;

@SuppressWarnings("serial")
public class QueryException extends ProjectException {

	public QueryException() {
		super("查询异常.");
	}

	public QueryException(String message, Throwable cause) {
		super(message, cause);
	}

	public QueryException(String msg) {
		super(msg);
	}

	public QueryException(Throwable cause) {
		super(cause);
	}
}
