package com.ayou.module.base.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.util.List;
import java.util.Map;

/**
 * @author AYOU
 * @version 2016年3月28日 下午8:26:46
 */
public interface JdbcDao {

	public static final int DB_TYPE_MYSQL = 1;

	public static final int DB_TYPE_SQLSERVER = 2;

	public static final int DB_TYPE_SQLLITE = 3;

	/**
	 * jdbc查询结果的遍历器
	 */
	public static interface JdbcResultVisitor {
		/**
		 * 处理一行查询结果，无需调用next方法
		 * 
		 * @param rs
		 */
		public void visit(ResultSet rs);

		/**
		 * 游标执行到底后执行的工作
		 */
		public default void afterNoNext() {

		}
	}

	/**
	 * 获取数据库连接，以便用来进行精细的jdbc操作. 注，必须使用releaseJdbcConnection方法释放获得的连接，而非close
	 * 
	 * @return
	 */
	public Connection getJdbcConnection();

	/**
	 * 释放连接给连接池
	 */
	public void releaseJdbcConnection(Connection conn);

	/**
	 * 以原生JDBC的方式进行查询，高效且便于无用的查询结果快速进入GC
	 * 
	 * @param sql
	 * @param rsVisitor
	 */
	public void queryWithJdbc(JdbcResultVisitor rsVisitor, String sql, Object... args);

	/**
	 * 执行SQL，不带参数，没有返回值
	 * 
	 * @param sql
	 */
	public void execute(String sql);

	/**
	 * 执行SQL，带参数，并且返回受影响的行数
	 * 
	 * @param sql
	 * @param args
	 * @return
	 */
	public int update(String sql, Object... args);

	/**
	 * 批处理
	 * 
	 * @param sql
	 * @param batchArgs
	 * @return
	 */
	public int[] batchUpdate(String sql, List<Object[]> batchArgs);

	/**
	 * 返回单个字段，参数baseType只能是JDK的基本类型
	 * 
	 * @param baseType
	 * @param sql
	 * @param args
	 * @return
	 */
	public <T> T queryForBaseType(Class<T> baseType, String sql, Object... args);

	/**
	 * 查询并返回单个自定义对象
	 * 
	 * @param objectType
	 * @param sql
	 * @param args
	 * @return
	 */
	public <T> T queryForObject(Class<T> objectType, String sql, Object... args);

	/**
	 * 查询并返回多个自定义对象
	 * 
	 * @param objectType
	 * @param sql
	 * @param args
	 * @return
	 */
	public <T> List<T> queryForObjectList(Class<T> objectType, String sql, Object... args);

	/**
	 * 查询并返回多个原始数据Map
	 * 
	 * @param sql
	 * @param args
	 * @return
	 */
	public List<Map<String, Object>> queryForMapList(String sql, Object... args);

	/**
	 * 对Sql查询的对象计数
	 * 
	 * @param sql
	 * @param args
	 * @return
	 */
	public int counts(String sql, Object... args);

	public <T> int saveOne(T obj);

}
