package com.ayou.module.base;

/**
 * 版本敏感接口
 */
public interface VersionAware {
	public Integer getVersion();

	public void setVersion(Integer version);
}
