package com.ayou.module.base;

import java.util.Date;

/**
 * 日期敏感接口
 * @author sb
 *
 */
public interface DateAware {
	public Date getCreateDate();

	public void setCreateDate(Date createDate);

	public Date getModifyDate();

	public void setModifyDate(Date modifyDate);
}
