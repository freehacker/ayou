package com.ayou.module.base.param;

import java.io.Serializable;
import java.util.List;

/**
 * 封装一组查询参数，返回一个build好的表达式
 *
 */
@SuppressWarnings("serial")
public abstract class QueryParams implements Serializable {

	/**
	 * 模糊匹配关键字
	 */
	private String q;

	/**
	 * 精确匹配关键字
	 */
	private String k;

	private Long id;

	private String uuid;

	/**
	 * 返回模糊匹配的字段
	 */
	protected String[] getQFields() {
		return null;
	}

	/**
	 * 返回精确匹配的字段
	 */
	protected String[] getKFields() {
		return null;
	}

	/**
	 * 数据权限表达式
	 */
	protected List<String> dataAuthorityExpressions;

	public void setDataAuthorityExpressions(List<String> dataAuthorityExpressions) {
		this.dataAuthorityExpressions = dataAuthorityExpressions;
	}

	public String getQ() {
		return q;
	}

	public void setQ(String q) {
		this.q = q;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getK() {
		return k;
	}

	public void setK(String k) {
		this.k = k;
	}
}
