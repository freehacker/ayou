package com.ayou.module.base.entity;

import java.io.Serializable;

/**
 * 联合主键的实体标识接口
 * @author sb
 *
 */
public interface CompositePkEntity extends Serializable, Cloneable {
}
