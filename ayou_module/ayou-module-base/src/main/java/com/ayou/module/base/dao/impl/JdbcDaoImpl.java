package com.ayou.module.base.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ayou.core.commons.exception.DbException;
import com.ayou.core.commons.utils.db.DBUtils;
import com.ayou.core.commons.utils.db.DBUtils.SqlTypes;
import com.ayou.module.base.dao.JdbcDao;


/**
* @author AYOU
* @version 2016年3月28日 下午8:30:49
*/
@Repository
public class JdbcDaoImpl extends JdbcDaoSupport implements JdbcDao{

	protected final Logger logger = LoggerFactory.getLogger(getClass());

	public JdbcDaoImpl() {
	}

	public JdbcDaoImpl(DataSource dataSource) {
		super.setDataSource(dataSource);
	}

	@Resource(name = "dataSource")
	private void setDataSourceOverride(DataSource dataSource) {
		super.setDataSource(dataSource);
	}

	@Override
	public Connection getJdbcConnection() {
		return getConnection();
	}

	@Override
	public void releaseJdbcConnection(Connection conn) {
		releaseConnection(conn);
	}

	@Override
	public void execute(String sql) {
		if(logger.isDebugEnabled()) {
			logger.debug("executeSQLExecute:{}", sql);
		}
		logger.info("executeSQLExecute:{}", sql);
		try {
			getJdbcTemplate().execute(sql);
		} catch(DataAccessException e) {
			throw new DbException(e);
		}
	}

	@Override
	public int update(String sql, Object... args) {
		if(logger.isDebugEnabled()) {
			logger.debug("executeSQLUpdate:{}", sql);
		}
		logger.info("executeSQLUpdate:{}", sql);
		try {
			return getJdbcTemplate().update(sql, args);
		} catch(DataAccessException e) {
			throw new DbException(e);
		}
	}

	@Override
	public int[] batchUpdate(String sql, List<Object[]> batchArgs) {
		if(logger.isDebugEnabled()) {
			logger.debug("executeSQLUpdate:{}", sql);
		}
		logger.info("executeSQLUpdate:{}", sql);
		Connection conn = getConnection();
		PreparedStatement pstm = null;
		try {
			conn.setAutoCommit(false);
			pstm = conn.prepareStatement(sql);
			for(Object[] args : batchArgs) {
				int i = 1;
				for(Object arg : args) {
					pstm.setObject(i, arg);
					i++;
				}
				pstm.addBatch();
			}
			int[] res = pstm.executeBatch();
			conn.commit();
			//conn.setAutoCommit(true);
			return res;
		} catch(Exception e) {
			e.printStackTrace();
			throw new DbException(e);
		} finally {
			if(null != pstm) {
				try {
					pstm.close();
				} catch(Exception e) {
				}
			}
			if(null != conn) {
				try {
					releaseConnection(conn);
				} catch(Exception e) {
				}
			}
		}
	}

	@Override
	public <T> T queryForBaseType(Class<T> baseType, String sql, Object... args) {
		logger.info("queryForBaseType:{}", sql);
		return getJdbcTemplate().queryForObject(sql, args, baseType);
	}

	@Override
	public <T> T queryForObject(Class<T> objectType, String sql, Object... args) {
		logger.info("objectType:{}", sql);
		return getJdbcTemplate().queryForObject(sql, args, new BeanPropertyRowMapper<T>(objectType));
	}

	@Override
	public <T> List<T> queryForObjectList(Class<T> objectType, String sql, Object... args) {
		logger.info("queryForObjectList:{}", sql);
		try {
			return getJdbcTemplate().query(sql, args, new BeanPropertyRowMapper<T>(objectType));
		} catch(DataAccessException e) {
			throw new DbException(e);
		}
	}

	@Override
	public List<Map<String, Object>> queryForMapList(String sql, Object... args) {
		if(logger.isDebugEnabled()) {
			logger.debug("queryForList:{}", sql);
		}
		logger.info("queryForMapList:{}", sql);
		
		try {
			return getJdbcTemplate().queryForList(sql, args);
		} catch(DataAccessException e) {
			throw new DbException(e);
		}
	}

	@Override
	public int counts(String sql, Object... args) {
		String countSql = "select count(1) from(" + sql + ") t";
		logger.info("executeSQLcounts:{}", countSql);
		return queryForBaseType(Integer.class, countSql, args);
	}

	@SuppressWarnings("unused")
	private String buildLimitSql(String sql, int start, int limit) {
		logger.info("executeSQLbuildLimitSql:{}", sql);
		switch(getDbType()) {
			case DB_TYPE_MYSQL:
			case DB_TYPE_SQLLITE:
				return sql + " limit " + start + ',' + limit;
			case DB_TYPE_SQLSERVER:
				return "";
			default:
				throw new DbException("不支持的数据库类型[" + getDbType() + "]");
		}
	}

	private int getDbType() {
		try {
			String driverName = getDataSource().getConnection().getMetaData().getDriverName();
			if(StringUtils.containsIgnoreCase(driverName, "MySQL")) {
				return DB_TYPE_MYSQL;
			}
			if(StringUtils.containsIgnoreCase(driverName, "sqlserver")) {
				return DB_TYPE_SQLSERVER;
			}
			if(StringUtils.containsIgnoreCase(driverName, "sqlite")) {
				return DB_TYPE_SQLLITE;
			}
			throw new DbException("不支持的数据库类型[" + driverName + "]");
		} catch(SQLException e) {
			throw new DbException("数据库异常]", e);
		}
	}

	@Override
	public void queryWithJdbc(JdbcResultVisitor rsVisitor, String sql, Object... args) {
		Connection conn = getConnection();
		PreparedStatement pstm = null;
		ResultSet rs = null;
		try {
			pstm = conn.prepareStatement(sql);
			int i = 1;
			for(Object arg : args) {
				pstm.setObject(i, arg);
				i++;
			}
			rs = pstm.executeQuery();
			while(rs.next()) {
				rsVisitor.visit(rs);
			}
		} catch(Exception e) {
			e.printStackTrace();
			throw new DbException(e);
		} finally {
			try {
				rsVisitor.afterNoNext();
			} catch(Exception e) {
			}
			if(null != rs) {
				try {
					rs.close();
				} catch(Exception e) {
				}
			}
			if(null != pstm) {
				try {
					pstm.close();
				} catch(Exception e) {
				}
			}
			if(null != conn) {
				try {
					releaseConnection(conn);
				} catch(Exception e) {
				}
			}
		}
	}
	
	@Override
	@Transactional
	public <T> int saveOne(T obj){
		String sql = DBUtils.getSqlByObject(SqlTypes.INSERT, obj);
		if(logger.isDebugEnabled()) {
			logger.debug("executeSQLUpdate:{}", sql);
		}
		KeyHolder key = new GeneratedKeyHolder();
		getJdbcTemplate().update(new PreparedStatementCreator() {
			@Override
			public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
				PreparedStatement ps = connection.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
				return ps;
			}
		}, key);
		
		return key.getKey().intValue();
	}
}
