package com.ayou.module.identity.dao;

import com.baomidou.kisso.Token;

/**
 * @category TokenCacheDao.java
 * @version 1.0
 * @author AYOU
 * @date 2016年7月24日 下午1:23:44
 */
public interface TokenCacheDao {

	public Token get(String key, int expires);

	public boolean set(String key, Token token, int expires);

	public boolean delete(String key);

}
