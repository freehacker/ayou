package com.ayou.module.identity.service;

import java.io.File;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.jstack.sendcloud4j.SendCloud;
import io.jstack.sendcloud4j.mail.Email;
import io.jstack.sendcloud4j.mail.Result;
import io.jstack.sendcloud4j.mail.Substitution;

/**
 * @category EmailService
 * @author AYOU
 * @version 1.0
 * @since JDK 1.8
 * @date 2016年12月2日 下午1:07:35
 */
public class EmailService {
	static Logger logger = LoggerFactory.getLogger(EmailService.class);
	static String SENDCLOUD_EAMIL_API_KEY = "euciFNT7mqhetSbM";
	static String SENDCLOUD_EAMIL_API_USER = "qtduvip";// qtduemail

	public static SendCloud getSendCloud() {
		return SendCloud.createWebApi(SENDCLOUD_EAMIL_API_USER, SENDCLOUD_EAMIL_API_KEY);
	}

	public void templ() {
		SendCloud webapi = getSendCloud();
		@SuppressWarnings("rawtypes")
		Email email = Email.template("qtdu_atcive").from("vip@qtdu.vip").fromName("齐天都")
				.substitutionVars(Substitution.sub().set("name", "世友").set("url", "https://passport.qtdu.com/register.html"))
				.attachment(new File("C:/Users/AYOU/Desktop/qtdu_log.png")).to("550244300@qq.com");

		Result result = webapi.mail().send(email);
		logger.info(result.isSuccess() + "");
	}

	@SuppressWarnings("rawtypes")
	public static void simpSend(String temp, String send_email, String url) {
		SendCloud webapi = getSendCloud();
		Email email = Email.template(temp).from("vip@qtdu.vip").fromName("齐天都").substitutionVars(Substitution.sub().set("name", send_email).set("url", url)).to(send_email);
		Result result = webapi.mail().send(email);
		logger.info(result.isSuccess() + "");
	}
	
	@SuppressWarnings("rawtypes")
	public static void bind(String send_email, String url) {
		SendCloud webapi = getSendCloud();
		Email email = Email.template("qtdu_bind").from("vip@qtdu.vip").fromName("齐天都").substitutionVars(Substitution.sub().set("name", "send_email").set("url", url)).to(send_email);
		Result result = webapi.mail().send(email);
		logger.info(result.isSuccess() + "");
	}
	
	public static void main(String[] args) {
		simpSend("qtdu_atcive","550244300@qq.com","https://passport.qtdu.com/register.html");
		simpSend("qtdu_bind","550244300@qq.com","https://passport.qtdu.com/register.html");
		simpSend("qtdu_getpass","550244300@qq.com","https://passport.qtdu.com/register.html");
		simpSend("qtdu_invitation","550244300@qq.com","https://passport.qtdu.com/register.html");
	}
}
