package com.ayou.module.identity.filter;

import java.io.IOException;
import java.net.HttpCookie;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ayou.core.commons.utils.UUIDUtil;
import com.ayou.core.commons.utils.security.DES;
import com.ayou.core.commons.utils.security.MD5;
import com.ayou.core.commons.utils.security.SHA;

/**
 * @author AYOU
 * @version 2016年3月29日 下午1:40:29
 */
@SuppressWarnings("unused")
//@WebFilter("/*")
public class LoginFilter implements Filter {
	private Logger logger = LoggerFactory.getLogger(LoginFilter.class);
	private ServletContext servletContext;
	private String login_page = "/login.html";
	private String current_url;

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		servletContext = filterConfig.getServletContext();
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse rep = (HttpServletResponse) response;
		HttpSession session = req.getSession();

		current_url = req.getServletPath();
		// String cur = req.getRequestURI();
		// logger.info("当前地址：" + cur);
		logger.info("当前sessionID->" + session.getId());
		String uuid = UUIDUtil.generate();
		// uuid = MD5.crypt(uuid);
		uuid = SHA.encrypt(uuid, "SHA-256");
		logger.error("NULL---"+SHA.encrypt(uuid, null));
		logger.error("MD5---"+SHA.encrypt(uuid, "MD5"));
		logger.error("SHA-1---"+SHA.encrypt(uuid, "SHA-1"));
		logger.error("SHA2---"+SHA.encrypt(uuid, "SHA-256"));
		logger.info("UUID----->" + uuid);
		session.setAttribute("u_token", uuid);
		Cookie cookie = new Cookie("u_token", uuid);
		cookie.setPath(".qtdu.com");
		rep.addCookie(cookie);
		/*
		 * ` System.out.println("当前路径"+current_url);
		 * System.out.println("排除路径"+common_page);
		 * System.out.println("验证路径"+validate_page);
		 */
		/*
		 * if (true) { chain.doFilter(request, response); return; }
		 */

		if (current_url.matches(".*/view/.*")) {
			logger.info("验证session成功：" + current_url);
			chain.doFilter(request, response);
			return;
		}
		if (session.getAttribute("user") != null) {
			logger.info("验证session成功：" + session.getAttribute("user"));
			chain.doFilter(request, response);
			return;
		} else {
			logger.info("准备跳转登陆：" + login_page);
			logger.info("当前：" + current_url);
			if (!current_url.equals(login_page)) {
				rep.sendRedirect(login_page + "?reDirect=" + current_url);
				return;
			}
			rep.sendRedirect(login_page);
		}

	}

	@Override
	public void destroy() {

	}
}
