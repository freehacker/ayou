package com.ayou.module.identity.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.ayou.core.commons.annotation.DBExclude;
import com.ayou.module.base.entity.BaseEntity;
import com.ayou.module.identity.UserInfo;

/**
 * @author AYOU
 * @version 2016年5月10日 下午10:28:08
 */

@Entity
@Table(name = "user")
public class User extends BaseEntity implements UserInfo {
	@DBExclude
	private static final long serialVersionUID = 9019593178501374702L;

	private Integer type;
	private String email;
	private String email_code;
	private String phone;
	private String phone_code;
	private String name;
	private String description;
	private String password;
	private Integer active;
	private Integer sex;
	private Date birthday;
	private Integer superUser;
	private String headUrl;
	
	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getEmail_code() {
		return email_code;
	}

	public void setEmail_code(String email_code) {
		this.email_code = email_code;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getPhone_code() {
		return phone_code;
	}

	public void setPhone_code(String phone_code) {
		this.phone_code = phone_code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Integer getActive() {
		return active;
	}

	public void setActive(Integer active) {
		this.active = active;
	}

	public Integer getSex() {
		return sex;
	}

	public void setSex(Integer sex) {
		this.sex = sex;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public Integer getSuperUser() {
		return superUser;
	}

	public void setSuperUser(Integer superUser) {
		this.superUser = superUser;
	}

	public String getHeadUrl() {
		return headUrl;
	}

	public void setHeadUrl(String headUrl) {
		this.headUrl = headUrl;
	}

	@Override
	public Integer atSuperUser() {
		return getSuperUser() == null ? 0 : getSuperUser();
	}
}
