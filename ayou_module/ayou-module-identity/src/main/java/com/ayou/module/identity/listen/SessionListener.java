package com.ayou.module.identity.listen;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @category 同步session和redis
 * @version 1.0
 * @author AYOU
 * @date 2016年7月1日 下午4:47:14
 */
public class SessionListener implements HttpSessionListener {
	Logger logger = LoggerFactory.getLogger(this.getClass());

	@Override
	public void sessionCreated(HttpSessionEvent se) {
		// 把session放到redis
		logger.info("创建SessionID->" + se.getSession().getId());
	}

	@Override
	public void sessionDestroyed(HttpSessionEvent se) {
		logger.info("销毁SessionID->" + se.getSession().getId());
		//logger.info("del--->"+se.getSession().getAttribute("u_token").toString());
		// 把session从redis中移除
	}

}
