package com.ayou.module.identity.dao.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.stereotype.Repository;

import com.ayou.core.cache.redis.AbstractBaseRedisDao;
import com.ayou.core.commons.json.JsonUtil;
import com.ayou.module.identity.dao.TokenCacheDao;
import com.baomidou.kisso.Token;

/**
 * @category TokenCacheDaoImpl.java
 * @version 1.0
 * @author AYOU
 * @date 2016年7月23日 下午8:53:15
 */
@Repository
public class TokenCacheDaoImpl extends AbstractBaseRedisDao<String, Token> implements TokenCacheDao{
	protected final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Override
	public Token get(String key, int expires) {
		String sk = "sso_token_" + key;
		return redisTemplate.execute(new RedisCallback<Token>() {
			public Token doInRedis(RedisConnection connection) throws DataAccessException {
				RedisSerializer<String> serializer = getRedisSerializer();
				byte[] k = serializer.serialize(sk);
				byte[] value = connection.get(k);
				if (value == null) {
					return null;
				}
				String u = serializer.deserialize(value);
				return (Token) JsonUtil.fromJson(u, Token.class);
			}
		});
	}

	@Override
	public boolean set(String key, Token token, int expires) {
		String sk = "sso_token_" + key;
		return redisTemplate.execute(new RedisCallback<Boolean>() {
			public Boolean doInRedis(RedisConnection connection) throws DataAccessException {
				RedisSerializer<String> serializer = getRedisSerializer();
				byte[] k = serializer.serialize(sk);
				byte[] value = serializer.serialize(JsonUtil.toJson(token));
				try {
					connection.setEx(k, expires, value);
					return true;
				} catch (Exception e) {
					e.printStackTrace();
					return false;
				}
			}
		});
	}

	@Override
	public boolean delete(String key) {
		String sk = "sso_token_" + key;
		return redisTemplate.execute(new RedisCallback<Boolean>() {
			public Boolean doInRedis(RedisConnection connection) throws DataAccessException {
				RedisSerializer<String> serializer = getRedisSerializer();
				byte[] k = serializer.serialize(sk);
				try {
					return connection.del(k) >= 1 ? true : false;
				} catch (Exception e) {
					e.printStackTrace();
					return false;
				}
			}
		});
	}

}
