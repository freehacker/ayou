package com.ayou.module.identity.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.stereotype.Repository;
import org.springframework.util.Assert;

import com.ayou.core.cache.redis.AbstractBaseRedisDao;
import com.ayou.core.commons.json.JsonUtil;
import com.ayou.module.base.entity.BaseEntity;
import com.ayou.module.identity.dao.IdentityDao;

/**
 * @category 登录状态
 * @author AYOU
 * @version 2016年5月10日 下午10:29:46
 * @param <T>
 */
@Repository
public class IdentityDaoImpl<T extends BaseEntity> extends AbstractBaseRedisDao<String, T> implements IdentityDao<T> {
	protected final Logger logger = LoggerFactory.getLogger(this.getClass());

	/**
	 * 新增
	 * 
	 * @param user
	 * @return
	 */
	public boolean add(final T t) {
		if (hasKey(t.getUuid())) {
			logger.info("key:" + t.getUuid() + " is already exists, will be replaced... ");
		}
		boolean result = redisTemplate.execute(new RedisCallback<Boolean>() {
			public Boolean doInRedis(RedisConnection connection) throws DataAccessException {
				RedisSerializer<String> serializer = getRedisSerializer();
				byte[] key = serializer.serialize(t.getUuid());
				byte[] value = serializer.serialize(JsonUtil.toJson(t));
				// return connection.setNX(key, name);
				try {
					connection.setEx(key, 60, value);
					return hasKey(t.getUuid());
				} catch (Exception e) {
					e.printStackTrace();
					return false;
				}
			}
		});
		return result;
	}

	public boolean add(final T t, Long time) {
		if (hasKey(t.getUuid())) {
			logger.info("key:" + t.getUuid() + " is already exists, will be replaced... ");
		}
		boolean result = redisTemplate.execute(new RedisCallback<Boolean>() {
			public Boolean doInRedis(RedisConnection connection) throws DataAccessException {
				RedisSerializer<String> serializer = getRedisSerializer();
				byte[] key = serializer.serialize(t.getUuid());
				byte[] value = serializer.serialize(JsonUtil.toJson(t));
				// return connection.setNX(key, name);
				try {
					connection.setEx(key, time, value);
					if (hasKey(t.getUuid())) {
						return true;
					} else {
						return false;
					}
				} catch (Exception e) {
					e.printStackTrace();
					return false;
				}
			}
		});
		return result;
	}
	
	/**
	 * 添加信息 
	 * 存储格式 k：user_uuid  v: T
	 * @param t
	 * @param name
	 * @param time
	 * @return
	 */
	@Override
	public boolean add(final T t, String name, Long time) {
		String k = name + "_" + t.getUuid();
		if (hasKey(k)) {
			logger.info("key:" + t.getUuid() + " is already exists, will be replaced... ");
		}
		logger.info("uuid:"+t.getUuid()+"--name:"+name+"--time:"+time+"-----k:"+k);
		boolean result = redisTemplate.execute(new RedisCallback<Boolean>() {
			public Boolean doInRedis(RedisConnection connection) throws DataAccessException {
				RedisSerializer<String> serializer = getRedisSerializer();
				byte[] key = serializer.serialize(k);
				byte[] value = serializer.serialize(JsonUtil.toJson(t));
				logger.info("sava->key:"+key+",value:"+value);
				try {
					connection.setEx(key, time, value);
					if (hasKey(t.getUuid())) {
						return true;
					} else {
						return false;
					}
				} catch (Exception e) {
					e.printStackTrace();
					return false;
				}
			}
		});
		return result;
	}

	/**
	 * 批量新增 使用pipeline方式
	 * 
	 * @param list
	 * @return
	 */
	public boolean add(final List<T> list) {
		Assert.notEmpty(list);
		boolean result = redisTemplate.execute(new RedisCallback<Boolean>() {
			public Boolean doInRedis(RedisConnection connection) throws DataAccessException {
				RedisSerializer<String> serializer = getRedisSerializer();
				for (T t : list) {
					byte[] key = serializer.serialize(t.getUuid());
					byte[] name = serializer.serialize(JsonUtil.toJson(t));
					connection.setNX(key, name);
				}
				return true;
			}
		}, false, true);
		return result;
	}
	
	/**
	 * 删除k
	 * @param k
	 * @return
	 */
	@Override
	public boolean del(String name, String k) {
		String kv = name + "_" + k;
		return redisTemplate.execute(new RedisCallback<Boolean>() {
			public Boolean doInRedis(RedisConnection connection) throws DataAccessException {
				RedisSerializer<String> serializer = getRedisSerializer();
				byte[] key = serializer.serialize(kv);
				try {
					return connection.del(key) >= 1 ? true : false;
				} catch (Exception e) {
					e.printStackTrace();
					return false;
				}
			}
		});
	}

	/**
	 * 删除
	 * 
	 * @param key
	 */
	public boolean delete(String key) {
		try {
			List<String> list = new ArrayList<String>();
			list.add(key);
			delete(list);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * 删除多个
	 * 
	 * @param keys
	 */
	@SuppressWarnings("unchecked")
	public boolean delete(List<String> keys) {
		try {
			redisTemplate.delete((T) keys);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * 修改
	 * 
	 * @param user
	 * @return
	 */
	public boolean update(final T t) {
		String key = t.getUuid();
		if (!hasKey(key)) {
			logger.info("key:" + t.getUuid() + " is not exists... ");
			// throw new NullPointerException("key not exists..., key = " +
			// key);
		}
		boolean result = redisTemplate.execute(new RedisCallback<Boolean>() {
			public Boolean doInRedis(RedisConnection connection) throws DataAccessException {
				RedisSerializer<String> serializer = getRedisSerializer();
				byte[] key = serializer.serialize(t.getUuid());
				byte[] value = serializer.serialize(JsonUtil.toJson(t));
				byte[] is = connection.get(key);
				if (is == null) {
					return null;
				} else {
					try {
						connection.setEx(key, 60, value);
						return true;
					} catch (Exception e) {
						e.printStackTrace();
						return false;
					}
				}
			}
		});
		return result;
	}

	/**
	 * 通过key获取 <br>
	 * 
	 * @param keyId
	 * @return
	 * @return
	 */
	public T get(final String keyId, T t) {
		T t1 = redisTemplate.execute(new RedisCallback<T>() {
			@SuppressWarnings("unchecked")
			public T doInRedis(RedisConnection connection) throws DataAccessException {
				RedisSerializer<String> serializer = getRedisSerializer();
				byte[] key = serializer.serialize(keyId);
				byte[] value = connection.get(key);
				if (value == null) {
					return null;
				}
				String u = serializer.deserialize(value);
				logger.info("getKey:" + keyId + " --- " + u);
				return (T) JsonUtil.fromJson(u, t.getClass());
			}
		});
		return t1;
	}

	public boolean hasKey(final String uuid) {
		boolean is = redisTemplate.execute(new RedisCallback<Boolean>() {
			public Boolean doInRedis(RedisConnection connection) throws DataAccessException {
				RedisSerializer<String> serializer = getRedisSerializer();
				byte[] key = serializer.serialize(uuid);
				byte[] value = connection.get(key);
				if (value == null) {
					return false;
				} else {
					return true;
				}
			}
		});
		return is;
	}
}