package com.ayou.module.identity.dao;

import java.util.List;

import com.ayou.module.base.entity.BaseEntity;

/**
 * @category 登录状态
 * @author AYOU
 * @version 2016年5月10日 下午10:29:32
 * @param <T>
 */
public interface IdentityDao<T extends BaseEntity> {

	/**
	 * 新增
	 * 
	 * @param user
	 * @return
	 */
	boolean add(T t);
	
	/**
	 * 新增分离 k v
	 * 例如 user_uuid
	 * @param t
	 * @param name
	 * @param time
	 * @return
	 */
	 
	boolean add(T t, String name, Long time);

	/**
	 * 批量新增 使用pipeline方式
	 * 
	 * @param list
	 * @return
	 */
	boolean add(List<T> list);
	
	/**
	 * 删除分类key
	 * 例如 del user_uuid
	 * @param name
	 * @param k
	 * @return
	 */
	boolean del(String name, String k);

	/**
	 * 删除
	 * 
	 * @param key
	 */
	boolean delete(String key);

	/**
	 * 删除多个
	 * 
	 * @param keys
	 */
	boolean delete(List<String> keys);

	/**
	 * 修改
	 * 
	 * @param use
	 * @return
	 */
	boolean update(T t);

	/**
	 * 通过key获取
	 * 
	 * @param keyId
	 * @return
	 */
	T get(String keyId ,T t);
}
