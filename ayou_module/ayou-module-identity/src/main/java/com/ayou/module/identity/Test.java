package com.ayou.module.identity;

import java.io.Serializable;

import javax.annotation.Resource;

import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.dao.DataAccessException;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.RedisTemplate;

import com.ayou.module.identity.entity.User;

/**
 * @category Main.java
 * @author AYOU
 * @version 2016年6月1日 下午3:28:36
 */
public class Test {
	@Resource
	static RedisTemplate<Serializable, Serializable> redisTemplate;
	
	public static void main(String[] args) {
		@SuppressWarnings("resource")
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(new String[] { "appctx-module-identity.xml" });
		context.start();
		
		System.out.println("Loading ClassPathXmlApplicationContext is ok...");
		
		@SuppressWarnings("unchecked")
		RedisTemplate<Serializable, Serializable> redisTemplate = (RedisTemplate<Serializable, Serializable>) context.getBean("redisTemplate");
		
		User user = new User();
		String uuid = user.generateUuid();
		user.setUuid(uuid);
		user.setName("AYOU");
		//增
		redisTemplate.execute(new RedisCallback<Object>() {
			@Override
			public Object doInRedis(RedisConnection connection) throws DataAccessException {
				connection.setEx(
					redisTemplate.getStringSerializer().serialize("user."+user.getUuid()),
					60 * 60 * 8, //秒
					redisTemplate.getStringSerializer().serialize(user.getName())
				);
				System.out.println(user.getUuid()+"\t"+user.getName());
				System.out.println("add ok...");
				return true;
			}
		});
		
		//查
		redisTemplate.execute(new RedisCallback<User>() {
	        @Override
	        public User doInRedis(RedisConnection connection) throws DataAccessException {
	            byte[] key = redisTemplate.getStringSerializer().serialize("user." + uuid);
	            if (connection.exists(key)) {
	                byte[] value = connection.get(key);
	                String name = redisTemplate.getStringSerializer().deserialize(value);
	                System.out.println();
	                System.out.println("key:"+key);
	                System.out.println("value:"+value);
	                System.out.println("uuid:"+uuid);
	                System.out.println("name:"+name);
	            }
	            System.out.println("select ok...");
	            return null;
	        }
	    });
		
		//删
		redisTemplate.execute(new RedisCallback<User>() {
			@Override
			public User doInRedis(RedisConnection connection) throws DataAccessException {
				byte[] key = redisTemplate.getStringSerializer().serialize("user." + uuid);
				if (connection.exists(key)) {
					connection.del(redisTemplate.getStringSerializer().serialize("user." + uuid));
				}
				System.out.println();
				System.out.println("delete ok...");
				return null;
			}
		});
	}
}
