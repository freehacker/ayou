package com.ayou.module.identity;

import java.io.IOException;

import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @category Loading
 * @author AYOU
 * @version 2016年6月1日 下午10:39:38
 */
public class Main {
	@SuppressWarnings("resource")
	public static void main(String[] args) throws IOException {
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(new String[] { "appctx-module-identity.xml" });
		context.start();
		System.out.println("Loading ClassPathXmlApplicationContext is ok...");
		System.in.read();
	}
}
