package com.ayou.module.identity.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import com.alibaba.dubbo.config.annotation.Service;
import com.ayou.core.commons.utils.UUIDUtil;
import com.ayou.core.commons.utils.db.DBUtil;
import com.ayou.core.commons.utils.db.DBUtil.SqlTypes;
import com.ayou.core.commons.utils.security.AES;
import com.ayou.core.commons.utils.security.MD5;
import com.ayou.core.commons.utils.validation.UserUtil;
import com.ayou.module.base.dao.JdbcDao;
import com.ayou.module.identity.dao.IdentityDao;
import com.ayou.module.identity.dao.TokenCacheDao;
import com.ayou.module.identity.entity.LoginInfo;
import com.ayou.module.identity.entity.User;
import com.ayou.module.identity.service.EmailService;
import com.ayou.module.identity.service.IdentityService;
import com.baomidou.kisso.Token;

/**
 * @category 用户Readis
 * @author AYOU
 * @version 2016年5月10日 下午10:29:14
 */
@Service(version = "1.0")
public class IdentityServiceImpl implements IdentityService {
	@Resource
	private IdentityDao<User> identityDao;
	@Resource
	private IdentityDao<LoginInfo> logDao;
	@Resource
	private JdbcDao jdbcDao;
	@Resource
	private TokenCacheDao tokenCacheDao;

	@Override
	public User getUser(String uuid, User user) {
		return (User) identityDao.get(uuid, user);
	}

	@Override
	public Boolean setUser(User user) {
		return identityDao.add(user);
	}

	@Override
	public Boolean delUser(String uuid) {
		return identityDao.delete(uuid);
	}

	@Override
	public Boolean update(User user) {
		return identityDao.update(user);
	}

	@Override
	public Boolean isRegister(String userid) {
		String sql = "SELECT COUNT(U.UUID) FROM USER U WHERE U.EMAIL=? OR U.PHONE=? OR U.NAME=?";
		Integer count = jdbcDao.queryForBaseType(Integer.class, sql, new Object[] { userid, userid, userid });
		return count > 0 ? true : false;
	}

	@Override
	public Boolean register(String userid, String pass) {
		User user = new User();
		if (UserUtil.getUserId(userid).equals("email")) {// 邮箱
			user.setEmail(userid);
			String key = UUIDUtil.generate() + "禁止任何未授权的黑客行为！";
			// 发送激活邮件
			try {
				key = AES.mailEncrypt(key);
				if (key == null) {
					return false;
				}
				String url = "https://passport.qtdu.com/active.html?u=" + userid + "&key=" + key;
				EmailService.simpSend("qtdu_active", userid, url);
				user.setEmail_code(key); // 设置邮箱激活码
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else if (UserUtil.getUserId(userid).equals("phone")) {// 手机
			user.setPhone(userid);
		} else if (UserUtil.getUserId(userid).equals("username")) {// 用户名
			user.setName(userid);
		} else {
			return false;
		}
		user.generateUuid();
		user.setPassword(MD5.crypt(pass));
		List<Object> params = new ArrayList<Object>();
		String sql = DBUtil.getSqlByObject(SqlTypes.INSERT, user, params);
		jdbcDao.update(sql, params.toArray(new Object[0]));
		return isRegister(userid);
	}

	/**
	 * 0：用户不存在 1：登录成功 -1: 密码错误
	 */
	@Override
	public Integer Login(String userid, String pass) {
		pass = MD5.crypt(pass);
		if (isRegister(userid)) {
			String sql = "SELECT COUNT(U.UUID) FROM USER U WHERE (U.EMAIL=? OR U.PHONE=? OR U.NAME=?) AND U.PASSWORD=?";
			Integer count = jdbcDao.queryForBaseType(Integer.class, sql, new Object[] { userid, userid, userid, pass });
			if (count > 0) {
				return 1;
			} else {
				return -1;
			}
		} else {
			return 0;
		}
	}

	/**
	 * 清除缓存
	 */
	@Override
	public Boolean LogOut(String userId) {
		return identityDao.del("user", userId);
	}

	@Override
	public LoginInfo getLoginInfo(String userId) {
		String sql = "SELECT U.UUID,U.EMAIL,U.NAME,U.DESCRIPTION,U.SEX,U.BIRTHDAY,U.HEADURL,U.ACTIVE FROM USER U WHERE U.EMAIL=? OR U.PHONE=? OR U.NAME=?";
		return jdbcDao.queryForObject(LoginInfo.class, sql, new Object[] { userId, userId, userId });
	}

	@Override
	public LoginInfo getLoginInfoByUuid(String uuid) {
		String sql = "SELECT U.UUID,U.EMAIL,U.NAME,U.DESCRIPTION,U.SEX,U.BIRTHDAY,U.HEADURL,U.ACTIVE FROM USER U WHERE U.UUID=?";
		return jdbcDao.queryForObject(LoginInfo.class, sql, new Object[] { uuid });
	}

	@Override
	public LoginInfo getLoginInfoForRedis(String userId) {
		LoginInfo li = new LoginInfo();
		return logDao.get("user_" + userId, li);
	}

	@Override
	public Boolean setLoginInfo(LoginInfo loginInfo, String name, Long time) {
		return logDao.add(loginInfo, name, time);
	}

	/**
	 * 获取ssoToken
	 */
	@Override
	public Token getToken(String key, int expires) {
		return tokenCacheDao.get(key, expires);
	}

	/**
	 * setToken
	 */
	@Override
	public boolean setToken(String key, Token token, int expires) {
		return tokenCacheDao.set(key, token, expires);
	}

	/**
	 * 删除ssoToken
	 */
	@Override
	public boolean deleteToken(String key) {
		return tokenCacheDao.delete(key);
	}

	@Override
	public User getUserByDB(User user) {
		List<Object> params = new ArrayList<Object>();
		String sql = DBUtil.getSqlByObject(SqlTypes.QUERY, user, params);
		return jdbcDao.queryForObject(User.class, sql, params);
	}

	@Override
	public Boolean updateByDB(User user) {
		List<Object> params = new ArrayList<Object>();
		String sql = DBUtil.getSqlByObject(SqlTypes.UPDATE, user, params);
		Integer r = jdbcDao.update(sql, params);
		if (r > 0) {
			return true;
		} else {
			return false;
		}
	}
}
