package com.ayou.module.identity.service;

import com.ayou.module.identity.entity.LoginInfo;
import com.ayou.module.identity.entity.User;
import com.baomidou.kisso.Token;

/**
 * @author AYOU
 * @version 2016年5月10日 下午10:28:49
 */
public interface IdentityService {
	User getUser(String uuid, User user);
	
	User getUserByDB(User user);

	Boolean setUser(User user);

	Boolean delUser(String uuid);

	Boolean update(User user);
	
	Boolean updateByDB(User user);

	Boolean isRegister(String userId);
	
	Boolean register(String userid, String pass);
	
	/**
	 * 0：用户不存在 1：登录成功 -1: 密码错误
	 */
	Integer Login(String userId, String pass);

	Boolean LogOut(String userId);
	
	
	Boolean setLoginInfo(LoginInfo loginInfo, String name, Long time);
	
	/**
	 * 获取登录用户部分信息用来放入redis
	 * @param uuid
	 * @return
	 */
	LoginInfo getLoginInfo(String userId);

	LoginInfo getLoginInfoForRedis(String userId);

	LoginInfo getLoginInfoByUuid(String uuid);

	/**
	 *  获取ssoToken
	 * @param key
	 * @param expires
	 * @return Token
	 */
	Token getToken(String key, int expires);
	/**
	 * 缓存ssoToken
	 * @param key
	 * @param token
	 * @param expires
	 * @return boolean
	 */
	boolean setToken(String key, Token token, int expires);
	/**
	 * 删除ssoToken
	 * @param key
	 * @return boolean
	 */
	boolean deleteToken(String key);
}
