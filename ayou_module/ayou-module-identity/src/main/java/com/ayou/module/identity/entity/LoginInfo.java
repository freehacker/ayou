package com.ayou.module.identity.entity;

import java.util.Date;

import com.ayou.module.base.entity.BaseEntity;

/**
 * @category 登录信息
 * @version 1.0
 * @author AYOU
 * @date 2016年7月22日 下午10:14:02
 */
public class LoginInfo extends BaseEntity{
	private static final long serialVersionUID = -6426120315381165280L;
	private String email;
	private String name;
	private String description;
	private Integer sex;
	private Date birthday;
	private String headUrl;
	private Integer active;
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Integer getSex() {
		return sex;
	}
	public void setSex(Integer sex) {
		this.sex = sex;
	}
	public Date getBirthday() {
		return birthday;
	}
	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}
	public String getHeadUrl() {
		return headUrl;
	}
	public void setHeadUrl(String headUrl) {
		this.headUrl = headUrl;
	}
	public Integer getActive() {
		return active;
	}
	public void setActive(Integer active) {
		this.active = active;
	}
}
