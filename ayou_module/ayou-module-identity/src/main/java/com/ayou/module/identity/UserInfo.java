package com.ayou.module.identity;

/**
 * @author AYOU
 * @version 2016年3月29日 上午2:23:49
 */
public interface UserInfo {

	public String getUuid();

	public String getName();

	public String getDescription();

	public Integer atSuperUser();

}
