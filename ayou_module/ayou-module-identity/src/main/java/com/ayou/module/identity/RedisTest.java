/*package com.ayou.module.identity;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;

import com.ayou.module.identity.dao.IdentityDao;
import com.ayou.module.identity.entity.User;

import junit.framework.Assert;

*//**
 * @category Redis测试
 * @author AYOU
 * @version 2016年6月1日 下午2:12:17
 *//*
@ContextConfiguration(locations = { "classpath*:appctx-module-identity.xml" })
public class RedisTest extends AbstractJUnit4SpringContextTests {

	@Resource
	private IdentityDao<User> identityDao;

	*//**
	 * 新增
	 *//*
	public void testAddUser() {
		User user = new User();
		String uuid = user.generateUuid();
		System.out.println(uuid);
		user.setUuid(uuid);
		user.setName("AYOU测试");
		boolean result = identityDao.add(user);
		Assert.assertTrue(result);
	}

	*//**
	 * 批量新增 普通方式
	 *//*
	public void testAddUsers1() {
		List<User> list = new ArrayList<User>();
		for (int i = 10; i < 50000; i++) {
			User user = new User();
			user.setUuid(user.generateUuid() + i);
			user.setName("AYOU测试" + i);
			list.add(user);
		}
		long begin = System.currentTimeMillis();
		for (User user : list) {
			identityDao.add(user);
		}
		System.out.println(System.currentTimeMillis() - begin);
	}

	*//**
	 * 批量新增 pipeline方式
	 *//*
	public void testAddUsers2() {
		List<User> list = new ArrayList<User>();
		for (int i = 10; i < 1500000; i++) {
			User user = new User();
			user.setUuid(user.generateUuid() + i);
			user.setName("AYOU测试" + i);
			list.add(user);
		}
		long begin = System.currentTimeMillis();
		boolean result = identityDao.add(list);
		System.out.println(System.currentTimeMillis() - begin);
		Assert.assertTrue(result);
	}

	*//**
	 * 修改
	 *//*
	
	public void testUpdate() {
		User user = new User();
		user.setUuid(user.generateUuid());
		user.setName("修改AYOU测试");
		boolean result = identityDao.update(user);
		Assert.assertTrue(result);
	}

	*//**
	 * 通过key删除单个
	 *//*
	
	public void testDelete() {
		String key = "user1";
		identityDao.delete(key);
	}

	*//**
	 * 批量删除
	 *//*
	
	public void testDeletes() {
		List<String> list = new ArrayList<String>();
		for (int i = 0; i < 10; i++) {
			list.add("user" + i);
		}
		identityDao.delete(list);
	}

	*//**
	 * 获取
	 *//*
	
	public void testGetUser() {
		String id = "user1";
		User u =new User();
		User user = (User) identityDao.get(id,u);
		Assert.assertNotNull(user);
		Assert.assertEquals(user.getName(), "AYOU");
	}

	*//**
	 * 设置identityDao
	 * 
	 * @param identityDao
	 *            the identityDao to set
	 *//*
	public void setUuidentityDao(IdentityDao<User> identityDao) {
		this.identityDao = identityDao;
	}
}
*/