package com.ayou.site.base.de.observer.v2;

/**
 * @category Mian
 * @author AYOU
 * @version 1.0
 * @since JDK 1.8
 * @date 2017年9月1日 下午2:54:47
 */
public class Mian {

	public static void main(String[] args) {
		Small sm = new Small();
/*		Ob1 ob1 = new Ob1();
		Ob2 ob2 = new Ob2();
		sm.register(ob1);
		sm.register(ob2);
		sm.send("hello!");

		sm.remove(ob2);
		sm.send("world!");*/
		
		sm.register((msg)->{System.out.println("A "+msg);});
		sm.register((msg)->{System.out.println("B "+msg);});
		sm.send("world!");
	}
}
