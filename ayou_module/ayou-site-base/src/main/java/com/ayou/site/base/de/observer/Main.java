package com.ayou.site.base.de.observer;

import com.ayou.site.base.de.observer.impl.VideoFans;
import com.ayou.site.base.de.observer.impl.VideoSite;

/**
 * @category 来一发
 * @author AYOU
 * @version 2016年6月3日 上午10:20:39
 */
public class Main {
	public static void main(String[] args) {
		VideoSite vs = new VideoSite();
		vs.registerObserver(new VideoFans("LiLei"));
		vs.registerObserver(new VideoFans("HanMeimei"));
		vs.registerObserver(new VideoFans("XiaoMing"));

		// add videos
		vs.addVideos("战争---东京热");
		vs.addVideos("动作---我们都可耻");
	}
}
