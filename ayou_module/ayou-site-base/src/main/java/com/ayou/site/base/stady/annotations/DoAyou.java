package com.ayou.site.base.stady.annotations;

/**
 * @category DoAyou.java
 * @version 1.0
 * @author AYOU
 * @date 2016年7月22日 下午4:32:12
 */
public enum DoAyou {
	Normal("0", "正常用户"),
	
	Admin("1", "管理员"),
	
	Sys("2", "系统管理员"),
	
	Skip("3", "跳过权限验证");

	/** 主键 */
	private final String key;

	/** 描述 */
	private final String desc;

	DoAyou(final String key, final String desc) {
		this.key = key;
		this.desc = desc;
	}

	public String getKey() {
		return this.key;
	}

	public String getDesc() {
		return this.desc;
	}
}
