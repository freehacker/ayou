package com.ayou.site.base.study.thread.pool;

import java.net.ServerSocket;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @category ThreadPoolExecutor
 * @author AYOU
 * @version 1.0
 * @since JDK 1.8
 * @date 2017年1月13日 下午4:05:22
 * 
 * public ThreadPoolExecutor(int corePoolSize, int maximumPoolSize, long keepAliveTime, TimeUnit unit, BlockingQueue<Runnable> workQueue, ThreadFactory threadFactory,  
 *                           RejectedExecutionHandler handler) {  
 *      if (corePoolSize < 0 ||  
 *          maximumPoolSize <= 0 ||  
 *          maximumPoolSize < corePoolSize ||  
 *          keepAliveTime < 0)  
 *          throw new IllegalArgumentException();  
 *      if (workQueue == null || threadFactory == null || handler == null)  
 *        throw new NullPointerException();  
 *    this.corePoolSize = corePoolSize;  
 *    this.maximumPoolSize = maximumPoolSize;  
 *    this.workQueue = workQueue;  
 *    this.keepAliveTime = unit.toNanos(keepAliveTime);  
 *    this.threadFactory = threadFactory;  
 *    this.handler = handler;  
 * }  
 * corePoolSize	核心线程池大小 
 * maximumPoolSize	最大线程池大小
 * keepAliveTime	线程池中超过corePoolSize数目的空闲线程最大存活时间；可以allowCoreThreadTimeOut(true)使得核心线程有效时间
 * TimeUnit	keepAliveTime时间单位
 * workQueue	阻塞任务队列
 * threadFactory	新建线程工厂
 * RejectedExecutionHandler	当提交任务数超过maxmumPoolSize+workQueue之和时，任务会交给RejectedExecutionHandler来处理
 */
public class NewThreadPoolExecutor {
	ServerSocket serverSocket;
	public ThreadPoolExecutor threadPool = new ThreadPoolExecutor(10, 15, 3, TimeUnit.SECONDS, new ArrayBlockingQueue<Runnable>(500),
			new ThreadPoolExecutor.CallerRunsPolicy());

	public void exec() {
		//System.out.println("启动服务器,开始监听39871端口");
		//serverSocket = new ServerSocket(39871);
		//Socket s = null;
		while (true) {//(s = serverSocket.accept()) != null
			R r = new R();
			threadPool.execute(r);
			threadPool.setRejectedExecutionHandler(new H());
			
			try {
				System.out.println("主动执行任务的近似线程数:" + threadPool.getActiveCount());
				System.out.println("池中的当前线程数:" + threadPool.getPoolSize());
				System.out.println("曾经同时位于池中的最大线程数:" + threadPool.getLargestPoolSize());
				System.out.println("此执行程序使用的任务队列:" + threadPool.getQueue());
				System.out.println("曾计划执行的近似任务总数:" + threadPool.getTaskCount());
				System.out.println("线程池中已完成的任务数:" + threadPool.getCompletedTaskCount());
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	public static void main(String[] args) {
		new NewThreadPoolExecutor().exec();
	}
}

class R implements Runnable {
	@Override
	public void run() {
		System.out.println("阿友大哥好！");
	}
}

class H implements RejectedExecutionHandler {
	@Override
	public void rejectedExecution(Runnable r, ThreadPoolExecutor executor) {
		System.out.println("阿友老弟好！");
	}
}
