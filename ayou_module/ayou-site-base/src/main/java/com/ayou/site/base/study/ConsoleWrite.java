package com.ayou.site.base.study;

import java.io.IOException;
import java.io.Writer;

/**
 * @category ConsoleWrite.java
 * @author AYOU
 * @version 2016年5月25日 下午2:18:18
 */
public class ConsoleWrite extends Writer {
	private char[] writeBuffer;

	@Override
	public void write(char[] cbuf, int off, int len) throws IOException {
	}

	@Override
	public void flush() throws IOException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void close() throws IOException {
		// TODO Auto-generated method stub
		
	}
	
	public void write(String str, int off, int len) throws IOException {
        synchronized (lock) {
            char cbuf[];
            if (len <= 1024) {
                if (writeBuffer == null) {
                    writeBuffer = new char[1024];
                }
                cbuf = writeBuffer;
            } else {    // Don't permanently allocate very large buffers.
                cbuf = new char[len];
            }
            str.getChars(off, (off + len), cbuf, 0);
            write(cbuf, 0, len);
        }
    }

}
