package com.ayou.site.base.de.observer.v2;

/**
 * @category Observer
 * @author   AYOU
 * @version  1.0
 * @since    JDK 1.8
 * @date    2017年9月1日 下午2:49:34
 */
public interface Observer {
	void get(String msg);
}

