package com.ayou.site.base.de.decorator;

/**
 * @category 装饰高挑
 * @version 1.0
 * @author AYOU
 * @date 2016年6月8日 上午11:15:06
 */
public class Tall extends GirlDecorator {
	private Girl girl;

	public Tall(Girl g) {
		girl = g;
	}

	@Override
	public String getDescription() {
		return girl.getDescription() + "+大美腿";
	}

}
