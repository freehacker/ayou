package com.ayou.site.base.de.observer.v3;

/**
 * @category Subject
 * @author   AYOU
 * @version  1.0
 * @since    JDK 1.8
 * @date    2017年9月4日 上午10:57:01
 */
public interface Subject {
	void regiser(Observer observer);
	void notify(String msg);
}

