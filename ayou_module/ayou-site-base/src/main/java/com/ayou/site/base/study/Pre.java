package com.ayou.site.base.study;

/**
 * @category Pre
 * @author AYOU
 * @version 1.0
 * @since JDK 1.8
 * @date 2016年12月6日 下午2:56:13
 */
public class Pre {
	public static void main(String[] args) {
		int a = 3;
		int b = 3;
		String c = "abc";
		String d = new String("abc");
		String e = "abc";
		String f = c.intern();
		String g = d.intern();

		System.out.printf("a==b %b", a == b);
		System.out.printf("\nc==d %b", c == d);
		System.out.printf("\nc==e %b", c == e);
		System.out.printf("\nc==f %b", c == f);
		System.out.printf("\nd==g %b", d == g);
	}
}
