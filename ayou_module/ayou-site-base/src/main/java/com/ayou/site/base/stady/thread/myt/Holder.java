package com.ayou.site.base.stady.thread.myt;

/**
 * @category 操作
 * @author AYOU
 * @version 2016年5月26日 上午9:06:11
 */
public class Holder {
	private int m;

	public synchronized void add() {
		System.out.println();
		System.out.print("value+::" + m);
		while (m == 0) {  //这里如果是if的话 线程多了 值就 不对！
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		m--;
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		notify();  //通知wait的线程
	}

	public synchronized void reduce() {
		System.out.println();
		System.out.print("value-::" + m);
		while (m != 0) {  //这里如果是if的话 线程多了 值就 不对！
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		m++;
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		notify(); //通知wait的线程
	}
}
