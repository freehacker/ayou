package com.ayou.site.base;

import java.util.Calendar;

/**
 * @category EntityByJava8
 * @author   AYOU
 * @version  1.0
 * @since    JDK 1.8
 * @date    2017年6月12日 上午10:44:36
 */
public class EntityByJava8 {
	private Calendar barithday;

	public Calendar getBarithday() {
		return barithday;
	}

	public void setBarithday(Calendar barithday) {
		this.barithday = barithday;
	}
}

