package com.ayou.site.base.study.jvm;

import java.io.IOException;
import java.io.InputStream;

/**
 * @category 类加载器
 * @author AYOU
 * @version 1.0
 * @since JDK 1.8
 * @date 2017年5月20日 下午4:25:36
 * 运行时虚拟机已经加载了MyClassLoader，然后我们在手动加载了MyClassLoader 是2个独立的类 故false
 */
public class MyClassLoader {
	public static void main(String[] args) {
		ClassLoader loader = new ClassLoader() {
			@Override
			public Class<?> loadClass(String name) throws ClassNotFoundException {
				try {
					String fileName = name.substring(name.lastIndexOf(".") + 1) + ".class";
					InputStream is = getClass().getResourceAsStream(fileName);
					if (is == null) {
						return super.loadClass(name);
					}
					byte[] b = new byte[is.available()];
					is.read(b);
					return defineClass(name, b, 0, b.length);
				} catch (IOException e) {
					throw new ClassNotFoundException(name);
				}
			}
		};

		try {
			Object obj = loader.loadClass("com.ayou.site.base.study.jvm.MyClassLoader").newInstance();
			System.out.println(obj.getClass());
			System.out.println(obj instanceof com.ayou.site.base.study.jvm.MyClassLoader);
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

	}
}
