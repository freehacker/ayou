package com.ayou.site.base.de.singleton;

/**
 * @category 饿汉模式
 * @version 1.0
 * @author AYOU
 * @date 2016年6月8日 上午10:23:54
 * 
 * 最常见、最简单的单例模式写法之一。
 * 顾名思义，“饿汉模式” 就是很 “饥渴”，所以一上来就需要给它新建一个实例。
 * 但这种方法有一个明显的缺点，
 * 那就是不管有没有调用过获得实例的方法（本例中为 getWife() ），每次都会新建一个实例。
 */
public class Singleton1 {

	// 一开始就新建一个实例
	private static final Singleton1 singleton = new Singleton1();

	// 默认构造方法
	private Singleton1() {
	}

	// 获得实例的方法
	public static Singleton1 getSingleton() {
		return singleton;
	}
}
