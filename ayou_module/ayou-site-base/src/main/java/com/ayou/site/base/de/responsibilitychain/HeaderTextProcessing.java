package com.ayou.site.base.de.responsibilitychain;

/**
 * @category HeaderTextProcessing
 * @author AYOU
 * @version 1.0
 * @since JDK 1.8
 * @date 2017年9月1日 下午4:05:29
 */
public class HeaderTextProcessing extends ProcessingObject<String> {

	@Override
	protected String handleWork(String input) {
		return "From Raoulm Mario And Alan: " + input;
	}

}
