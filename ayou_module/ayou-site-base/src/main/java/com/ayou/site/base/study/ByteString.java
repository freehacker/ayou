package com.ayou.site.base.study;

/**
 * @category ByteString.java
 * @author AYOU
 * @version 2016年5月24日 下午4:01:16
 */
public class ByteString {
	private String a;
	private Byte b;
	private String c;

	public Byte getB() {
		return b;
	}

	public void setB(Byte b) {
		this.b = b;
	}

	public String getA() {
		return a;
	}

	public void setA(String a) {
		this.a = a;
	}

	public String getC() {
		return c;
	}

	public void setC(String c) {
		this.c = c;
	}

	public ByteString(String a, Byte b, Object t) {
		super();
		this.a = a;
		this.b = b;
		this.c = c.toString();
	}
}
