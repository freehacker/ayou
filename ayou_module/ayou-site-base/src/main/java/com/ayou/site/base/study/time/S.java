package com.ayou.site.base.study.time;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * @category S
 * @author AYOU
 * @version 1.0
 * @since JDK 1.8
 * @date 2017年1月18日 上午10:46:35
 */
public class S {
	public static void main(String[] args) {
		for(;;){
			long t = System.currentTimeMillis();
			System.out.println(t);
			System.out.println(Long.toString(t).length());

			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS");
			Date date = new Date(t);
			System.out.println(simpleDateFormat.format(date));
		}
	}
}

class Y {
	public static void main(String[] args) {
		Calendar calendar = Calendar.getInstance();
		calendar.get(Calendar.YEAR);
		System.out.println(new Date());
	}
}
