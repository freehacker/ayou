package com.ayou.site.base.de.observer.impl;

import java.util.ArrayList;

import com.ayou.site.base.de.observer.Observer;
import com.ayou.site.base.de.observer.Subject;

/**
 * @category 某视频网站 实现 Subject 接口.java
 * @author AYOU
 * @version 2016年6月3日 上午10:13:12
 */
public class VideoSite implements Subject {

	// 观察者列表 以及 更新了的视频列表
	private ArrayList<Observer> userList;
	private ArrayList<String> videos;

	public VideoSite() {
		userList = new ArrayList<Observer>();
		videos = new ArrayList<String>();
	}

	@Override
	public void registerObserver(Observer o) {
		userList.add(o);
	}

	@Override
	public void removeObserver(Observer o) {
		userList.remove(o);
	}

	@Override
	public void notifyAllObservers() { //O(n)
		for (Observer o : userList) {
			o.update(this);
		}
	}

	public void addVideos(String video) {
		this.videos.add(video);
		notifyAllObservers();
	}

	/*public ArrayList<String> getVideos() {
		return videos;
	}*/

	public String toString() {
		return videos.toString();
	}
}
