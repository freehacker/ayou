package com.ayou.site.base.stady.thread.myt;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @category 减
 * @author AYOU
 * @version 2016年5月26日 上午9:05:22
 */
public class Reduce extends Thread {
	protected Logger logger = LoggerFactory.getLogger(getClass());
	private Holder h;
	public Reduce(Holder h) {
		this.h = h;
	}

	@Override
	public void run() {
		//byte[] bt =new byte[1024];
		//char[] ch = new char[1024*1024];
		int index = 1;
		while (true) {
			h.reduce();
			System.err.print(" +" + index + "次");
			index++;
			
			Thread current = Thread.currentThread();  
	        /*System.out.println(current.getPriority());  
	        System.out.println(current.getName());  
	        System.out.println(current.activeCount());  
	        System.out.println(current.getId());  
	        System.out.println(current.getThreadGroup()); */
			if(current.getPriority()==10){
				/*logger.error(" \n堆栈:"+current.getStackTrace() 
						+ "\t" + "hashCode:"+ current.hashCode() 
						+ "\t" + "toString:" + current.toString()
						+ "\t" + "名称:" + current.getName()
						+ "\t" + "优先级:" + current.getPriority());*/
				System.err.print(
						" 堆栈:"+current.getStackTrace() 
						+ "\t" + "hashCode:"+ current.hashCode() 
						+ "\t" + "toString:" + current.toString()
						+ "\t" + "名称:" + current.getName()
						+ "\t" + "优先级:" + current.getPriority());
			}else{
				/*logger.info(" \n堆栈:"+current.getStackTrace() 
				+ "\t" + "hashCode:"+ current.hashCode() 
				+ "\t" + "toString:" + current.toString()
				+ "\t" + "名称:" + current.getName()
				+ "\t" + "优先级:" + current.getPriority());*/
				System.out.print(
						" 堆栈:"+current.getStackTrace() 
						+ "\t" + "hashCode:"+ current.hashCode() 
						+ "\t" + "toString:" + current.toString()
						+ "\t" + "名称:" + current.getName()
						+ "\t" + "优先级:" + current.getPriority());
			}
		}
	}
}
