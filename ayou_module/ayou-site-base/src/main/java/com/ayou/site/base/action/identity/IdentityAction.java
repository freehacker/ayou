package com.ayou.site.base.action.identity;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ayou.site.base.action.SimpleActionSupport;

/**
 * @category 用户登录
 * @author AYOU
 * @version 2016年5月30日 下午8:42:30
 */

@Controller
@RequestMapping("/user")
public class IdentityAction extends SimpleActionSupport {
	
}
