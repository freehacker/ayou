package com.ayou.site.base.study.java8.completefuture;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * @category Pool
 * @author AYOU
 * @version 1.0
 * @since JDK 1.8
 * @date 2017年9月4日 下午4:16:33
 */
public class Pool {
	public static void main(String[] args) {
		ExecutorService pool = Executors.newFixedThreadPool(3);
		Future<Double> future = pool.submit(new Callable<Double>() {
			@Override
			public Double call() throws Exception {
				return 1.;
			}
		});
		try {
			Double result = future.get(1, TimeUnit.SECONDS);
			System.out.println(result);
		} catch (InterruptedException | ExecutionException e) {
			e.printStackTrace();
		} catch (TimeoutException e) {
			e.printStackTrace();
		}
	}
}
