package com.ayou.site.base.study.java8.lambda;

/**
 * @category Apple
 * @author   AYOU
 * @version  1.0
 * @since    JDK 1.8
 * @date    2017年5月19日 下午3:19:20
 */
public class Apple {
	private Integer weight;
	private String color;
	
	public Apple(Integer weight, String color) {
		super();
		this.weight = weight;
		this.color = color;
	}
	public Integer getWeight() {
		return weight;
	}
	public void setWeight(Integer weight) {
		this.weight = weight;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
}
