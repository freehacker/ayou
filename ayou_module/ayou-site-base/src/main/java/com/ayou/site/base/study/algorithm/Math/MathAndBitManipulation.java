package com.ayou.site.base.study.algorithm.Math;

/**
 * @category MathAndBitManipulation
 * @author   AYOU
 * @version  1.0
 * @since    JDK 1.8
 * @date     2016年11月22日 下午5:29:28
 * 
 * Question:<br>
 *  Using O(1) time to check whether an integer n is a power of 2.<br>
	Example<br>
	For n=4, return true;<br>
	For n=5, return false;<br>
	Challenge<br>
	O(1) time
 */
public class MathAndBitManipulation {
	/*
     * @param n: An integer
     * @return: True or false
     */
    public boolean checkPowerOf2(int n) {
        if (n < 1) {
            return false;
        } else {
            return (n & (n - 1)) == 0;
        }
    }
}

