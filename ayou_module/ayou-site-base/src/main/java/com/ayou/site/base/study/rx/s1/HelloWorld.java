package com.ayou.site.base.study.rx.s1;

import io.reactivex.Flowable;

/**
 * @category HelloWorld
 * @author AYOU
 * @version 1.0
 * @since JDK 1.8
 * @date 2017年4月24日 上午10:54:54
 */
public class HelloWorld {
	public static void main(String[] args) {
		Flowable.just("Hello world").subscribe(System.out::println);
	}
}
