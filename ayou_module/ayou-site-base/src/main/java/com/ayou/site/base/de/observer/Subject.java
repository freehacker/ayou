package com.ayou.site.base.de.observer;

/**
 * @category 主题接口.java
 * @author AYOU
 * @version 2016年6月3日 上午10:10:59
 */
public interface Subject {
	public void registerObserver(Observer o);

	public void removeObserver(Observer o);

	public void notifyAllObservers();
}
