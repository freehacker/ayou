package com.ayou.site.base.stady.thread;

/**
 * @category 减
 * @author AYOU
 * @version 2016年5月25日 下午5:03:47
 */
public class DecreaseThread extends Thread {
	private NumberHolder numberHolder;

	public DecreaseThread(NumberHolder numberHolder) {
		this.numberHolder = numberHolder;
	}

	@Override
	public void run() {
		int i =1;
		while (true) {
			// 进行一定的延时
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

			// 进行减少操作
			numberHolder.decrease();
			System.out.println("减" + i + "次");
			i++;
		}
	}
}
