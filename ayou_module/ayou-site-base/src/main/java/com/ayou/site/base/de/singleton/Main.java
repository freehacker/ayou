package com.ayou.site.base.de.singleton;

/**
 * @category Main
 * @author   AYOU
 * @version  1.0
 * @since    JDK 1.8
 * @date    2017年6月6日 上午11:23:30
 */
public class Main {
	public static void main(String[] args) {
		Singleton6 h = Singleton6.INSTANCE;
		System.out.println(h.whateverMethod());
	}
}

