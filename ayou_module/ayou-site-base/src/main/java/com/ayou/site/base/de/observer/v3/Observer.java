package com.ayou.site.base.de.observer.v3;

/**
 * @category Observer
 * @author AYOU
 * @version 1.0
 * @since JDK 1.8
 * @date 2017年9月4日 上午10:57:10
 */
public interface Observer {
	void getMsg(String msg);
}
