package com.ayou.site.base.de.singleton;

/**
 * @category 线程安全的懒汉模式
 * @version 1.0
 * @author AYOU
 * @date 2016年6月8日 上午10:49:47
 * 
 * 是不是感觉很简单？但是上面的懒汉模式却存在一个严重的问题。
 * 那就是如果有多个线程并行调用 getSingleton() 方法的时候，还是会创建多个实例，单例模式就失效了。
 * Bug 来了，改改改！
 * 
 * 简单，我们在基本的懒汉模式上，把它设为线程同步（synchronized）就好了。
 * synchronized 的作用就是保证在同一时刻最多只有一个线程运行，这样就避免了多线程带来的问题。
 * 关于 synchronized 关键字，你可以 点击这里 了解更多。
 */
public class Singleton3 {
	private static Singleton3 singleton;

	private Singleton3() {
	}

	// 添加了 synchronized 关键字
	public static synchronized Singleton3 getSingleton() {
		if (singleton == null) {
			singleton = new Singleton3();
		}
		return singleton;
	}
}
