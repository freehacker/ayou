package com.ayou.site.base.de.decorator;

/**
 * @category 走一个
 * @version 1.0
 * @author AYOU
 * @date 2016年6月8日 上午11:16:32
 */
public class Main {
	public static void main(String[] args) {
		//造个妞
		Girl g1 = new AmericanGirl();
		System.out.println(g1.getDescription());
		//弄个头发
		GoldenHair g2 = new GoldenHair(g1);
		System.out.println(g2.getDescription());
		//我要大长腿
		Tall g3 = new Tall(g2);
		System.out.println(g3.getDescription());

		System.out.println();
		
		//一步到位
		Girl g = new Tall(new GoldenHair(new ChineseGirl()));
		System.out.println(g.getDescription());
	}
}
