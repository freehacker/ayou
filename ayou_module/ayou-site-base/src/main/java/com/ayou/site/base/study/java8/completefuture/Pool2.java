package com.ayou.site.base.study.java8.completefuture;

import java.util.concurrent.CompletableFuture;

/**
 * @category Pool2
 * @author AYOU
 * @version 1.0
 * @since JDK 1.8
 * @date 2017年9月4日 下午4:42:25
 */
public class Pool2 {
	public static void main(String[] args) {
		CompletableFuture.runAsync(() -> {
			System.out.println("hello");
		});
	}
}
