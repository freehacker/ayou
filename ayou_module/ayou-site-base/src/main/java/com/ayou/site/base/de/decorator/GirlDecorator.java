package com.ayou.site.base.de.decorator;

/**
 * @category 装饰女孩
 * @version 1.0
 * @author AYOU
 * @date 2016年6月8日 上午11:12:21
 */
public abstract class GirlDecorator extends Girl {
	public abstract String getDescription();
}
