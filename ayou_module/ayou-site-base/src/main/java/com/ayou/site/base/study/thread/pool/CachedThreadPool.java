package com.ayou.site.base.study.thread.pool;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @Category 可缓存线程池
 * @see 如果线程池长度超过处理需要，可灵活回收空闲线程，若无可回收，则新建线程。
 * @author AYOU
 * @version 1.0
 * @since JDK 1.8
 * @date 2017年1月13日 下午2:36:08
 */
public class CachedThreadPool {
	public void exec() {
		ExecutorService cachedThreadPool = Executors.newCachedThreadPool();
		for (int i = 0; i < 10; i++) {
			final int index = i;
			try {
				Thread.sleep(index * 1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

			cachedThreadPool.execute(new Runnable() {
				@Override
				public void run() {
					System.out.println(index);
				}
			});
		}
	}

	public static void main(String[] args) {
		CachedThreadPool ctp = new CachedThreadPool();
		ctp.exec();
	}
}
