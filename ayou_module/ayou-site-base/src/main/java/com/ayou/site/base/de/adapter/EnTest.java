package com.ayou.site.base.de.adapter;

import com.ayou.site.base.de.adapter.impl.EnPlugin;

/**
 * @category 英标测试
 * @author AYOU
 * @version 2016年5月23日 下午5:00:16
 */
public class EnTest {
	public static void main(String[] args) {
		EnPluginInterface enPlugin = new EnPlugin();
		Home home = new Home(enPlugin);
		home.charge();
	}
}
