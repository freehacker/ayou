package com.ayou.site.base.study.thread.book.c1;

/**
 * @category MyThread
 * @author   AYOU
 * @version  1.0
 * @since    JDK 1.8
 * @date    2017年9月8日 上午9:30:16
 */
public class MyThread extends Thread {
	@Override
	public void run() {
		for (int i = 0; i < 10; i++) {
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			System.out.println("hello from Thread:" + Thread.currentThread().getName());
		}
	}
}
