package com.ayou.site.base.study.thread;

/**
 * @category 加
 * @author AYOU
 * @version 2016年5月25日 下午5:04:36
 */
public class IncreaseThread extends Thread {
	private NumberHolder numberHolder;

	public IncreaseThread(NumberHolder numberHolder) {
		this.numberHolder = numberHolder;
	}

	@Override
	public void run() {
		int  i =1;
		while (true) {
			// 进行一定的延时
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

			// 进行增加操作
			numberHolder.increase();
			System.out.println("加" + i + "次");
			i++;
		}
	}
}
