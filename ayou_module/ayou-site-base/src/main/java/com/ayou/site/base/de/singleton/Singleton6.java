package com.ayou.site.base.de.singleton;

/**
 * @category 枚举
 * @version 1.0
 * @author AYOU
 * @date 2016年6月8日 上午10:49:47
 * 
 * 如下，代码简直是简单得不能再简单了。 我们可以通过 Singleton6.INSTANCE 来访问实例对象， 这比 getSingleton()
 * 要简单得多，而且创建枚举默认就是线程安全的，还可以防止反序列化带来的问题。 这么 优（niu）雅（bi）的方法，来自于新版
 * 《Effective Java》 这本书。这种方式虽然不常用，但是最为推荐。
 */
public enum Singleton6 {
	INSTANCE;

	// 自定义的其他任意方法
	public String whateverMethod() {
		return "hello Singleton!";
	}
}
