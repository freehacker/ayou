package com.ayou.site.base.study.thread;

/**
 * @category NumberHolder.java
 * @author AYOU
 * @version 2016年5月25日 下午5:04:05
 */

public class NumberHolder {
	private int number;
	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	// 加
	public synchronized void increase() {
		while (0 != number) { // if
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		// 能执行到这里说明已经被唤醒
		// 并且number为0
		number++;
		System.out.println("+后：" + number);
		// 通知在等待的线程
		notify();
	}

	// 减
	public synchronized void decrease() {
		while (0 == number) { // if
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		// 能执行到这里说明已经被唤醒
		// 并且number不为0
		number--;
		System.out.println("-后：" + number);
		// 通知在等待的线程
		notify();
	}
}
