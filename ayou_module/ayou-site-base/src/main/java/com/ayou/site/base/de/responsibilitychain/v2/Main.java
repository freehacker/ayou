package com.ayou.site.base.de.responsibilitychain.v2;

import java.util.function.Function;
import java.util.function.UnaryOperator;

/**
 * @category Main
 * @author AYOU
 * @version 1.0
 * @since JDK 1.8
 * @date 2017年9月1日 下午4:25:16
 */
public class Main {
	public static void main(String[] args) {
		UnaryOperator<String> headerProcessing = (String text) -> "From Raoul, Mario and Alan: " + text;
		UnaryOperator<String> spellCheckerProcessing = (String text) -> text.replace("labda", "lambda");
		Function<String, String> pipeline = headerProcessing.andThen(spellCheckerProcessing);
		String result = pipeline.apply("Aren't labdas really sexy?!!");
		System.out.println(result);
	}
}
