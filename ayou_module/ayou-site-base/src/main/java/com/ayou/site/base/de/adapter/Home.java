package com.ayou.site.base.de.adapter;

/**
 * @category 在室内充电
 * @author AYOU
 * @version 2016年5月23日 下午4:53:12
 */
public class Home {
	private EnPluginInterface enPlugin;

	public Home() {
	}

	public Home(EnPluginInterface enPlugin) {
		this.enPlugin = enPlugin;
	}

	public void setPlugin(EnPluginInterface enPlugin) {
		this.enPlugin = enPlugin;
	}

	// 充电
	public void charge() {
		enPlugin.chargeWith3Pins();
	}
}
