package com.ayou.site.base.de.observer.v2;

/**
 * @category Subject
 * @author AYOU
 * @version 1.0
 * @since JDK 1.8
 * @date 2017年9月1日 下午2:51:42
 */
public interface Subject {
	void register(Observer observer);
	void remove(Observer observer);
	void send(String msg);
}
