package com.ayou.site.base.de.decorator;

/**
 * @category 装饰金色头发
 * @version 1.0
 * @author AYOU
 * @date 2016年6月8日 上午11:13:24
 */
public class GoldenHair extends GirlDecorator {
	private Girl girl;

	public GoldenHair(Girl g) {
		girl = g;
	}

	@Override
	public String getDescription() {
		return girl.getDescription() + "+金色头发";
	}

}
