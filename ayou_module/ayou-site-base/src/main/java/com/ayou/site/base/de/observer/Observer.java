package com.ayou.site.base.de.observer;

/**
 * @category 观察者接口.java
 * @author AYOU
 * @version 2016年6月3日 上午10:12:08
 */
public interface Observer {
	public void update(Subject s);
}
