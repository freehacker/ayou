package com.ayou.site.base.de.singleton;

/**
 * @category 静态内部类
 * @version 1.0
 * @author AYOU
 * @date 2016年6月8日 上午10:49:47
 * 
 * 上面的方法，修修补补，实在是太复杂了... 
 * 而且 volatile 关键字在某些老版本的 JDK 中无法正常工作。咱们得换一种方法，即 “静态内部类”。
 * 这种方式，利用了 JVM 自身的机制来保证线程安全，因为 Singleton5Holder 类是私有的，
 * 除了 getSingleton5() 之外没有其它方式可以访问实例对象，而且只有在调用 getSingleton5() 时才会去真正创建实例对象。（这里类似于 “懒汉模式”）
 */
public class Singleton5 {
	private static class SingletonHolder {
		private static final Singleton5 singleton = new Singleton5();
	}

	private Singleton5() {
	}

	public static Singleton5 getSingleton() {
		return SingletonHolder.singleton;
	}
}
