package com.ayou.site.base.de.responsibilitychain;

/**
 * @category Mian
 * @author AYOU
 * @version 1.0
 * @since JDK 1.8
 * @date 2017年9月1日 下午4:08:40
 */
public class Mian {
	public static void main(String[] args) {
		ProcessingObject<String> p1 = new HeaderTextProcessing();
		ProcessingObject<String> p2 = new SpellCheckerProcessing();
		p1.setSuccessor(p2);
		String result = p1.handle("Arent't labdas really sexy?!!");
		System.out.println(result);
	}
}
