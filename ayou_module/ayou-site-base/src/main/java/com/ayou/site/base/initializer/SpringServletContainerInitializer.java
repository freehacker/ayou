package com.ayou.site.base.initializer;

import java.util.Set;

import javax.servlet.ServletContainerInitializer;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;

/**
 * @category SpringServletContainerInitializer.java
 * @version 1.0
 * @author AYOU
 * @date 2016年6月13日 上午10:40:10
 */
public class SpringServletContainerInitializer implements ServletContainerInitializer {
	
	@Override
	public void onStartup(Set<Class<?>> c, ServletContext ctx) throws ServletException {
		System.out.println("hello ServletContainerInitializer");
	}

}
