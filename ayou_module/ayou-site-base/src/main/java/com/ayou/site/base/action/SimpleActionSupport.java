package com.ayou.site.base.action;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ui.ModelMap;

import com.ayou.core.commons.exception.ProjectException;
import com.ayou.core.commons.utils.JsonUtil;
import com.ayou.core.commons.utils.StringUtil;

/**
 * @author AYOU
 * @version 2016年3月28日 下午7:49:55
 */
public abstract class SimpleActionSupport {
	protected final Logger logger = LoggerFactory.getLogger(getClass());

	/**
	 * 绕过Template,直接输出内容的简便函数.
	 */
	protected void render(HttpServletResponse response, String text, String contentType) {
		try {
			response.setContentType(contentType);
			response.getWriter().write(text);
		} catch (IOException e) {
			logger.error(e.getMessage(), e);
		}
	}

	/**
	 * 直接输出字符串.
	 */
	protected void renderText(HttpServletResponse response, String text) {
		render(response, text, "text/plain;charset=UTF-8");
	}

	/**
	 * 直接输出HTML.
	 */
	protected void renderHtml(HttpServletResponse response, String html) {
		render(response, html, "text/html;charset=UTF-8");
	}

	/**
	 * 直接输出XML.
	 */
	protected void renderXML(HttpServletResponse response, String xml) {
		render(response, xml, "text/xml;charset=UTF-8");
	}

	/**
	 * 直接输出json.
	 */
	protected void renderJson(HttpServletResponse response, String json) {
		render(response, json, "text/javascript;charset=UTF-8");
	}

	/**
	 * 直接输出byte file.
	 */
	protected void renderFileByte(HttpServletRequest request, HttpServletResponse response, byte[] bytes,
			String fileName) {
		String mime = request.getSession(true).getServletContext().getMimeType(fileName);
		renderFileByte(request, response, bytes, fileName, mime);
	}

	protected void renderFileByte(HttpServletRequest request, HttpServletResponse response, byte[] bytes,
			String fileName, String mime) {
		if (StringUtils.isBlank(mime)) {
			mime = "application/octet-stream";
		}
		mime += ";charset=utf-8";
		response.setContentType(mime);
		response.setHeader("Content-Disposition", "attachment;fileName=\"" + fileName + "\"");
		if (bytes.length > 0) {
			try {
				response.getOutputStream().write(bytes, 0, bytes.length);
				response.flushBuffer();
			} catch (IOException e) {
				logger.error(e.getMessage(), e);
			}
		}
	}

	protected void renderBytes(HttpServletRequest request, HttpServletResponse response, String mime, byte[] bytes) {
		if (StringUtils.isBlank(mime)) {
			mime = "application/octet-stream";
		}
		response.setContentType(mime);
		if (bytes.length > 0) {
			try {
				response.getOutputStream().write(bytes, 0, bytes.length);
				response.flushBuffer();
			} catch (IOException e) {
				logger.error(e.getMessage(), e);
			}
		}
	}

	/**
	 * 用于处理JsonUtil.toJson造成的死循环
	 *
	 * @param map
	 * @return
	 */
	private ModelMap filter(ModelMap map) {
		Iterator<String> iterator = map.keySet().iterator();
		List<String> list = new ArrayList<String>();
		while (iterator.hasNext()) {
			String key = iterator.next();
			if (key.contains(".")) {
				list.add(key);
			}
		}
		for (String key : list) {
			map.remove(key);
		}
		return map;
	}

	// FIXME - renderJsonError/renderJsonMsg暂时采用filter(map);来过滤掉key值带有.的
	// 注：采用JsonUtil.toJson(map,JsonUtil.ExposeAnnotationGson)，会影响到其他的
	// 后续需要进一步修复
	/**
	 * Action执行正常返回消息
	 *
	 * @param map
	 *            其实没什么用
	 * @param msg
	 *            展示消息
	 * @param detail
	 *            要返回的Map数据
	 * @return
	 */
	protected String renderJsonMsg(ModelMap map, String msg) {
		filter(map);
		map.put("success", true);
		map.put("resultMsg", StringUtil.toHtmlString(msg, false).replaceAll("\n", "<br/>").replaceAll("\r", ""));
		return JsonUtil.toJson(map);
	}

	/**
	 * Action执行异常返回消息
	 *
	 * @param map
	 *            其实没什么用
	 * @param msg
	 *            展示消息，因此在调用是不要添加getCause().getMessage()
	 * @param t
	 *            异常
	 * @return
	 */
	protected String renderJsonError(ModelMap map, String resultMsg, Throwable t) {
		filter(map);
		String errMsg = t.getMessage();
		if (t instanceof ProjectException) {
			errMsg = "错误: " + errMsg;
		} else if (t instanceof RuntimeException) {
			errMsg = "运行时错误: " + errMsg;
		} else {
			errMsg = "未知错误: " + errMsg;
		}

		StringWriter writer = new StringWriter();
		t.printStackTrace(new PrintWriter(writer));// t.getCause().printStackTrace(new
													// PrintWriter(writer));会出现空指针错误
		String detailMsg = StringUtil.toHtmlString(writer.getBuffer().toString(), false).replaceAll("\n", "<br/>")
				.replaceAll("\r", "");
		map.put("success", false);
		map.put("resultMsg", StringUtil.toHtmlString(resultMsg, false).replaceAll("\n", "<br/>").replaceAll("\r", ""));
		map.put("errorMsg", errMsg);
		map.put("detailMsg", detailMsg);
		return JsonUtil.toJson(map);// JsonUtil.toJson(map,
									// JsonUtil.ExposeAnnotationGson);
	}
}
