package com.ayou.site.base.study.java8.lambda;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @category Filter
 * @author AYOU
 * @version 1.0
 * @since JDK 1.8
 * @date 2017年5月19日 下午3:19:07
 */
@SuppressWarnings("unused")
public class Filter {

	public static void main(String[] args) {
		List<Apple> apples = Arrays.asList(
				new Apple(100, "red"),
				new Apple(200, "黑"),
				new Apple(300, "绿"),
				new Apple(400, "waite"),
				new Apple(500, "gray"),
				new Apple(900, "春"),
				new Apple(700, "紫色"),
				new Apple(800, "red"));

		// apples.stream().filter(x->{System.out.println(x.getColor()); return true;});
		// apples.forEach(x->{System.out.println(x.getColor());});
		// apples.stream().forEach(x->{System.out.println(x.getColor());;});
		List<Apple> red = apples.stream().filter(a -> a.getColor() == "red").collect(Collectors.toList());
		red.forEach(a -> {
			System.out.println(a.getWeight());
		});

		long uniqueWords = 0;
		String f = "";
		Integer i = 0;
		for (;;) {
			final String aaa = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx";
			try (Stream<String> lines = Files.lines(
					Paths.get("C:/developer/code/ayou/ayou_module/ayou-site-base/src/main/java/com/ayou/site/base/study/java8/lambda/data.txt"),
					Charset.defaultCharset())) {
				uniqueWords = lines.flatMap(line -> Arrays.stream(line.split(""))).distinct().count();
				//System.out.println(uniqueWords);
				//System.out.println(i);
				f += aaa;
				i++;
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
	}

}
