package com.ayou.site.base.study.java8.methodreference;

import java.util.Arrays;
import java.util.List;


/**
 * @category Main
 * @author AYOU
 * @version 1.0
 * @since JDK 1.8
 * @date 2017年8月28日 下午12:00:22
 */
public class FilterApples {

	public static void main(String[] args) {
		List<Apple> inventory = Arrays.asList(new Apple("green", 120), new Apple("red", 151), new Apple("green", 152));
		List<Apple> green = Apple.filterApples(inventory, Apple::isGreenApple);
		List<Apple> heavy = Apple.filterApples(inventory, Apple::isHeavyApple);
		System.out.println(green);
		System.out.println(heavy);
		
		List<Apple> green2 = Apple.filterApples(inventory, (Apple a) -> "green".equals(a.getColor()));
		List<Apple> heavy2 = Apple.filterApples(inventory, (Apple a) -> a.getWeight() > 150);
		List<Apple> gh2 = Apple.filterApples(inventory, (Apple a) -> a.getWeight() > 150 && "green".equals(a.getColor()));
		
		System.out.println(green2);
		System.out.println(heavy2);
		System.out.println(gh2);
	}
}
