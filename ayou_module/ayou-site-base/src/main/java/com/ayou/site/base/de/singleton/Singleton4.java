package com.ayou.site.base.de.singleton;

/**
 * @category 双重检验锁（double check）
 * @version 1.0
 * @author AYOU
 * @date 2016年6月8日 上午10:49:47
 * 
 * 线程安全的懒汉模式解决了多线程的问题，看起来完美了。
 * 但是它的效率不高，每次调用获得实例的方法 getSingleton() 时都要进行同步，
 * 但是多数情况下并不需要同步操作（例如我的 Singleton4 实例并不为空可以直接使用的时候，
 * 就不需要给 getSingleton() 加同步方法，直接返回 Singleton4 实例就可以了）。
 * 所以只需要在第一次新建实例对象的时候，使用同步方法。

 * 不怕，程序猿总是有办法的。于是，在前面的基础上，又有了 “双重检验锁” 的方法。
 */
public class Singleton4 {
	private static Singleton4 singleton;

	private Singleton4() {
	}

	// 双重锁的 getWife() 方法
	public static Singleton4 getSingleton() {
		// 第一个检验锁，如果不为空直接返回实例对象，为空才进入下一步
		if (singleton == null) {
			synchronized (Singleton4.class) {
				// 第二个检验锁，因为可能有多个线程进入到 if 语句内
				if (singleton == null) {
					singleton = new Singleton4();
				}
			}
		}
		return singleton;
	}
}
