package com.ayou.site.base.study;

/**
 * @category 数据类型相关
 * @author AYOU
 * @version 2016年5月25日 下午10:49:24
 */
public class Data {
	public static Integer at(int a, int b){
		return a&b;
	}
	
	@SuppressWarnings("unused")
	public static void main(String[] args) {
		Integer ia = 100;
		Integer ib = new Integer(100);
		System.out.println(ia == ib);
		System.out.println(ia.hashCode() == ib.hashCode());
		
		Integer ib2 = new Integer(100);
		Integer ia2 = 100;
		Integer ic2 = new Integer(100);
		System.out.println(ia2 == ib2);
		System.out.println(ia2 == ic2);
		System.out.println(ia2.hashCode() == ib2.hashCode());
		
		System.out.println();
		
		System.out.println("总内存：" + Runtime.getRuntime().totalMemory());
		System.out.println("TOTAL：" + Runtime.getRuntime().totalMemory()/(1024*1024));
		System.out.println("Max：" + Runtime.getRuntime().maxMemory()/(1024*1024));
		long bm = Runtime.getRuntime().freeMemory();
		System.out.println("NEW前：" + bm);
		//char[] ch = new char[1024*1024];
		//char[] ch1 = new char[1024*1024];
		String a = new String(
				  "AYOUAYOUAYOUAYOUAYOUAYOUAYOUAYOUAYOUAYOUAYOUAYOUAYOUAYOUAYOUAYOUAYOUAYOUAYOUAYOUAYOUAYOUAYOUAYOUAYOUAYOUAYOUAYOUAYOUAYOUAYOUAYOUAYOUAYOUAYOUAYOUAYOUAYOUAYOUAYOU"
				+ "AYOUAYOUAYOUAYOUAYOUAYOUAYOUAYOUAYOUAYOUAYOUAYOUAYOUAYOUAYOUAYOUAYOUAYOUAYOUAYOUAYOUAYOUAYOUAYOUAYOUAYOUAYOUAYOUAYOUAYOUAYOUAYOUAYOUAYOUAYOUAYOUAYOUAYOUAYOUAYOU"
				+ "AYOUAYOUAYOUAYOUAYOUAYOUAYOUAYOUAYOUAYOUAYOUAYOUAYOUAYOUAYOUAYOUAYOUAYOUAYOUAYOUAYOUAYOUAYOUAYOUAYOUAYOUAYOUAYOUAYOUAYOUAYOUAYOUAYOUAYOUAYOUAYOUAYOUAYOUAYOUAYOU"
				);
		char[] ccc= new char[a.length()*1024];
		/*byte[] bt = new byte[1024*1024];
		byte[] bt1 = new byte[1024*1024];
		byte[] bt2 = new byte[1024*1024];*/
		long em = Runtime.getRuntime().freeMemory();
		System.out.println("NEW后：" + em);
		System.out.println("计算结果：" + (bm - em));
		System.out.println("计算结果：" + (bm - em)/(1024*1024));
		
		//System.out.println(2==2&3==1);
		
		/**
		 * 
		 * Integer ia2 = 100; 会在栈上寻找值为 100 的地址，没有则开辟一个存放字面值为100的地址 ，有则返回地址 然后将 ia2 指向 值为100的地址
		 * Integer ib = new Integer(100); 首先在 栈上 创建 变量 ib  然后 在堆中创建 100 的对象 最后 把 堆中的地址 引向 栈的变量
		 */
		
	}
}
