package com.ayou.site.base.study.thread.pool;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * @category ScheduledThreadPool
 * @author AYOU
 * @version 1.0
 * @since JDK 1.8
 * @date 2017年1月13日 下午3:49:54
 */
public class ScheduledThreadPool {

	/*
	 * 延迟3秒执行
	 */
	public void exec1() {
		ScheduledExecutorService scheduledThreadPool = Executors.newScheduledThreadPool(5);
		scheduledThreadPool.schedule(new Runnable() {
			@Override
			public void run() {
				System.out.println("delay 3 seconds");
			}
		}, 3, TimeUnit.SECONDS);
	}

	/*
	 * 表示延迟1秒后每3秒执行一次。 ScheduledExecutorService比Timer更安全，功能更强大。
	 * 
	 */
	public void exec2() {
		ScheduledExecutorService scheduledThreadPool = Executors.newScheduledThreadPool(5);
		scheduledThreadPool.scheduleAtFixedRate(new Runnable() {
			@Override
			public void run() {
				System.out.println("delay 1 seconds, and excute every 3 seconds");
			}
		}, 3, 3, TimeUnit.SECONDS);
	}

	public static void main(String[] args) {
		ScheduledThreadPool stp = new ScheduledThreadPool();
		stp.exec1();
		stp.exec2();
	}
}
