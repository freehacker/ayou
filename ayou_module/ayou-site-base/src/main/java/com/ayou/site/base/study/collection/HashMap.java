package com.ayou.site.base.study.collection;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;



/**
 * @category HashMap.java
 * @author AYOU
 * @version 2016年6月6日 上午9:41:11
 */
public class HashMap {
	public static void main(String[] args) {
		Map<Integer, String> map = Collections.synchronizedMap(new java.util.HashMap<Integer, String>());
		map.putIfAbsent(null, null);
		map.putIfAbsent(1, "null");

		System.out.println(map.size());
		System.out.println(map.get(null));
		System.out.println(map.containsKey(null));

		Object o[] = map.keySet().toArray();
		for (int i = 0; i < map.size(); i++) {
			System.out.print(map.get(o[i]));
		}
		
		
		
		List<Object> list = Arrays.asList(map.keySet().toArray());
		//System.out.println(list(1));
		//stream = list.stream();
		
		long sun = list.stream().filter(l -> l == "null").count();
		System.out.println(sun);

		map.forEach((id, val) -> System.out.print(val));
		
		//int sum = Collection.stream().filter(map.get(key) -> map.get(key) == "").mapToInt(w -> w.getWeight()).sum();

	}
}
