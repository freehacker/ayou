package com.ayou.site.base.de.builder.impl;

import com.ayou.site.base.de.builder.Builder;
import com.ayou.site.base.de.builder.Part;
import com.ayou.site.base.de.builder.Product;

/**
 * @category Builder的具体实现
 * @version 1.0
 * @author AYOU
 * @date 2016年6月8日 上午10:02:20
 * 
 * Builder的具体实现ConcreteBuilder:
 * 通过具体完成接口Builder来构建或装配产品的部件;
 * 定义并明确它所要创建的是什么具体东西;
 * 提供一个可以重新获取产品的接口:
 */
public class ConcreteBuilder implements Builder {

	Part partA, partB, partC;

	@Override
	public void buildPartA() {
		// 这里是具体如何构建partA的代码
		
	}

	@Override
	public void buildPartB() {
		// 这里是具体如何构建parB的代码

	}

	@Override
	public void buildPartC() {
		// 这里是具体如何构建partC的代码

	}

	@Override
	public Product getResult() {
		// 返回最后组装成品结果
		return null;
	}

}
