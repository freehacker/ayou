package com.ayou.site.base.de.singleton;

/**
 * @category 懒汉模式
 * @version 1.0
 * @author AYOU
 * @date 2016年6月8日 上午10:43:16
 * 
 * 最常见、最简单的单例模式之二，跟 “饿汉模式” 是 “好基友”。
 * 再次顾名思义，“懒汉模式” 就是它很懒，一开始不新建实例，
 * 只有当它需要使用的时候，会先判断实例是否为空，如果为空才会新建一个实例来使用。
 */
public class Singleton2 {
	// 一开始没有新建实例
	private static Singleton2 singleton;

	private Singleton2() {
	}

	// 需要时再新建
	public static Singleton2 getSingleton() {
		if (singleton == null) {
			singleton = new Singleton2();
		}
		return singleton;
	}
}
