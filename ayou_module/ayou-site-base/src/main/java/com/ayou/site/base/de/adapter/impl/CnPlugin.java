package com.ayou.site.base.de.adapter.impl;

import com.ayou.site.base.de.adapter.CnPluginInterface;

/**
 * @category 实现国际接口
 * @author AYOU
 * @version 2016年5月23日 下午5:04:18
 */
public class CnPlugin implements CnPluginInterface {
	@Override
	public void chargeWith2Pins() {
		System.out.println("启用国际接口...");
	}
}
