package com.ayou.site.base.de.adapter.impl;

import com.ayou.site.base.de.adapter.CnPluginInterface;
import com.ayou.site.base.de.adapter.EnPluginInterface;

/**
 * @category 适配器
 * @author AYOU
 * @version 2016年5月23日 下午5:06:02
 */
public class PluginAdapter implements EnPluginInterface {

	private CnPluginInterface cnPlugin;

	public PluginAdapter(CnPluginInterface cnPlugin) {
		this.cnPlugin = cnPlugin;
	}

	// 这是重点，适配器实现了英标的接口，然后重载英标的充电方法为国标的方法
	@Override
	public void chargeWith3Pins() {
		cnPlugin.chargeWith2Pins();
	}

}
