package com.ayou.site.base.de.adapter;

import com.ayou.site.base.de.adapter.impl.CnPlugin;
import com.ayou.site.base.de.adapter.impl.PluginAdapter;

/**
 * @category 测试适配器
 * @author AYOU
 * @version 2016年5月23日 下午5:07:59
 */
public class AdapterTest {
	public static void main(String[] args) {
		CnPluginInterface cnPlugin = new CnPlugin();
		Home home = new Home();
		PluginAdapter pluginAdapter = new PluginAdapter(cnPlugin);
		home.setPlugin(pluginAdapter);

		home.charge();
	}
}
