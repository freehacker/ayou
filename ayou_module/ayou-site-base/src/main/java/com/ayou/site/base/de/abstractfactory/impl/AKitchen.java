package com.ayou.site.base.de.abstractfactory.impl;

import com.ayou.site.base.de.abstractfactory.AbstractFactory;
import com.ayou.site.base.de.abstractfactory.Food;
import com.ayou.site.base.de.abstractfactory.TableWare;

/**
 * @category 以具体工厂 AKitchen 为例
 * @author AYOU
 * @version 2016年5月23日 下午4:30:12
 */
public class AKitchen implements AbstractFactory {

	@Override
	public Food getFood() {
		return new Apple();
	}

	@Override
	public TableWare getTableWare() {
		return new Knife();
	}

}
