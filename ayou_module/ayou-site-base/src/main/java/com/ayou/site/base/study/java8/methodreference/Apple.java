package com.ayou.site.base.study.java8.methodreference;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

import com.ayou.site.base.study.t.T;

/**
 * @category Apple
 * @author AYOU
 * @version 1.0
 * @since JDK 1.8
 * @date 2017年8月28日 下午2:06:19
 */
public class Apple {
	private int weight = 0;
	private String color = "";

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}

	public Apple(String color, int weight) {
		this.color = color;
		this.weight = weight;
	}

	public Apple() {
	}

	public static boolean isGreenApple(Apple apple) {
		return "green".equals(apple.getColor());
	}

	public static boolean isHeavyApple(Apple apple) {
		return apple.getWeight() > 150;
	}

	@SuppressWarnings("hiding")
	public interface MyPredicate<T>{
		boolean isA(T t);
	}
	public static List<Apple> filterApples(List<Apple> inventory, Predicate<Apple> p) {
		List<Apple> result = new ArrayList<>();
		for (Apple apple : inventory) {
			if (p.test(apple)) {
				result.add(apple);
			}
		}
		return result;
	}
	@SuppressWarnings("hiding")
	public static <T> List<T> filterApple2(List<T> list, MyPredicate<T> p) {
		List<T> result = new ArrayList<>();
		for (T t : list) {
			if (p.isA(t)) {
				result.add(t);
			}
		}
		return result;
	}
	
	public String toString() {
		return "{" + "color='" + color + '\'' + ", weight=" + weight + '}';
	}
}
