package com.ayou.site.base.study;

import java.math.BigDecimal;

/**
 * @category 学习相关
 * @author AYOU
 * @version 2016年5月24日 上午9:07:10
 */
public class Stady{
	static String strA = "hello";
	static String strB = "word";
	static StringBuffer sb = new StringBuffer();

	public static String add(String a, String b) {
		/*
		 * return a + b; return a.concat(b);
		 */

		String c = sb.append(a).append(b).toString();
		return c;
	}

	public static void main(String[] args) {
		/*System.out.println(add(strA, strB));
		String str = "一二三abc";
		for (byte b : str.getBytes()) {
			String temp1 = String.valueOf(b);
			System.out.print(temp1 + "  |  ");
			String temp2 = new String(new byte[] { b });
			System.out.print(temp2 + "  |  ");
			String temp3 = String.valueOf(new byte[] { b });
			System.out.println(temp3);
		}*/
		 

		/*
		 * for (int i = -1; i < 1000000; i++) { Object o = new Object();
		 * System.out.println(o.hashCode()); }
		 */

		Long bdt = System.currentTimeMillis();
		Double pai = new Double(354.99997 / 113);
		Long edt = System.currentTimeMillis();

		System.out.println(pai.hashCode());
		System.out.println(pai);
		String p = pai.toString();
		System.out.println("精度：" + p.substring(p.lastIndexOf(".") + 1, p.length()).length());
		System.out.println("耗时：" + (edt - bdt) + " 毫秒");

		System.out.println("\n");

		Long bb = System.currentTimeMillis();
		BigDecimal bd = new BigDecimal(354.99997 / 113);
		Long eb = System.currentTimeMillis();

		System.out.println(bd.hashCode());
		System.out.println(bd);
		String b = bd.toPlainString();
		System.out.println("精度：" + b.substring(b.lastIndexOf(".") + 1, b.length()).length());
		System.out.println("耗时：" + (eb - bb) + " 毫秒");
		
		System.out.println("\n");
		
	/*	String aStr = new String("ayou");
		String bStr = new String("ayou");
		
		System.out.println(aStr == bStr);
		System.out.println(aStr.equals(bStr));
		System.out.println(aStr.hashCode() == bStr.hashCode());*/
		
		/*Hashtable<Double, String> ht = new Hashtable<>();
		HashMap<Integer, String> hm = new HashMap<>();
		
		Integer aInt = 100;
		Integer bInt = 3;
		System.out.println(aInt >> bInt);
		System.out.println(aInt / (bInt*2));*/
		
		/*char a = 'a';
		
		ConsoleWrite cw = new ConsoleWrite();
		try {
			cw.write(strA, 0, strA.length());
		} catch (IOException e) {
			e.printStackTrace();
		}
		*/
		
		String charStr = new String();
		for (int i = 0; i < (1024 * 1024); i++) {
			char c = 'a';
			c = (char) (c + (int) (Math.random() * 26));
			charStr += c;
		}
		Long bcharStr = System.currentTimeMillis();
		System.out.println(charStr);
		Long echarStr = System.currentTimeMillis();

		System.out.println("time：" + (echarStr - bcharStr));
		System.out.println(charStr.length());
	}
	
	static final int hash(Object key) {
        int h;
        return (key == null) ? 0 : (h = key.hashCode()) ^ (h >>> 16);
        
    }
}
