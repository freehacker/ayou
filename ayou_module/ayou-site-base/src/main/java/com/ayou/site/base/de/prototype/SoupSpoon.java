package com.ayou.site.base.de.prototype;

/**
 * @category SoupSpoon
 * @version 1.0
 * @author AYOU
 * @date 2016年6月8日 上午9:22:30
 */
public class SoupSpoon extends AbstractSpoon {

	public SoupSpoon() {
		setSpoonName("Soup Spoon");
	}
}
