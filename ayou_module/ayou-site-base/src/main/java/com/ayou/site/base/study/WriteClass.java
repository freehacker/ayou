package com.ayou.site.base.study;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * @category 读写文件
 * @author AYOU
 * @version 2016年5月25日 下午3:23:39
 */
public class WriteClass{
	@SuppressWarnings({ "unused", "resource" })
	public static void w() {
		String charStr = new String();
		for (int i = 0; i < (1024 * 300); i++) {
			char c = 'a';
			c = (char) (c + (int) (Math.random() * 26));
			charStr += c;
		}
		Long bcharStr = System.currentTimeMillis();
		//System.out.println(charStr);
		Long echarStr = System.currentTimeMillis();

		System.out.println("time:" + (echarStr - bcharStr) + "\t length:" + charStr.length());
		
		// 字节流
		File fileByte = new File("E:/JAVA", "WriteByte.txt"); //C:/Users/Ayou/Desktop
		File fileChar = new File("E:/JAVA", "WriteChar.txt");
		
		try { //创建文件
			fileByte.createNewFile();
			fileChar.createNewFile();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		byte bt[] = new byte[1024*1024];
		bt = charStr.getBytes();
		try {//Write
			FileOutputStream in = new FileOutputStream(fileByte);
			try {
				Long bfb = System.currentTimeMillis();
				in.write(bt, 0, bt.length);
				in.close();
				Long efb = System.currentTimeMillis();
				System.out.println("字符文件写入完成...  用时：" + (efb - bfb) + "ms");
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		try {//Reader
			FileInputStream out = new FileInputStream(fileByte);
			InputStreamReader isr = new InputStreamReader(out);
			int ch = 0;
			while ((ch = isr.read()) != -1) {
				//System.out.print((char) ch);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		// 字符流
		Long bfc = System.currentTimeMillis();
		BufferedReader read = null;
		BufferedWriter writer = null;
		try {
			read = new BufferedReader(new FileReader(fileByte));
			writer = new BufferedWriter(new FileWriter(fileChar));
			String tempString = null;
			while ((tempString = read.readLine()) != null) {
				writer.append(tempString);
				//writer.newLine(); //换行
				writer.flush(); // 需要及时清掉流的缓冲区，万一文件过大就有可能无法写入了
			}
			read.close();
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		Long efc = System.currentTimeMillis();
		System.out.println("字符文件写入完成...  用时：" + (efc - bfc) + "ms");
	}
}
