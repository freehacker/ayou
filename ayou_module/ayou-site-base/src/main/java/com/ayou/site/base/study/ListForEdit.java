package com.ayou.site.base.study;

import java.util.ArrayList;
import java.util.List;

import com.ayou.core.commons.utils.UUIDUtil;

/**
 * @category ListForEdit
 * @author AYOU
 * @version 1.0
 * @since JDK 1.8
 * @date 2017年5月13日 下午6:17:12
 */
public class ListForEdit {
	public static void main(String[] args) {
		List<A> list = new ArrayList<A>();
		for (int i = 0; i < 10; i++) {
			A a = new A();
			a.id = UUIDUtil.generate();
			a.code = i + "";
			list.add(a);
		}
		for (A a : list) {
			System.out.print(a.id + "----" + a.code);
			System.out.print(">>>>");
			a.id = UUIDUtil.generate();
			a.code = a.code + "x";
			System.out.println(a.id + "----" + a.code);
		}
		for (A a : list) {
			System.out.println(a.id + "--修改后--" + a.code);
		}
	}
}

class A {
	String id;
	String code;
	String name;
	String addr;
}
