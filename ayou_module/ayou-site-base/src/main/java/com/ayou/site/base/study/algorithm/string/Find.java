package com.ayou.site.base.study.algorithm.string;

/**
 * @category 字符串查找
 * @author   AYOU
 * @version  1.0
 * @since    JDK 1.8
 * @date     2016年11月22日 下午4:10:31
 */
public class Find {
	public static int findStr(String haystack, String needle) {
		if (haystack == null && needle == null)
			return 0;
		if (haystack == null)
			return -1;
		if (needle == null)
			return 0;

		for (int i = 0; i < haystack.length() - needle.length() + 1; i++) {
			int j = 0;
			for (; j < needle.length(); j++) {
				if (haystack.charAt(i + j) != needle.charAt(j))
					break;
			}
			if (j == needle.length())
				return i;
		}

		return -1;
	}
}
