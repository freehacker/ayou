package com.ayou.site.base.study.java8.methodreference.c2;

/**
 * @category Runnable
 * @author AYOU
 * @version 1.0
 * @since JDK 1.8
 * @date 2017年8月28日 下午4:21:23
 */
public class MyRunnable {
	public interface Runnable {
		public void run();
	}

	public static void main(String[] args) {
		/*Thread t = new Thread(new Runnable() {
			@Override
			public void run() {
				System.out.println("hello world");
			}
		});*/
		@SuppressWarnings("unused")
		Thread t = new Thread(()->System.out.println("hello world"));
	}
}
