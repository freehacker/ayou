package com.ayou.site.base.stady.annotations;

/**
 * @category M.java
 * @version 1.0
 * @author AYOU
 * @date 2016年7月22日 下午3:54:07
 */
public class M {
	
	String clazz = this.getClass().getName();
	
	@Ayou(isAuthor = true)
	public static String hello() {
		
		return "hello";
	}

	public static void main(String[] args) {
		System.out.println(hello());
	}
}
