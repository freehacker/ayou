package com.ayou.site.base.stady.arr;

/**
 * @category 多维数组
 * @author AYOU
 * @version 2016年5月27日 上午9:56:21
 */
public class A2 {

	public static void main(String[] args) {
		int arr[][] = { { 1, 2, 3 }, { 4, 5, 6, 7 }, { 9 } };
		
		int arr1[][][] = {
				{ { 1, 2, 3, 4, 5, 6, 7, 8, 9 }, { 10, 11 }, { 908, 413 } },
				{ { 34, 765, 3452, 12, 565 }, { 456, 87, 2, 45, }, { 637, 234, 345, 6 }, { 3 }, { 2, 4, 1 } },
				{ { 3, 5 } } 
		};
		
		boolean found = false;
		System.out.println(arr[1][1]);
		System.out.println(arr1[0][0].length);
		
		for (int i = 0; i < arr.length && !found; i++) {
			for (int j = 0; j < arr[i].length; j++) {
				System.out.println("i=" + i + ",j=" + j);
				if (arr[i][j] == 5) {
					found = true;
					break;
				}
			}
		}
		
	}
}
