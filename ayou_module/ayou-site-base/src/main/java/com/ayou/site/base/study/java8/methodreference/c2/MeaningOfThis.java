package com.ayou.site.base.study.java8.methodreference.c2;

/**
 * @category MeaningOfThis
 * @author AYOU
 * @version 1.0
 * @since JDK 1.8
 * @date 2017年8月28日 下午4:19:29
 */
public class MeaningOfThis {
	public final int value = 4;

	@SuppressWarnings("unused")
	public void doIt() {
		int value = 6;
		Runnable r = new Runnable() {
			public final int value = 5;

			public void run() {
				int value = 10;
				System.out.println(this.value);
			}
		};
		r.run();
	}

	public static void main(String... args) {
		MeaningOfThis m = new MeaningOfThis();
		m.doIt(); // 5
	}
}
