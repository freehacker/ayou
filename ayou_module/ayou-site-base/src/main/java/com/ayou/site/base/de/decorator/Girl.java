package com.ayou.site.base.de.decorator;

/**
 * @category 抽象女孩
 * @version 1.0
 * @author AYOU
 * @date 2016年6月8日 上午11:09:01
 */
public abstract class Girl {
	String description = "no particular";

	public String getDescription() {
		return description;
	}
}
