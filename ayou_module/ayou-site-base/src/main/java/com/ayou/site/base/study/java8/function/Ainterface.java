package com.ayou.site.base.study.java8.function;

/**
 * @category Ainterface
 * @author AYOU
 * @version 1.0
 * @since JDK 1.8
 * @date 2017年9月1日 下午4:51:41
 */
public interface Ainterface {
	void speck(String msg);

	default void say(String msg) {
		System.out.println(msg);
	}
}
