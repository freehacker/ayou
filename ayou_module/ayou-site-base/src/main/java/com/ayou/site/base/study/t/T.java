package com.ayou.site.base.study.t;

import com.ayou.core.commons.utils.UUIDUtil;

/**
 * @category 泛型
 * @author AYOU
 * @version 1.0
 * @since JDK 1.8
 * @date 2017年4月14日 下午5:15:04
 */
public class T {
	@SuppressWarnings({ "hiding", "unchecked" })
	public static <T> T getMiddle(T... a) {
		System.out.println(a[1]);
		return a[a.length / 2];
	}
	
	public static void main(String[] args) {
		System.out.println(getMiddle(1,3,5,1000,7,9));
		System.out.println(UUIDUtil.generate());
	}
}

