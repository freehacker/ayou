package com.ayou.site.base.de.abstractfactory;

import com.ayou.site.base.de.abstractfactory.impl.AKitchen;

/**
 * @category 测试
 * @author AYOU
 * @version 2016年5月23日 下午4:38:37
 */
public class Main {

	public void eat(AbstractFactory a) {
		System.out.println("我吃 " + a.getFood().getFoodName() + " 用 " + a.getTableWare().getToolName());
	}

	public static void main(String[] args) {
		Main m = new Main();
		AbstractFactory ak = new AKitchen();
		m.eat(ak);
	}

}
