package com.ayou.site.base.de.abstractfactory;

/**
 * @category 抽象餐具
 * @author AYOU
 * @version 2016年5月23日 下午4:29:31
 */
public interface TableWare {
	public String getToolName();
}
