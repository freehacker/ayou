package com.ayou.site.base.stady.clac;

import java.math.BigInteger;

/**
 * @category 超大整数计算,其实用 BigInteger就行了
 * @author AYOU
 * @version 2016年5月27日 上午10:15:25
 * 
 * 题目要求：如果系统要使用超大整数（超过long的范围），请你设计一个数据结构来存储这种 
 * 超大型数字以及设计一种算法来实现超大整数的加法运算 
 * @author Administrator 
 */  
public class BigInt {
	
	public static void main(String[] args) {
		String a = "1234532434555356349945234677599241234999999123523453664563634123453243455535634995252345234677576252241234123523453664563699";
		String b = "123453243455535634539";
		System.out.println(a + " + " + b + " = " + add(a, b));
		System.out.println(a + " × " + b + " = " + bigIntMultiply(a, b, Math.max(a.length(), b.length())));
		System.out.println(a + " ÷ " + b + " = " + divide(a, b));
		
    }  
    /** 
     * 
     * @param a 加数字符串1 
     * @param b 加数字符串2 
     * @return 结果字符串 
     * 分析： 
     * 1、取得两个字符串的长度 
     * 2、把两个的长度做比较，并得出较长的长度，及较短的长度 
     * 3、把长度较短的加数字符串，在左面补0，使之与较长的字符串一样长 
     * 4、从最高位，一个个数的取出来相加，当然首先得转换为整型 
     * 5、设置进位，如果两个数相加及加上进位大于等于10，并且这不是最左边一个字符相加，相加结果等于 
     *    （取出1＋取出2＋进位）－10，并把进位设为1；如果没有大于10，就把进位设为0，如些循环，把 
     *    相加的结果以字符串的形式结合起来，就得到最后的结果 
     */  
    static String add(String a,String b)  
    {  
		String str = "";
		int lenA = a.length();
		int lenB = b.length();
		int maxLen = (lenA > lenB) ? lenA : lenB;
		int minLen = (lenA < lenB) ? lenA : lenB;
		String strTmp = "";
		for (int i = maxLen - minLen; i > 0; i--) {
			strTmp += "0";
		}
		// 把长度调整到相同
		if (maxLen == lenA) {
			b = strTmp + b;
		} else
			a = strTmp + a;
		int JW = 0;// 进位
		for (int i = maxLen - 1; i >= 0; i--) {
			int tempA = Integer.parseInt(String.valueOf(a.charAt(i)));
			int tempB = Integer.parseInt(String.valueOf(b.charAt(i)));
			int temp;
			if (tempA + tempB + JW >= 10 && i != 0) {
				temp = tempA + tempB + JW - 10;
				JW = 1;
			} else {
				temp = tempA + tempB + JW;
				JW = 0;
			}
			str = String.valueOf(temp) + str;
		}
		return str;
	}
    
    
    
	/**
	 * 乘法
	 * 
	 */

	// 规模只要在这个范围内就可以直接计算了
	private final static int SIZE = 4;
	// 此方法要保证入参len为X、Y的长度最大值
	private static String bigIntMultiply(String X, String Y, int len) {
		// 最终返回结果
		String str = "";
		// 补齐X、Y，使之长度相同
		X = formatNumber(X, len);
		Y = formatNumber(Y, len);

		// 少于4位数，可直接计算
		if (len <= SIZE) {
			return "" + (Integer.parseInt(X) * Integer.parseInt(Y));
		}
		// 将X、Y分别对半分成两部分
		int len1 = len / 2;
		int len2 = len - len1;
		String A = X.substring(0, len1);
		String B = X.substring(len1);
		String C = Y.substring(0, len1);
		String D = Y.substring(len1);
		// 乘法法则，分块处理
		int lenM = Math.max(len1, len2);
		String AC = bigIntMultiply(A, C, len1);
		String AD = bigIntMultiply(A, D, lenM);
		String BC = bigIntMultiply(B, C, lenM);
		String BD = bigIntMultiply(B, D, len2);
		// 处理BD，得到原位及进位
		String[] sBD = dealString(BD, len2);

		// 处理AD+BC的和
		String ADBC = addition(AD, BC);
		// 加上BD的进位
		if (!"0".equals(sBD[1])) {
			ADBC = addition(ADBC, sBD[1]);
		}
		// 得到ADBC的进位
		String[] sADBC = dealString(ADBC, lenM);
		// AC加上ADBC的进位
		AC = addition(AC, sADBC[1]);
		// 最终结果
		str = AC + sADBC[0] + sBD[0];
		return str;
	}

	// 两个数字串按位加
	private static String addition(String ad, String bc) {
		// 返回的结果
		String str = "";

		// 两字符串长度要相同
		int lenM = Math.max(ad.length(), bc.length());
		ad = formatNumber(ad, lenM);
		bc = formatNumber(bc, lenM);

		// 按位加，进位存储在temp中
		int flag = 0;

		// 从后往前按位求和
		for (int i = lenM - 1; i >= 0; i--) {
			int t = flag + Integer.parseInt(ad.substring(i, i + 1)) + Integer.parseInt(bc.substring(i, i + 1));

			// 如果结果超过9，则进位当前位只保留个位数
			if (t > 9) {
				flag = 1;
				t = t - 10;
			} else {
				flag = 0;
			}
			// 拼接结果字符串
			str = "" + t + str;
		}
		if (flag != 0) {
			str = "" + flag + str;
		}
		return str;
	}

	// 处理数字串，分离出进位；
	// String数组第一个为原位数字，第二个为进位
	private static String[] dealString(String ac, int len1) {
		String[] str = { ac, "0" };
		if (len1 < ac.length()) {
			int t = ac.length() - len1;
			str[0] = ac.substring(t);
			str[1] = ac.substring(0, t);
		} else {
			// 要保证结果的length与入参的len一致，少于则高位补0
			String result = str[0];
			for (int i = result.length(); i < len1; i++) {
				result = "0" + result;
			}
			str[0] = result;
		}
		return str;
	}

	// 乘数、被乘数位数对齐
	private static String formatNumber(String x, int len) {
		while (len > x.length()) {
			x = "0" + x;
		}
		return x;
	}
	
	private static BigInteger divide(String at, String bt) {
		BigInteger a = new BigInteger(at);
		BigInteger b = new BigInteger(bt);
		return a.divide(b);
	}
}
