package com.ayou.site.base.study.jvm;

import java.io.IOException;
import java.util.Arrays;

/**
 * @category F00F
 * @author AYOU
 * @version 1.0
 * @since JDK 1.8
 * @date 2017年6月7日 上午11:15:14
 */
public class F00F {
	public static int BC_INT_SHORT_ZERO = 0xd4;
	public static int INT_SHORT_MIN = -0x40000;
	public static int INT_SHORT_MAX = 0x3ffff;
	public static final int BC_LONG_SHORT_ZERO = 0x3c;
	public static int x = 0b0100_0000_0000; //2的10次方

	public static void main(String[] args) throws IOException {
		int value = 101910;
		byte[] buffer = new byte[3];
		int offset = 0;
		if (INT_SHORT_MIN <= value && value <= INT_SHORT_MAX) {
			buffer[offset++] = (byte) (BC_INT_SHORT_ZERO + (value >> 16));
			buffer[offset++] = (byte) (value >> 8);
			buffer[offset++] = (byte) (value);
		}

		System.out.println(value);
		System.out.println(Arrays.toString(buffer));
		System.out.printf("%02x %02x %02x\n", buffer[0], buffer[1], buffer[2]);
		System.out.printf("%d\n", ((buffer[0] - BC_LONG_SHORT_ZERO) << 16) + 256 * buffer[1] + buffer[2]);
		System.out.printf("%d\n", Long.valueOf(((buffer[0] - BC_LONG_SHORT_ZERO) << 16) + 256 * buffer[1] + buffer[2]));
		//return Long.valueOf(((tag - BC_LONG_SHORT_ZERO) << 16) + 256 * read()+ read());
		System.out.println(Math.pow(2, 10));
		System.out.println(x);
	}
}
