package com.ayou.site.base.de.responsibilitychain;

/**
 * @category ProcessingObject
 * @author AYOU
 * @version 1.0
 * @since JDK 1.8
 * @date 2017年9月1日 下午4:00:57
 */
public abstract class ProcessingObject<T> {
	protected ProcessingObject<T> successor;

	public void setSuccessor(ProcessingObject<T> successor) {
		this.successor = successor;
	}

	public T handle(T input) {
		T r = handleWork(input);
		if (successor != null) {
			return successor.handle(input);
		}
		return r;
	}

	abstract protected T handleWork(T input);
}
