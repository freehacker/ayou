package com.ayou.site.base.study.java8.function;

/**
 * @category Main
 * @author   AYOU
 * @version  1.0
 * @since    JDK 1.8
 * @date    2017年9月1日 下午4:54:07
 */
public class Main {
	public static void main(String[] args) {
		AinterfaceImpl ai =new AinterfaceImpl();
		ai.say("say");
		ai.speck("speck");
	}
}

