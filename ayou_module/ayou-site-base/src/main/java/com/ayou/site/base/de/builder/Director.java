package com.ayou.site.base.de.builder;

/**
 * @category 如何将部件最后组装成成品
 * @version 1.0
 * @author AYOU
 * @date 2016年6月8日 上午9:55:53
 * 
 * 用Director构建最后的复杂对象,
 * 而在上面Builder接口中封装的是如何创建一个个部件(复杂对象是由这些部件组成的),
 * 也就是说Director的内容是如何将部件最后组装成成品:
 */
public class Director {
	private Builder builder;

	public Director(Builder builder) {
		this.builder = builder;
	}

	// 将部件partA partB partC最后组成复杂对象
	// 这里是将车轮 方向盘和发动机组装成汽车的过程
	public void construct() {
		builder.buildPartA();
		builder.buildPartB();
		builder.buildPartC();
	}
}
