package com.ayou.site.base.study.java8;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.function.Predicate;

/**
 * @category Lambda
 * @author AYOU
 * @version 1.0
 * @since JDK 1.8
 * @date 2016年12月7日 上午9:22:34
 * 
 *       Lambda表达式优先用于定义功能接口在行内的实现，即单个方法只有一个接口。
 *       例子中，用了多个类型的Lambda表达式来定义MathOperation接口的操作方法。然后我们定义了GreetingService的sayMessage的实现。
 *       Lambda表达式让匿名类不再需要，这为Java增添了简洁但实用的函数式编程能力。
 */
public class Lambda {
	public static void main(String args[]) {
		Lambda tester = new Lambda();

		// 带有类型声明的表达式
		MathOperation addition = (int a, int b) -> a + b;

		// 没有类型声明的表达式
		MathOperation subtraction = (a, b) -> a - b;

		// 带有大括号、带有返回语句的表达式
		MathOperation multiplication = (int a, int b) -> {
			return a * b;
		};

		// 没有大括号和return语句的表达式
		MathOperation division = (int a, int b) -> {
			return (a / b);
		};

		int a = tester.operate(100, 2, addition);
		int s = tester.operate(100, 2, subtraction);
		int m = tester.operate(100, 2, multiplication);
		int d = tester.operate(100, 2, division);

		// 输出结果
		System.out.println("10 + 5 = " + a);
		System.out.println("10 - 5 = " + s);
		System.out.println("10 x 5 = " + m);
		System.out.println("10 / 5 = " + d);

		// 没有括号的表达式
		GreetingService greetService1 = message -> System.out.println("Hello " + message);

		// 有括号的表达式
		GreetingService greetService2 = (message) -> System.out.println("Hello " + message);

		// 调用sayMessage方法输出结果
		greetService1.sayMessage("Shiyanlou");
		greetService2.sayMessage("Classmate");
	}

	// 下面是定义的一些接口和方法

	interface MathOperation {
		int operation(int a, int b);
	}

	interface GreetingService {
		void sayMessage(String message);
	}

	private int operate(int a, int b, MathOperation mathOperation) {
		return mathOperation.operation(a, b);
	}
}

/**
 * 
 * 通过使用Lambda表达式，你可以引用final变量或者有效的final变量（只赋值一次） 如果一个变量被再次赋值，Lambda表达式将抛出一个编译错误。
 */
class final_referrance {
	final static String salutation = "Hello ";

	public static void main(String args[]) {
		GreetingService greetService1 = message -> System.out.println(salutation + message);
		GreetingService greetService2 = message -> System.out.println(salutation + message);
		greetService1.sayMessage("Shiyou");
		greetService2.sayMessage("哈哈");
	}

	interface GreetingService {
		void sayMessage(String message);
	}
}

/**
 * 方法引用 http://docs.oracle.com/javase/tutorial/java/javaOO/methodreferences.html
 *
 */
class method_referrance {
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static void main(String args[]) {
		List names = new ArrayList();

		names.add("Peter");
		names.add("Linda");
		names.add("Smith");
		names.add("Zack");
		names.add("Bob");

		// 通过System.out::println引用了输出的方法
		names.forEach(System.out::println);

		// 引用string的方法实现数组排序
		String[] stringArray = { "Barbara", "James", "Masry", "John", "Patricia", "Robert", "Michael", "Linda" };
		Arrays.sort(stringArray, String::compareToIgnoreCase);
		System.out.println(Arrays.toString(stringArray));
	}
}

/**
 * 默认方法
 */
class Person {

	public enum Sex {
		MALE, FEMALE
	}

	String name;
	Calendar birthday;
	//LocalDate birthday;
	Sex gender;
	String emailAddress;

	public int getAge() {
		return 1;
	}

	public Calendar getBirthday() {
		return birthday;
	}

	public static int compareByAge(Person a, Person b) {
		return a.birthday.compareTo(b.birthday);
	}
	
	public static void main(String[] args) {
		//1
		/*Person[] rosterAsArray = roster.toArray(new Person[roster.size()]);

		class PersonAgeComparator implements Comparator<Person> {
		    public int compare(Person a, Person b) {
		        return a.getBirthday().compareTo(b.getBirthday());
		    }
		}
		        
		Arrays.sort(rosterAsArray, new PersonAgeComparator());*/
	}
}

/**
 * 函数式接口
 *
 * 下面是部分函数式接口的列表。
	BitConsumer<T,U>：该接口代表了接收两个输入参数T、U，并且没有返回的操作。
	BiFunction<T,U,R>：该接口代表提供接收两个参数T、U，并且产生一个结果R的方法。
	BinaryOperator<T>：代表了基于两个相同类型的操作数，产生仍然是相同类型结果的操作。
	BiPredicate<T,U>：代表了对两个参数的断言操作（基于Boolean值的方法）。
	BooleanSupplier：代表了一个给出Boolean值结果的方法。
	Consumer<T>：代表了接受单一输入参数并且没有返回值的操作。
	DoubleBinaryOperator：代表了基于两个Double类型操作数的操作，并且返回一个Double类型的返回值。
	DoubleConsumer：代表了一个接受单个Double类型的参数并且没有返回的操作。
	DoubleFunction<R>：代表了一个接受Double类型参数并且返回结果的方法。
	DoublePredicate：代表了对一个Double类型的参数的断言操作。
	DoubleSupplier：代表了一个给出Double类型值的方法。
	DoubleToIntFunction：代表了接受单个Double类型参数但返回Int类型结果的方法。
	DoubleToLongFunction：代表了接受单个Double类型参数但返回Long类型结果的方法。
	DoubleUnaryOperator：代表了基于单个Double类型操作数且产生Double类型结果的操作。
	Function<T,R>：代表了接受一个参数并且产生一个结果的方法。
	IntBinaryOperator：代表了对两个Int类型操作数的操作，并且产生一个Int类型的结果。
	IntConsumer：代表了接受单个Int类型参数的操作，没有返回结果。
	IntFunction<R>：代表了接受Int类型参数并且给出返回值的方法。
	IntPredicate：代表了对单个Int类型参数的断言操作。
	
	更多参考 https://docs.oracle.com/javase/8/docs/api/java/lang/FunctionalInterface.html
	在实际使用过程中，加有@FunctionalInterface注解的方法均是此类接口，位于java.util.Funtion包中。
 */ 
class _Interface {
   public static void main(String args[]){
      List<Integer> list = Arrays.asList(0, 1, 2, 3, 4, 5, 6, 7, 8, 9);

      System.out.println("All of the numbers:");

      eval(list, n->true);

      System.out.println("Even numbers:");
      eval(list, n-> n%2 == 0 );

      System.out.println("Numbers that greater than  5:");
      eval(list, n -> n > 5 );
   }

   public static void eval(List<Integer> list, Predicate<Integer> predicate) {
      for(Integer n: list) {
         if(predicate.test(n)) {
            System.out.println(n + " ");
         }
      }
   }
}
