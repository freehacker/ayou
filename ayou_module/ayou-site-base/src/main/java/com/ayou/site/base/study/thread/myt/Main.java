package com.ayou.site.base.study.thread.myt;

/**
 * @category Main.java
 * @author AYOU
 * @version 2016年5月26日 上午9:05:37
 */
public class Main {
	public static void main(String[] args) {
		Holder h = new Holder();
		
		Add add = new Add(h);
		Reduce rd = new Reduce(h);
		
		Add add1 = new Add(h);
		Reduce rd1= new Reduce(h);
		
		Add add2 = new Add(h);
		Reduce rd2= new Reduce(h);
		
		Add add3 = new Add(h);
		Reduce rd3= new Reduce(h);
		
		Add add4 = new Add(h);
		Reduce rd4= new Reduce(h);
		
		Add add5 = new Add(h);
		Reduce rd5= new Reduce(h);
		
		add.setName("add");
		add.start();
		rd.setName("rd");
		rd.start();
		
		add1.setName("add1");
		add1.start();
		rd1.setName("rd1");
		rd1.start();
		
		add2.setName("add2");
		rd2.setName("rd2");
		add2.start();
		rd2.start();
		
		add3.setName("add3");
		rd3.setName("rd3");
		add3.start();
		rd3.start();
		
		add4.setName("add4");
		rd4.setName("rd4");
		add4.start();
		rd4.start();
		
		add5.setName("add5");
		rd5.setName("rd5");
		add5.setPriority(10);
		rd5.setPriority(10);
		add5.start();
		rd5.start();
	}
}
