package com.ayou.site.base.study.thread.book.c1;

/**
 * @category Main
 * @author AYOU
 * @version 1.0
 * @since JDK 1.8
 * @date 2017年9月8日 上午9:33:38
 */
public class Main {
	public static void main(String[] args) {
		Thread t1 = new MyThread();
		t1.setName("第一个线程");
		Thread t2 = new MyThread();
		t2.setName("第二个线程");
		t1.start();
		t2.start();
	}
}
