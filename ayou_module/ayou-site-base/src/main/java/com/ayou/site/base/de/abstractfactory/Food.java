package com.ayou.site.base.de.abstractfactory;

/**
 * @category 抽象食物
 * @author AYOU
 * @version 2016年5月23日 下午4:28:51
 */
public interface Food {
	public String getFoodName();
}
