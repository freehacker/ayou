package com.ayou.site.base.de.observer.v3;

import java.util.ArrayList;
import java.util.List;

/**
 * @category FB
 * @author   AYOU
 * @version  1.0
 * @since    JDK 1.8
 * @date    2017年9月4日 上午11:01:34
 */
public class FB implements Subject{
	List<Observer> observers = new ArrayList<Observer>();
	@Override
	public void regiser(Observer observer) {
		observers.add(observer);
	}

	@Override
	public void notify(String msg) {
		for (Observer observer : observers) {
			observer.getMsg(msg);
		}
	}

}

