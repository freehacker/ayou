package com.ayou.site.base.study.thread;

/**
 * @category 测试类
 * @author AYOU
 * @version 2016年5月25日 下午5:05:33
 */
public class NumberTest {
	public static void main(String[] args) {
		NumberHolder numberHolder = new NumberHolder();

		Thread t1 = new IncreaseThread(numberHolder);
		Thread t2 = new DecreaseThread(numberHolder);

		Thread t3 = new IncreaseThread(numberHolder);
		Thread t4 = new DecreaseThread(numberHolder);

		Thread t5 = new IncreaseThread(numberHolder);
		Thread t6 = new DecreaseThread(numberHolder);

		t1.start();
		t2.start();

		t3.start();
		t4.start();

		t5.start();
		t6.start();

		// 3个加的3个减的线程
	}

}
