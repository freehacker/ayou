package com.ayou.site.base.de.responsibilitychain;

/**
 * @category SpellCheckerProcessing
 * @author AYOU
 * @version 1.0
 * @since JDK 1.8
 * @date 2017年9月1日 下午4:06:57
 */
public class SpellCheckerProcessing extends ProcessingObject<String> {

	@Override
	protected String handleWork(String input) {
		return input.replace("labda", "lambda");
	}

}
