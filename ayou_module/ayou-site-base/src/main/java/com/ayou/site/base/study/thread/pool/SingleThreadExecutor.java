package com.ayou.site.base.study.thread.pool;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @category 单线程化的线程池
 * @author AYOU
 * @version 1.0
 * @since JDK 1.8
 * @date 2017年1月13日 下午3:53:47
 *  它只会用唯一的工作线程来执行任务，保证所有任务按照指定顺序(FIFO, LIFO, 优先级)执行
 */
public class SingleThreadExecutor {
	public void exec() {
		ExecutorService singleThreadExecutor = Executors.newSingleThreadExecutor();
		for (int i = 0; i < 10; i++) {
			final int index = i;
			singleThreadExecutor.execute(new Runnable() {
				@Override
				public void run() {
					try {
						System.out.println(index);
						Thread.sleep(2000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			});
		}
	}

	public static void main(String[] args) {
		SingleThreadExecutor ste = new SingleThreadExecutor();
		ste.exec();
	}
}
