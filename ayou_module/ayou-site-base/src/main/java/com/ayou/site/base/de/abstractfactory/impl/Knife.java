package com.ayou.site.base.de.abstractfactory.impl;

import com.ayou.site.base.de.abstractfactory.TableWare;

/**
 * @category Knife.java
 * @author AYOU
 * @version 2016年5月23日 下午4:34:43
 */
public class Knife implements TableWare {

	@Override
	public String getToolName() {
		return "一把杀猪刀！";
	}

}
