package com.ayou.site.base.de.observer.impl;

import com.ayou.site.base.de.observer.Observer;
import com.ayou.site.base.de.observer.Subject;

/**
 * @category 实现观察者即看视频的美剧迷们.java
 * @author AYOU
 * @version 2016年6月3日 上午10:16:51
 */
public class VideoFans implements Observer {
	private String name;

	public VideoFans() {
	}

	public VideoFans(String name) {
		this.name = name;
	}

	@Override
	public void update(Subject s) {
		System.out.println(this.name + "-" + this.hashCode() + "：订阅了" + s);
		//System.out.println(s);
	}
}
