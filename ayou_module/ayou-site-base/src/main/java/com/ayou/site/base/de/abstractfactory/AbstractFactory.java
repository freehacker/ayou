package com.ayou.site.base.de.abstractfactory;

/**
 * @category 抽象工厂
 * @author AYOU
 * @version 2016年5月23日 下午4:27:36
 */
public interface AbstractFactory {
	public Food getFood();
	public TableWare getTableWare();
}
