package com.ayou.site.base.study.thread.pool;

import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @category 定长线程池
 * @author AYOU
 * @version 1.0
 * @since JDK 1.8
 * @date 2017年1月13日 下午3:44:04
 *   可控制线程最大并发数，超出的线程会在队列中等待
 */
public class FixedThreadPool {
	public void exec() {
		ExecutorService fixedThreadPool = Executors.newFixedThreadPool(3);
		for (int i = 0; i < 10; i++) {
			final int index = i;
			fixedThreadPool.execute(new Runnable() {
				@Override
				public void run() {
					System.out.println(index);
				}
			});
		}
	}
	
	public <T> void execAndReturn(List<Callable<T>> list) {
		ExecutorService fixedThreadPool = Executors.newFixedThreadPool(3);
		for (int i = 0; i < 10; i++) {
			//final int index = i;
			new Callable<T>() {
				@Override
				public T call() throws Exception {
					return null;
				}
			};
		}
		for (int i = 0; i < 10; i++) {
			final int index = i;
			fixedThreadPool.execute(new Runnable() {
				@Override
				public void run() {
					System.out.println(index);
				}
			});
		}
	}

	public static void main(String[] args) {
		ExecutorService fixedThreadPool = Executors.newFixedThreadPool(30);
		for (int i = 0; i < 130; i++) {
			final int index = i;
			fixedThreadPool.execute(new Runnable() {

				@Override
				public void run() {
					System.out.println(index);
				}
			});
		}
	}
}
