package com.ayou.site.base.de.abstractfactory.impl;

import com.ayou.site.base.de.abstractfactory.Food;

/**
 * @category Apple.java
 * @author AYOU
 * @version 2016年5月23日 下午4:33:42
 */
public class Apple implements Food {

	@Override
	public String getFoodName() {
		return "一个大苹果！";
	}

}
