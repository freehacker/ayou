package com.ayou.site.base.de.adapter.impl;

import com.ayou.site.base.de.adapter.EnPluginInterface;

/**
 * @category 实现英标接口
 * @author AYOU
 * @version 2016年5月23日 下午4:49:05
 */
public class EnPlugin implements EnPluginInterface {

	@Override
	public void chargeWith3Pins() {
		 System.out.println("启用英标接口...");
	}

}
