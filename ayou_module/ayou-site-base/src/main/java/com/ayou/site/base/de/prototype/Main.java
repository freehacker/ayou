package com.ayou.site.base.de.prototype;

/**
 * @category Main.java
 * @version 1.0
 * @author AYOU
 * @date 2016年6月8日 上午9:30:48
 */
public class Main {

	public static void main(String[] args) {
		AbstractSpoon spoon = new SoupSpoon();
		AbstractSpoon spoon2 = (AbstractSpoon) spoon.clone();

		System.out.println(spoon2.hashCode());
		System.out.println(spoon2.spoonName);
		
		System.out.println();
		spoon2.setSpoonName("AYOU 大牛");
		
		System.out.println(spoon2.hashCode());
		System.out.println(spoon2.spoonName);
	}

}
