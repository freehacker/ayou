package com.ayou.site.base.study.java8.function;

/**
 * @category AinterfaceImpl
 * @author AYOU
 * @version 1.0
 * @since JDK 1.8
 * @date 2017年9月1日 下午4:53:00
 */
public class AinterfaceImpl implements Ainterface {

	@Override
	public void speck(String msg) {
		System.out.println(msg);
	}
	
//	@Override
//	public void say(String msg) {
//		System.out.println(msg+" v2");
//	}

}
