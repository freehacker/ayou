package com.ayou.site.base.de.observer.v3;

/**
 * @category Main
 * @author AYOU
 * @version 1.0
 * @since JDK 1.8
 * @date 2017年9月4日 上午11:06:59
 */
public class Main {
	public static void main(String[] args) {
		FB fb = new FB();
		fb.regiser((msg)->{System.out.println(msg);});
		fb.regiser((msg)->{System.out.println(msg);});
		fb.notify("你猜我是谁！");
	}
}
