package com.ayou.site.base.de.builder;

import com.ayou.site.base.de.builder.impl.ConcreteBuilder;

/**
 * @category 何调用Builder模式
 * @version 1.0
 * @author AYOU
 * @date 2016年6月8日 上午10:10:55
 */
public class Main {
	@SuppressWarnings("unused")
	public static void main(String[] args) {
		ConcreteBuilder builder = new ConcreteBuilder();
		Director director = new Director(builder);

		director.construct();
		Product product = builder.getResult();
	}
}
