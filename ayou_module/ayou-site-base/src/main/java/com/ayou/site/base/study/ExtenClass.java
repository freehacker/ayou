package com.ayou.site.base.study;

/**
 * @category s
 * @author AYOU
 * @version 2016年5月25日 上午10:07:51
 */
public class ExtenClass {
	private String name;
	private Integer age;
	private Double deposit;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getAge() {
		return age;
	}
	public void setAge(Integer age) {
		this.age = age;
	}
	public Double getDeposit() {
		return deposit;
	}
	public void setDeposit(Double deposit) {
		this.deposit = deposit;
	}
}
