package com.ayou.site.base.study.rx.s1;

import org.reactivestreams.Subscriber;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

/**
 * @category 接收
 * @author AYOU
 * @version 1.0
 * @since JDK 1.8
 * @date 2017年4月24日 上午10:34:48
 */
public class ObserverTest {

	public void f() {
		@SuppressWarnings("unused")
		Observable<String> sender = Observable.create(new ObservableOnSubscribe<String>() {

			@SuppressWarnings("unused")
			public void call(Subscriber<? super String> subscriber) {
				subscriber.onNext("Hi，Weavey！"); // 发送数据"Hi，Weavey！"
			}

			@Override
			public void subscribe(ObservableEmitter<String> paramObservableEmitter) throws Exception {

			}
		});
	}

	public void r() {
		@SuppressWarnings("unused")
		Observer<String> receiver = new Observer<String>() {

			@Override
			public void onSubscribe(Disposable paramDisposable) {

			}

			@Override
			public void onNext(String paramT) {

			}

			@Override
			public void onError(Throwable paramThrowable) {

			}

			@Override
			public void onComplete() {

			}

		};
	}

}
