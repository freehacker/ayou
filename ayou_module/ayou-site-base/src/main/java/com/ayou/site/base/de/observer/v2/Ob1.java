package com.ayou.site.base.de.observer.v2;

/**
 * @category Ob1
 * @author   AYOU
 * @version  1.0
 * @since    JDK 1.8
 * @date    2017年9月1日 下午2:50:18
 */
public class Ob1 implements Observer{

	@Override
	public void get(String msg) {
		System.out.println(this.getClass().getName()+" get a msg: " + msg);
	}

}

