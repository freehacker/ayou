package com.ayou.site.base.de.observer.v2;

import java.util.LinkedList;
import java.util.List;

/**
 * @category Small
 * @author   AYOU
 * @version  1.0
 * @since    JDK 1.8
 * @date    2017年9月1日 下午2:53:00
 */
public class Small implements Subject{
	private List<Observer> list = new LinkedList<Observer>();
	@Override
	public void register(Observer observer) {
		list.add(observer);
	}

	@Override
	public void send(String msg) {
		for (Observer observer : list) {
			observer.get(msg);
		}
	}

	@Override
	public void remove(Observer observer) {
		list.remove(observer);
	}
}

