package com.ayou.site.base.study.java8.methodreference.c2;

import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * @category Filter2
 * @author   AYOU
 * @version  1.0
 * @since    JDK 1.8
 * @date    2017年8月28日 下午4:06:38
 */
public class Filter2 {
	public static void main(String[] args) {
		Logger logger = Logger.getLogger("Filter2");
//		List<Apple> inventory = Arrays.asList(new Apple("green", 120), new Apple("red", 151), new Apple("green", 152));
//		Apple a = new Apple();
//		Apple b = new Apple();
//		//a.setColor(b::getColor);
//		List<Apple> green = Apple.filterApple2(inventory, Apple::isGreenApple);
//		List<Apple> heavy = Apple.filterApple2(inventory, Apple::isHeavyApple);
//		System.out.println(green);
//		System.out.println(heavy);
//		
//		List<Apple> green2 = Apple.filterApple2(inventory, (Apple a) -> "green".equals(a.getColor()));
//		List<Apple> heavy2 = Apple.filterApple2(inventory, (Apple a) -> a.getWeight() > 150);
//		List<Apple> gh2 = Apple.filterApple2(inventory, (Apple a) -> a.getWeight() > 150 && "green".equals(a.getColor()));
//		
//		System.out.println(green2);
//		System.out.println(heavy2);
//		System.out.println(gh2);
		logger.log(Level.INFO, () -> "日志");
	}
}

