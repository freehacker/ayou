package com.ayou.site.base.study;

/**
 * @category 自动装箱拆箱
 * @author AYOU
 * @version 1.0
 * @since JDK 1.8
 * @date 2017年1月11日 下午3:58:02
 * 
 * 原始类型转引用类型 装箱
 * 引用类型转原始类型 拆箱
 */
public class 自动装箱拆箱 {
	public static void main(String[] args) {
		Integer a = 1;
		Integer b = 2;
		Integer c = 3;
		Integer d = 3;
		Integer e = 321;//-128  127
		Integer f = 321;
		Long g = 3L;

		System.out.println(c == d);
		System.out.println(e == f);
		System.out.println(e.equals(f)); 
		System.out.println(c == (a + b));
		System.out.println(c.equals(a + b));
		System.out.println(g == (a + b));
		System.out.println(g.equals(a + b));
		
		Integer x = 10;
		Integer y = 10;
		for (;;) {
			x++;
			y++;
			boolean flag = (x == y);
			if (!flag) {
				System.out.println(x == y);
				System.out.println(x + "@" + y);
				break;
			}
		}
	}
}
