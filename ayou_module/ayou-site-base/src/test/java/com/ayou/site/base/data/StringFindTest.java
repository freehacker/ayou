package com.ayou.site.base.data;

import com.ayou.site.base.study.algorithm.string.Find;

/**
 * @category 测试字符串查找
 * @author   AYOU
 * @version  1.0
 * @since    JDK 1.8
 * @date     2016年11月22日 下午4:54:59
 */
public class StringFindTest {
	public static void main(String[] args) {
		System.out.println(Find.findStr("123456jhgfshgda", "hg"));
	}
}

