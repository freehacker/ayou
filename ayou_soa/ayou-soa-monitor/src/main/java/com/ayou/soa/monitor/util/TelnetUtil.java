package com.ayou.soa.monitor.util;

/**
 * @author AYOU
 * @version 2016年5月10日 下午1:29:12
*/
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedTransferQueue;

import org.apache.commons.net.telnet.EchoOptionHandler;
import org.apache.commons.net.telnet.SuppressGAOptionHandler;
import org.apache.commons.net.telnet.TelnetClient;
import org.apache.commons.net.telnet.TelnetNotificationHandler;
import org.apache.commons.net.telnet.TerminalTypeOptionHandler;

import com.ayou.core.commons.exception.ProjectException;;

/**
 * telnet工具类
 * 
 * @author liuyu
 *
 */
public class TelnetUtil implements Runnable, TelnetNotificationHandler {

	// public static void main(String[] args) throws IOException {
	// TelnetUtil tu = new TelnetUtil("192.168.77.129", 23, "root", "0000aaaa");
	// System.out.println("start");
	// tu.execute(
	// "nohup java -jar /usr/local/test/identity/identity.jar > /dev/null 2>&1
	// &",
	// "Last login");
	// }

	private final TelnetClient tc;

	/**
	 * 命令队列[命令,开始执行命令的标识符]
	 */
	private BlockingQueue<String[]> cmdQueue = new LinkedTransferQueue<String[]>();

	/**
	 * 构造方法
	 *
	 * @param ip
	 * @param port
	 * @param usr
	 * @param pwd
	 */
	public TelnetUtil(String ip, int port, String usr, String pwd) {
		tc = new TelnetClient();
		TerminalTypeOptionHandler ttopt = new TerminalTypeOptionHandler("VT100", false, false, true, false);
		EchoOptionHandler echoopt = new EchoOptionHandler(true, false, true, false);
		SuppressGAOptionHandler gaopt = new SuppressGAOptionHandler(true, true, true, true);

		try {
			tc.addOptionHandler(ttopt);
			tc.addOptionHandler(echoopt);
			tc.addOptionHandler(gaopt);
			tc.connect(ip, port);
		} catch (Exception e) {
			System.err.println("Error registering option handlers: " + e.getMessage());
		}
		tc.registerNotifHandler(this);
		Thread reader = new Thread(this);
		reader.start();
		execute(usr, "login:");
		execute(pwd, "Password:");
	}

	/**
	 * 执行一条命令。 命令不会立即执行，而是等待服务器返回信息中包含标识符flag时才开始执行
	 *
	 * @param cmd
	 *            命令语句，不用加换行符
	 * @param flag
	 *            可以执行命令的标识符
	 * @throws IOException
	 */
	public synchronized void execute(String cmd, String flag) {
		cmd = cmd + "\n";
		cmdQueue.add(new String[] { cmd, flag });
	}

	@Override
	/**
	 * run方法。 获取服务器返回的信息，并检返回信息是否表示命令行队首的命令可以执行。 若检测到可执行，则取出队首命令并执行。
	 */
	public void run() {
		InputStream in = tc.getInputStream();
		OutputStream out = tc.getOutputStream();
		String[] strs;
		try {
			strs = cmdQueue.take();
		} catch (InterruptedException e1) {
			throw new ProjectException(e1);
		}
		while (true) {
			try {
				byte[] buff = new byte[1024];
				int ret_read = 0;

				ret_read = in.read(buff);
				if (ret_read > 0) {
					String res = new String(buff, 0, ret_read);
					System.out.print(res);
					if (res.indexOf(strs[1]) >= 0) {
						String cmd = strs[0];
						System.out.print(cmd);
						out.write(cmd.getBytes());
						out.flush();
						try {
							strs = cmdQueue.take();
						} catch (InterruptedException e1) {
							throw new ProjectException(e1);
						}
					}
				}
			} catch (Exception e) {
				System.err.println("Exception while reading socket:" + e.getMessage());
			}
		}
	}

	@Override
	public void receivedNegotiation(int negotiation_code, int option_code) {
		String command = null;
		if (negotiation_code == TelnetNotificationHandler.RECEIVED_DO) {
			command = "DO";
		} else if (negotiation_code == TelnetNotificationHandler.RECEIVED_DONT) {
			command = "DONT";
		} else if (negotiation_code == TelnetNotificationHandler.RECEIVED_WILL) {
			command = "WILL";
		} else if (negotiation_code == TelnetNotificationHandler.RECEIVED_WONT) {
			command = "WONT";
		}
		System.out.println("Received " + command + " for option code " + option_code);

	}
}
