package com.ayou.soa.monitor;

import com.ayou.soa.monitor.util.TelnetUtil;

/**
 * @author AYOU
 * @version 2016年5月10日 下午1:28:33
 * 
 *  * 远程服务提供者。 实际上是通过telnet命令远程操作某个提供者jar TODO
 * 由于java程序停止后，windows下的服务会随之停止，故暂只支持linux
 * 
 * @author liuyu
*/
public class RemoteProvider {

	private String ip;
	private String usr;
	private String pwd;
	private String jarPath;

	/**
	 * 构造方法
	 * 
	 * @param ip
	 * @param usr
	 * @param pwd
	 * @param jarPath
	 */
	public RemoteProvider(String ip, String usr, String pwd, String jarPath) {
		this.ip = ip;
		this.usr = usr;
		this.pwd = pwd;
		this.jarPath = jarPath;
	}

	public void start() {
		TelnetUtil telnetOperator = new TelnetUtil(ip, 23, usr, pwd);
		String command = "nohup java -jar " + jarPath + " > /dev/null 2>&1 &";
		telnetOperator.execute(command, "Last login");
	}

	public void stop() {
		// TODO 修改真正的提供者在zookeeper上放置的节点，触发提供者上的关闭事件
	}

}
