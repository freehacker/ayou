package com.ayou.soa.base.consumer;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 自定义soa服务注解，用以注入服务。
 **/
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface SoaResource {
	/**
	 * 服务id。若此参数为空，则以变量类名的首字母小写作为服务id
	 **/
	String id() default "";
}
