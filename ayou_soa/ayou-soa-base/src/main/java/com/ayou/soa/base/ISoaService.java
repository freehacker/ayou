package com.ayou.soa.base;

/**
 * soa服务接口
 * **/
public interface ISoaService {
	
	/**
	 * 获得服务中引用到的className
	 * **/
	public String[] getRefClassNames();
	
}
