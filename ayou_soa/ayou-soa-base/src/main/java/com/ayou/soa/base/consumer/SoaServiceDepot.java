package com.ayou.soa.base.consumer;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.util.ReflectionUtils;

import com.ayou.core.commons.exception.ProjectException;

/**
 * soa服务仓库，存放了所有可用的服务实例，以便按需选择所需服务。
 * 
 */
public class SoaServiceDepot {
	private static Logger logger = LoggerFactory.getLogger(SoaServiceDepot.class);

	/**
	 * 待注入服务的字段
	 */
	private static List<ServiceField> serviceFieldList = new LinkedList<ServiceField>();

	/**
	 * 存储Service的Map<id,service>
	 */
	private static final Map<String, Object> serviceMap = new HashMap<String, Object>();

	/**
	 * 存储Service的Map<className,service>
	 */
	private static final Map<Class<?>, Object> boServiceMap = new HashMap<>();

	/**
	 * 读写锁，存入service时加写锁，获取service时加读锁
	 */
	private static final ReentrantReadWriteLock rwLock = new ReentrantReadWriteLock();

	/**
	 * spring ctx
	 **/
	private static AbstractApplicationContext oldCxt;

	/**
	 * 添加一个服务实例
	 * 
	 * @param service
	 *            服务
	 * @param id
	 *            服务bean id
	 * @param managedboClasses
	 *            service管理的bo类
	 */
	public static void addService(Object service, String id, Class<?>[] managedboClasses) {
		rwLock.writeLock().lock();
		try {
			addServiceWithoutLock(service, id, managedboClasses);
		} finally {
			rwLock.writeLock().unlock();
		}
	}

	private static void addServiceWithoutLock(Object service, String id, Class<?>[] managedboClasses) {
		logger.info("SoaServiceInjection.addService:" + id);
		serviceMap.put(id, service);
		if (null != managedboClasses) {
			for (Class<?> c : managedboClasses) {
				boServiceMap.put(c, service);
			}
		}
	}

	/**
	 * 添加一个注入任务
	 * 
	 * @param s
	 *            需要注入的字段
	 */
	public static void addInjection(ServiceField s) {
		if (null == oldCxt) {
			// 若spring ctx未初始化完成，将注入任务添加到注入队列，待ctx完成初始化后由观察者触发注入
			serviceFieldList.add(s);
		} else {
			// 若spring ctx已初始化完成，直接从ctx中取服务并注入
			rwLock.writeLock().lock();
			try {
				injectionOne(s, oldCxt);
			} catch (Exception e) {
				throw new RuntimeException(e);
			} finally {
				rwLock.writeLock().unlock();
			}
		}
	}

	/**
	 * 根据服务id，获得一个服务
	 * 
	 * @param id
	 * @return
	 */
	public static Object getServiceByID(String id) {
		rwLock.readLock().lock();
		try {
			return serviceMap.get(id);
		} finally {
			rwLock.readLock().unlock();
		}
	}

	/**
	 * 获取所有服务id及其对应service的list
	 * 
	 * @return <Entry<服务id, 服务>>
	 */
	public static List<Entry<String, Object>> getServiceList() {
		rwLock.readLock().lock();
		try {
			List<Entry<String, Object>> list = new LinkedList<Map.Entry<String, Object>>();
			Iterator<Entry<String, Object>> iterator = serviceMap.entrySet().iterator();
			while (iterator.hasNext()) {
				list.add(iterator.next());
			}
			return list;
		} finally {
			rwLock.readLock().unlock();
		}

	}

	/**
	 * 根据被管理的boclass，获得一个管理此bo的服务
	 * 
	 * @param managedboClass
	 * @return
	 */
	public static Object getServiceByBoClass(Class<?> managedboClass) {
		rwLock.readLock().lock();
		try {
			return boServiceMap.get(managedboClass);
		} finally {
			rwLock.readLock().unlock();
		}
	}

	/**
	 * 获取所有bo及其对应service的迭代器
	 * 
	 * @return <Entry<bo类, 服务>>
	 */
	public static List<Entry<Class<?>, Object>> getBoServiceList() {
		rwLock.readLock().lock();
		try {
			List<Entry<Class<?>, Object>> list = new LinkedList<Map.Entry<Class<?>, Object>>();
			Iterator<Entry<Class<?>, Object>> iterator = boServiceMap.entrySet().iterator();
			while (iterator.hasNext()) {
				list.add(iterator.next());
			}
			return list;
		} finally {
			rwLock.readLock().unlock();
		}

	}

	/**
	 * 注入
	 * 
	 * @param springCtx
	 *            ctx
	 */
	public static void injection(ApplicationContext springCtx) {
		AbstractApplicationContext rctx = (AbstractApplicationContext) springCtx;
		if (!rctx.isRunning()) {
			rctx.start();
		}
		if (null != oldCxt && oldCxt.isRunning()) {
			oldCxt.close();
		}
		oldCxt = rctx;
		rwLock.writeLock().lock();
		try {
			serviceMap.clear();
			for (ServiceField s : serviceFieldList) {
				try {
					injectionOne(s, rctx);
				} catch (Exception e) {
					throw new RuntimeException(e);
				}
			}
		} finally {
			rwLock.writeLock().unlock();
			serviceFieldList.clear();
		}
	}

	private static void injectionOne(ServiceField s, AbstractApplicationContext rctx) throws NoSuchMethodException,
			SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		String serviceId = s.getId();
		Field field = s.getField();
		Object bean = s.getBean();
		Object service;
		service = serviceMap.get(serviceId);
		if (null == service) {
			service = serviceMap.get(serviceId + "Imp");
			if (null == service) {
				service = serviceMap.get(serviceId + "Impl");
			}
			if (null == service) {
				try {
					service = rctx.getBean(serviceId);
				} catch (BeansException e) {
					try {
						service = rctx.getBean(serviceId + "Imp");
					} catch (BeansException e1) {
						try {
							service = rctx.getBean(serviceId + "Impl");
						} catch (BeansException e2) {
							throw new ProjectException("spring环境中不存在service id:" + serviceId + "(Impl)");
						}
					}
				}
			}
			// addService
			Method method = service.getClass().getMethod("getManagedBoClasses", new Class[0]);
			Class<?>[] managedboClasses = (Class[]) method.invoke(service, new Object[0]);
			addServiceWithoutLock(service, serviceId, managedboClasses);
		}
		ReflectionUtils.makeAccessible(field);
		field.set(bean, service);
		logger.info("Service注入完成, id:" + serviceId);
	}
}

/**
 * 服务需要注入的字段
 **/
class ServiceField {
	private String id;

	private Field field;

	private Object bean;

	public String getId() {
		return id;
	}

	public Field getField() {
		return field;
	}

	public Object getBean() {
		return bean;
	}

	public ServiceField(String id, Field field, Object bean) {
		this.id = id;
		this.field = field;
		this.bean = bean;
	}
}