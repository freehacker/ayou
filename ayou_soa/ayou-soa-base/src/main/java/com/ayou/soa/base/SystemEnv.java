package com.ayou.soa.base;


import java.util.LinkedList;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import org.springframework.context.ApplicationContext;

public class SystemEnv {

	/**
	 * SpringCtx的观察者。用以当SpringCtx初始化完成时，向调用者返回SpringCtx
	 *
	 */
	@FunctionalInterface
	public static interface SpringCtxWatcher {
		/**
		 * 当SystemEnv.setSpringCtx被调用时，将新的ctx传给目标
		 *
		 * @param newCtx
		 *            SystemEnv.setSpringCtx传入的参数
		 * **/
		public void notice(ApplicationContext newCtx);
	}

	/**
	 * homePath观察者。用以当homePath被确定时，向调用者返回homePath
	 *
	 */
	@FunctionalInterface
	public static interface HomePathWatcher {
		/**
		 * 当SystemEnv.setHomePath被调用时，将新的homePath传给目标
		 *
		 * @param homePath
		 *            新的homePath
		 * **/
		public void notice(String homePath);
	}

	private static SystemEnv systemEnv = new SystemEnv();

	/**
	 * 当调用getSpringCxt方法时，spring环境可能还未初始化完成，此时利用此Waiter将getSpringCxt方法挂起，直到初始化完毕
	 */
	private final Object springCxtWaiter = new byte[0];

	private LinkedList<SpringCtxWatcher> ctxWatcherList = new LinkedList<SpringCtxWatcher>();

	private ReentrantReadWriteLock ctxWatcherLock = new ReentrantReadWriteLock();

	private LinkedList<HomePathWatcher> homePathWatcherList = new LinkedList<HomePathWatcher>();

	private ReentrantReadWriteLock homePathWatcherLock = new ReentrantReadWriteLock();

	/**
	 * 添加一个HomePath观察者，当HomePath被确定时，通知此观察者
	 *
	 * @param watcher
	 */
	public void addHomePathWatcher(HomePathWatcher watcher) {
		homePathWatcherLock.writeLock().lock();
		homePathWatcherList.add(watcher);
		homePathWatcherLock.writeLock().unlock();
	}

	private String superPwd;

	private boolean initConvertorCache = false;

	private String reportPath;

	private String cmsTemplatePath;// cms数据同步模版路径

	private String contextPath = "/";

	private String homePath = "";

	private String tempPath = "";

	private String dateFormat = "yyyy-MM-dd";

	private String timeFormat = "yyyy-MM-dd HH:mm:ss";

	private String encoding = "utf-8";

	private Locale locale = null;

	private ApplicationContext springCtx = null;

	private SystemEnv() {
	}

	public static SystemEnv getInstance() {
		return systemEnv;
	}

	public void destroyed() {
	}

	public String getSuperPwd() {
		return superPwd;
	}

	public void setSuperPwd(String superPwd) {
		this.superPwd = superPwd;
	}

	public boolean isInitConvertorCache() {
		return initConvertorCache;
	}

	public void setInitConvertorCache(boolean initConvertorCache) {
		this.initConvertorCache = initConvertorCache;
	}

	public String getDateFormat() {
		return dateFormat;
	}

	public void setDateFormat(String dateFormat) {
		this.dateFormat = dateFormat;
	}

	public String getContextPath() {
		return contextPath;
	}

	public void setContextPath(String contextPath) {
		this.contextPath = contextPath;
	}

	public String getHomePath() {
		return homePath;
	}

	public void setHomePath(String homePath) {
		this.homePath = homePath;
		homePathWatcherLock.readLock().lock();
		for(HomePathWatcher watcher : homePathWatcherList) {
			watcher.notice(homePath);
		}
		homePathWatcherLock.readLock().unlock();
	}

	public String getEncoding() {
		return encoding;
	}

	public void setEncoding(String encoding) {
		this.encoding = encoding;
	}

	public ApplicationContext getSpringCtx() {
		// 若ctx未初始化完毕，将线程挂起
		if(null == springCtx) {
			synchronized(springCxtWaiter) {
				try {
					springCxtWaiter.wait();
				} catch(Exception e) {
				}
			}
		}
		return springCtx;
	}

	public void setSpringCtx(ApplicationContext springCtx) {
		if(null != springCtx) {
			ctxWatcherLock.readLock().lock();
			for(SpringCtxWatcher watcher : ctxWatcherList) {
				watcher.notice(springCtx);
			}
			ctxWatcherLock.readLock().unlock();
		}
		this.springCtx = springCtx;
		// 若ctx初始化完毕，唤醒挂起的getSpringCtx线程
		if(null != springCtx) {
			synchronized(springCxtWaiter) {
				try {
					springCxtWaiter.notifyAll();
				} catch(Exception e) {
				}
			}
		}

	}

	public Locale getLocale() {
		return locale;
	}

	public void setLocale(Locale locale) {
		this.locale = locale;
	}

	public String getTimeFormat() {
		return timeFormat;
	}

	public void setTimeFormat(String timeFormat) {
		this.timeFormat = timeFormat;
	}

	public String getTempPath() {
		return tempPath;
	}

	public void setTempPath(String tempPath) {
		this.tempPath = tempPath;
	}

	/**
	 * 放置一个观察者，监控SystemEnv.setSpringCtx方法的调用
	 * **/
	public void addCtxWatcher(SpringCtxWatcher watcher) {
		if(null != springCtx) {
			// 若之前已设置过ctx，将ctx推送给watcher
			watcher.notice(springCtx);
		} else {
			ctxWatcherLock.writeLock().lock();
			try {
				ctxWatcherList.add(watcher);
			} finally {
				ctxWatcherLock.writeLock().unlock();
			}
		}
	}

	@SuppressWarnings("unchecked")
	public static <T> T getBean(String name) {
		return (T) SystemEnv.getInstance().getSpringCtx().getBean(name);
	}

	public static <T> Map<String, T> getBeansOfType(Class<T> clazz) {
		return SystemEnv.getInstance().getSpringCtx().getBeansOfType(clazz);
	}

	public String getReportPath() {
		return reportPath;
	}

	public void setReportPath(String reportPath) {
		this.reportPath = reportPath;
	}

	public String getCmsTemplatePath() {
		return cmsTemplatePath;
	}

	public void setCmsTemplatePath(String cmsTemplatePath) {
		this.cmsTemplatePath = cmsTemplatePath;
	}
}
