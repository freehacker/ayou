package com.ayou.soa.base.provider;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.alibaba.dubbo.config.ProtocolConfig;
import com.ayou.core.commons.exception.ServiceException;
import com.ayou.soa.base.SystemEnv;

/**
 * 服务提供者
 **/
public class Provider {
	protected final static Logger logger = LoggerFactory.getLogger(String.class);
	/**
	 * 存储context的hashmap，防止context重复生成和start
	 **/
	private static HashMap<String, ClassPathXmlApplicationContext> contextMap = new HashMap<String, ClassPathXmlApplicationContext>();

	/**
	 * 启动服务
	 * 
	 * @param cfgPath
	 *            配置文件路径
	 **/
	public synchronized static void start(String cfgPath) {
		logger.info("启动dubbo中的服务...");
		ClassPathXmlApplicationContext context = contextMap.get(cfgPath);
		if (null == context) {
			try {
				context = new ClassPathXmlApplicationContext(new String[] { cfgPath });
				try {
					context.getBean("systemEnv");// 主动get一下systemEnv，使其依据配置的参数进行初始化
				} catch (Exception e) {
					e.printStackTrace();
				}
				context.start();
				contextMap.put(cfgPath, context);
				SystemEnv.getInstance().setSpringCtx(context);
			} catch (BeansException e) {
				e.printStackTrace();
				throw new ServiceException("错误的provider配置信息:" + cfgPath, e);
			}
		}

	}

	/**
	 * 关闭所有服务
	 **/
	public synchronized static void destroyAll() {
		ProtocolConfig.destroyAll();
		Iterator<Entry<String, ClassPathXmlApplicationContext>> iterator = contextMap.entrySet().iterator();
		while (iterator.hasNext()) {
			iterator.next().getValue().close();
		}
	}

}
