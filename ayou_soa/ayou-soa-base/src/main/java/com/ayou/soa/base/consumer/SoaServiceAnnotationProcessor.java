package com.ayou.soa.base.consumer;

import java.lang.reflect.Field;
//import java.lang.reflect.Modifier;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.InstantiationAwareBeanPostProcessorAdapter;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.util.ReflectionUtils;

import com.ayou.soa.base.SystemEnv;
import com.ayou.soa.base.SystemEnv.SpringCtxWatcher;

/**
 * 注解@SoaService的处理器
 **/
@Component
public class SoaServiceAnnotationProcessor extends InstantiationAwareBeanPostProcessorAdapter {

	static {
		SpringCtxWatcher watcher = new SpringCtxWatcher() {
			@Override
			public void notice(ApplicationContext newCtx) {
				SoaServiceDepot.injection(newCtx);
			}
		};
		SystemEnv.getInstance().addCtxWatcher(watcher);
	}

	@Override
	public boolean postProcessAfterInstantiation(final Object bean, String beanName) throws BeansException {
		ReflectionUtils.doWithFields(bean.getClass(), new ReflectionUtils.FieldCallback() {
			@Override
			public void doWith(Field field) throws IllegalArgumentException, IllegalAccessException {
				SoaResource annotation = field.getAnnotation(SoaResource.class);
				if (annotation != null) {
					// if (Modifier.isStatic(field.getModifiers())) {
					// throw new IllegalStateException("@SoaService annotation
					// is not supported on static fields");
					// }
					String serviceId = annotation.id().length() <= 0 ? getDefaultId(field) : annotation.id();// 得到服务id
					/*
					 * 由于dubbo与action的spring环境共用，导致context还未set到SystemEnv中。
					 * 故先将要注入的service保存起来，待SystemEnv set context后通过观察者来注入服务
					 */
					SoaServiceDepot.addInjection(new ServiceField(serviceId, field, bean));
				}
			}
		});

		return true;
	}

	/**
	 * 当SoaService注解的参数为空时，通过此方法得到默认id:类名的首字母小写
	 **/
	private String getDefaultId(Field field) {
		String name = field.getType().getSimpleName();
		String c = name.substring(0, 1);
		String id = name.replaceFirst(c, c.toLowerCase());
		return id;
	}

}
