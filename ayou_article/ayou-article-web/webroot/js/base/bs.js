define([ 'ayou', 'jquery', 'init', 'bootstrap'], function(ayou, $) {
	var URL = {
		view : '/article/view/',
		list : '',
		add : '',
		del : '',
		edit : '',
	};

	var view = function(uuid, success) {
		var config = {
			url : URL.view + uuid,
			type : 'get',
			success : success
		};
		ayou.ajax(config);
	}

	return {
		view : view,
	}

})