require([ "jquery", "avalon", "viode", "layer", "init", "domReady!" ], function($, avalon, videojs, markdown) {
	var uuid = getParam("uuid");
	var modelVM = avalon.define({
		$id : "model",
		model : {},
		tag : 0,
		like:function(uuid){
			if(modelVM.tag == 1){
				layer.msg('<span style="color:red">您已经赞过了~</span>', {icon : 6});
				return;
			}
			$.post("/viode/like/" + uuid,"",function(data){
				if(data.success && data.result==1){
					modelVM.tag = 1;
					modelVM.model.like+=1;
					layer.msg('<span style="color:red">点赞成功，非常感谢您的金手指~</span>', {icon : 6});
				}
			});
		},
		down:function(uuid){
			$.post("/viode/down/"+uuid,"",function(data){
			});
		},
		background:function(){
			console.log(this);
		}
	});

	avalon.scan();

	(function() {
		var viode = document.getElementById("viode-play");
		setTimeout(function(){
			$.get("/viode/view/"+uuid,"",function(data){
				if (data.success && data.resultMsg == 'ok') {
					modelVM.model = data.result;
					if(!data.result.url){
						layer.msg('<span style="color:red">对不起，此视频为收费视频！~</span>', {icon : 6});
					}
					viode.src = modelVM.model.url;
					viode.load();
				}
			});
		},1000);
	})();
});