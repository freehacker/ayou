require(["jquery", "avalon", "init", "bootstrap", "domReady!"], function($, avalon, markdown) {
	var listVM = avalon.define({
		$id : "listVM",
		tbody : [],
		show:function(uuid){
			window.open("/viodes/play.html?uuid=" + uuid);
		},
		like:function(){
			$.post("/viode/like/c787f697f36a4a8691b2f94dd53ec386","",function(data){
				console.log(data);
			});
		}
	});

	avalon.scan();

	(function() {
		$.post("/viode/list","{status:'1'}",function(data){
			if (data.success && data.result) {
				listVM.tbody = data.result;
			}
		});
	})();
});