require([ "jquery", "avalon", "markdown", "bootstrap", "highlight", "domReady!" ], function($, avalon) {
	var uuid = getParam("uuid");
	var modelVM = avalon.define({
		$id : "model",
		showResult : false,
		tip : '',
		tipShow : false,
		delBtnShow : false,
		ediBtnShow : false,
		model : {content:''},
		user : {},
		contentPreview:'',
		del : function(id) {
			$.ajax({
				type : "post",
				async : true,
				cache : true,
				dataType : "json",
				url : "/article/del",
				data : {'id' : id},
				success : function(data) {
					console.log(data);
					if (data.success == true) {
						$('#sTip').modal('show');
						$('#sTipPre').on("click", function() {
							window.location = "/article/index.html";
						});
					} else {
						modelVM.tip = data.msg;
						modelVM.tipShow = true;
					}
				},
				error : function(data) {
					console.log(data);
					modelVM.tip = data.msg;
					modelVM.tipShow = true;
				}
			});
		},
		preview:function(){
			$.ajax({
				type : "post",
				async : true,
				cache : true,
				dataType : "json",
				url : "/article/preview",
				data : {content:modelVM.model.content},
				success : function(data) {
					console.log(data);
					modelVM.contentPreview=data.result;
					$('pre code').each(function(i, block) {
						hljs.highlightBlock(block);
					});
				},
				error : function(data) {
					console.log(data);
				}
			});
		}
	});

	avalon.scan();

	(function() {
	})();
});