require([ "jquery", "avalon", "init", "bootstrap", "domReady!" ], function($, avalon) {
	var stop = false;
	var type = getParam("type");
	var search = getParam("search");

	var listVM = avalon.define({
		$id : "listVM",
		page : 1,
		l_counts : 15,
		tbody : [],
		show : function(uuid) {
			window.open("/articles/view.html?uuid=" + uuid);
		},
		like : function() {
			$.post("/viode/like/" + "", "", function(data) {
			});
		},
		more : function() {

		}
	});

	avalon.scan();

	(function() {
		$.post("/article/list", {
			type : type,
			l_start : 0,
			l_counts : listVM.l_counts
		}, function(data) {
			if (data.success && data.counts > 0) {
				listVM.tbody = data.result;
				if (listVM.tbody.length == 15) {
					stop = false;
				} else {
					stop = true;
				}
			}
		});
	})();

	/**
	 * 滚动加载
	 */
	$(window).scroll(function() {
		totalheight = parseFloat($(window).height()) + parseFloat($(window).scrollTop());
		if ($(document).height() <= totalheight + 300) {
			if (!stop) {
				stop = true;
				$.post("/article/list", {
					l_start : listVM.page * listVM.l_counts,
					l_counts : listVM.l_counts,
					type : type
				}, function(data) {
					if (data.success && data.counts > 0) {
						for (var i = 0; i < data.result.length; i++) {
							listVM.tbody.push(data.result[i]);
						}
						if (data.result.length == 15) {
							stop = false;
							listVM.page += 1;
						}
					}
				});
			}
		}
	});
});