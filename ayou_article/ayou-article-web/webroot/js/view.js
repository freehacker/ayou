require([ "jquery", "avalon", "bootstrap", "highlight", "domReady!" ], function($, avalon) {
	var uuid = getParam("uuid");
	var modelVM = avalon.define({
		$id : "model",
		showResult : false,
		tip : '',
		tipShow : false,
		delBtnShow : false,
		ediBtnShow : false,
		model : {},
		user : {},
		del : function(id) {
			$.ajax({
				type : "post",// post方式优先
				async : true,// 异步优先
				cache : true,// 缓存优先
				dataType : "json",// json格式优先
				url : "/lazy/article/del",
				data : {
					'id' : id
				},
				success : function(data) {
					console.log(data);
					if (data.success == true) {
						$('#sTip').modal('show');
						$('#sTipPre').on("click", function() {
							window.location = "/article/index.html";
						});
					} else {
						modelVM.tip = data.msg;
						modelVM.tipShow = true;
					}
				},
				error : function(data) {
					console.log(data);
					modelVM.tip = data.msg;
					modelVM.tipShow = true;
				}
			});
		}
	});

	avalon.scan();

	(function() {
		$.get("/article/view/" + uuid, function(data) {
			modelVM.model = data.result;
			$('pre code').each(function(i, block) {
				hljs.highlightBlock(block);
			});
			if (data.user) {
				modelVM.user = data.user;
			} else if (!data.user) {
				//alert("没有查到用户！");
			}
			//modelVM.model.content = markdown.toHTML(modelVM.model.content);
			modelVM.showResult = true;
			$(document).attr("title", modelVM.model.title + "-齐天都");// 修改title值
		});
	})();
});