package com.ayou.article.action.sso;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.baomidou.kisso.annotation.Action;
import com.baomidou.kisso.annotation.Login;

@Controller
public class TimeoutAction extends BaseAction {

	/**
	 * 跨域登录超时
	 */
	@Login(action = Action.Skip)
	@RequestMapping("/timeout")
	public String timeout() {
		return "timeout";
	}
}