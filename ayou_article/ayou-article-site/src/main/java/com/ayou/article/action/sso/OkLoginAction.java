package com.ayou.article.action.sso;

import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.kisso.AuthToken;
import com.baomidou.kisso.SSOConfig;
import com.baomidou.kisso.SSOHelper;
import com.baomidou.kisso.SSOToken;
import com.baomidou.kisso.annotation.Action;
import com.baomidou.kisso.annotation.Login;
import com.baomidou.kisso.common.SSOProperties;

@RestController
@RequestMapping("/sso")
public class OkLoginAction extends BaseAction {
	@Login(action = Action.Skip)
	@RequestMapping("/sso_oklogin")
	public String oklogin(ModelMap map) {
		String returl = "webbase/d/timeout.html";
		String replyTxt = request.getParameter("replyTxt");
		if (replyTxt != null && !"".equals(replyTxt)) {
			SSOProperties prop = SSOConfig.getSSOProperties();
			AuthToken at = SSOHelper.ok(request, response, replyTxt, prop.get("sso.defined.article_public_key"), prop.get("sso.defined.sso_public_key"));
			if (at != null) {
				returl = "https://www.qtdu.com";
				SSOToken st = new SSOToken();
				st.setUid(at.getUid());
				st.setTime(at.getTime());
				SSOHelper.setSSOCookie(request, response, st, true);
			}
		}
		map.put("returl", returl);
		return renderJsonMsg(map, "ok");
	}
}
