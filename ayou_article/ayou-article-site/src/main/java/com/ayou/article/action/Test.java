package com.ayou.article.action;

/**
 * @category 测试
 * @author AYOU
 * @version 2016年5月20日 下午2:11:04
 */
public class Test extends Base {
	private String name = "dervied";
	public Test() {
		tellName();
		printName();
	}
	public void tellName() {
		System.out.println("Dervied tell name: " + name);
	}
	public void printName() {
		System.out.println("Dervied print name: " + name);
	}

	public static void main(String[] args) {
		new Test();
	}
}

class Base {
	private String name = "base";
	public Base() {
		tellName();
		printName();
	}
	public void tellName() {
		System.out.println("Base tell name: " + name);
	}
	public void printName() {
		System.out.println("Base print name: " + name);
	}
}