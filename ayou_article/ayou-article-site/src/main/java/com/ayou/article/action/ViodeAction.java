package com.ayou.article.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ayou.article.bo.Viode;
import com.baomidou.kisso.SSOHelper;
import com.baomidou.kisso.SSOToken;

@RestController
@RequestMapping(value = "/viode", produces = { "text/json;charset=UTF-8" })
public class ViodeAction extends ViodeBaseAction {
	@RequestMapping(value = "/list", method = RequestMethod.POST)
	public String list(ModelMap map, HttpServletResponse response, HttpServletRequest request, Viode viode) {
		try {
			List<Viode> v = viodeService.list(viode);
			map.put("result", v);
			return renderJsonMsg(map, "ok");
		} catch (Exception e) {
			e.printStackTrace();
			return renderJsonError(map, "异常", e);
		}
	}

	@RequestMapping(value = "/like/{uuid}", method = RequestMethod.POST)
	public String like(ModelMap map, HttpServletResponse response, HttpServletRequest request, @PathVariable("uuid") String uuid) {
		SSOToken token = SSOHelper.getToken(request);
		if (token == null) {
			return renderJsonMsg(map, "no access!");
		}
		try {
			map.put("result", viodeService.like(uuid));
			return renderJsonMsg(map, "ok");
		} catch (Exception e) {
			e.printStackTrace();
			return renderJsonError(map, "异常", e);
		}
	}
}
