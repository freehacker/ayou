package com.ayou.article.action;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.ayou.article.ViodeProvider;
import com.ayou.article.bo.Viode;
import com.ayou.article.util.QiniuUtil;
import com.ayou.module.identity.service.IdentityService;
import com.ayou.site.base.action.SimpleActionSupport;
import com.ayou.user.provider.UserProvider;
import com.baomidou.kisso.SSOHelper;
import com.baomidou.kisso.SSOToken;

/**
 * @category ViodeBaseAction.java
 * @version 1.0
 * @author AYOU
 * @date 2016年6月15日 上午11:25:02
 */
public abstract class ViodeBaseAction extends SimpleActionSupport {
	protected final Logger logger = LoggerFactory.getLogger(getClass());

	@Resource
	protected IdentityService identityService;
	@Resource
	protected UserProvider userServcie;
	@Resource
	protected ViodeProvider viodeService;

	@RequestMapping(value = "/view/{uuid}", method = RequestMethod.GET)
	public String view(ModelMap map, HttpServletResponse response, HttpServletRequest request, @PathVariable("uuid") String uuid) {
		SSOToken token = SSOHelper.getToken(request);
		if (token == null) {
			return renderJsonMsg(map, "no access!");
		}
		try {
			Viode viode = viodeService.view(uuid);
			viode.setUrl(QiniuUtil.getUrl(viode.getUrl(), 3600L));
			map.put("result", viode);
			viodeService.show(uuid); // 浏览次数+1
			return renderJsonMsg(map, "ok");
		} catch (Exception e) {
			e.printStackTrace();
			return renderJsonError(map, "异常", e);
		}
	}

	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public String add(ModelMap map, HttpServletRequest request, Viode viode) {
		SSOToken token = SSOHelper.getToken(request);
		if (token == null) {
			return renderJsonMsg(map, "no access!");
		}
		try {
			Integer id = viodeService.add(viode);
			map.put("success", true);
			map.put("msg", "新增成功！");
			return renderJsonMsg(map, id + "");
		} catch (Exception e) {
			e.printStackTrace();
			map.put("msg", "新增失败！");
			return renderJsonError(map, "异常！", e);
		}
	}
}
