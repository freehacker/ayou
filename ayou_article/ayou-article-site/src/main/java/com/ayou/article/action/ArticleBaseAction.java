package com.ayou.article.action;

import java.io.PrintWriter;
import java.io.StringWriter;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.markdown4j.Markdown4jProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ayou.article.ArticleProvider;
import com.ayou.article.bo.Article;
import com.ayou.module.identity.service.IdentityService;
import com.ayou.site.base.action.SimpleActionSupport;
import com.ayou.user.provider.UserProvider;
import com.baomidou.kisso.SSOHelper;
import com.baomidou.kisso.SSOToken;

/**
 * @category ArticleBaseAction.java
 * @version 1.0
 * @author AYOU
 * @date 2016年6月15日 上午11:25:02
 */
public abstract class ArticleBaseAction extends SimpleActionSupport {
	protected final Logger logger = LoggerFactory.getLogger(getClass());
	
	@Resource
	protected IdentityService identityService;
	@Resource
	protected UserProvider userServcie;
	@Resource
	protected ArticleProvider articleService;
	
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public String add(ModelMap map, HttpServletRequest request, Article article) {
		SSOToken token = SSOHelper.getToken(request);
		if (token == null) {//未登录
			return "-1";
		}
		article.setUser(userServcie.view(token.getUid()));
		try {
			Integer id = articleService.add(article);
			map.put("success", true);
			map.put("msg", "新增成功！");
			return renderJsonMsg(map, id + "");
		} catch (Exception e) {
			e.printStackTrace();
			map.put("msg", "新增失败！");
			return renderJsonError(map, "异常！", e);
		}
	}

	@RequestMapping(value = "/view/{uuid}", method = RequestMethod.GET)
	@ResponseBody
	public String view(ModelMap map, HttpServletResponse response, HttpServletRequest request, @PathVariable("uuid") String uuid) {
		try {
			Article article = articleService.view(uuid);
			String str = new Markdown4jProcessor().process(article.getContent());
			article.setContent(str);
			map.put("result", article);
			articleService.addShow(uuid); // 浏览次数+1
			return renderJsonMsg(map, "ok");
		} catch (Exception e) {
			// 打印堆栈信息
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			logger.error("downalod exception:" + e.getMessage() + "\t" + errors.toString());
			e.printStackTrace();
			return renderJsonError(map, "异常", e);
		}
	}
	
	
	@RequestMapping(value = "/send/{uuid}", method = RequestMethod.GET)
	public String send(ModelMap map, HttpServletResponse response, HttpServletRequest request, @PathVariable("uuid") String uuid) {
		try {
			Article article = articleService.view(uuid);
			String str = new Markdown4jProcessor().process(article.getContent());
			article.setContent(str);
			map.put("article", article);
			articleService.sendMessage(map);
			// 暂停一下，好让消息消费者去取消息打印出来
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			return "发送成功！";
		} catch (Exception e) {
			// 打印堆栈信息
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			logger.error("downalod exception:" + e.getMessage() + "\t" + errors.toString());
			e.printStackTrace();
			return renderJsonError(map, "异常", e);
		}
	}
}
