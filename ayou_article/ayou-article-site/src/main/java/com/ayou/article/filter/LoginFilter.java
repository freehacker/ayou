package com.ayou.article.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
* @author AYOU
* @version 2016年3月29日 下午1:40:29
* 用来验证拦截登录
*/
@SuppressWarnings("unused")
@WebFilter("/bbb")
public class LoginFilter implements Filter{
	private Logger logger = LoggerFactory.getLogger(LoginFilter.class);

	private ServletContext servletContext;
	// 1.可以进行配置(用户访问的页面,都可以在web.xml中进行配置)

	// 获取配置的登录页面地址
	private String login_page;
	// 获取我们需要登录才能访问的页面地址
	
	private String validate_page;
	// 获取共用的页面(不需要可以就访问的页面地址)
	private String common_page;
	// 获取当前用户访问的路径地址
	private String current_url;
	
	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		servletContext = filterConfig.getServletContext();
		
		/*login_page = filterConfig.getInitParameter("login_page");
		validate_page = filterConfig.getInitParameter("validate_page");
		common_page = filterConfig.getInitParameter("common_page");*/
		
		login_page = "/user/login.html";
		validate_page = "/";
		common_page = "html,js,css,jpg,png,gif,svg,woff,woff2,eot,ttf,*/view,*/view/*,*/about,*/login,*/reg,*/register,*/collects,*/list,*/config";
		
		logger.info(login_page);
	}
	

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse rep = (HttpServletResponse) response;
		HttpSession session = req.getSession();
		
		current_url = req.getServletPath();
		String cur= req.getRequestURI();
		logger.info("当前请求::"+cur);
		
		/*System.out.println("当前路径"+current_url);
		System.out.println("排除路径"+common_page);
		System.out.println("验证路径"+validate_page);*/
		if (isHtmlEnd(request)) {
            chain.doFilter(request, response);
            return;
        }
		
		if (common_page.indexOf(subStatic(current_url)) != -1 || common_page.indexOf(subDomain(current_url)) != -1) {
			chain.doFilter(request, response);
		} else{
			if(session.getAttribute("user") != null){
				logger.info("验证session成功："+session.getAttribute("user"));
				chain.doFilter(request, response);
				return;
			}else{
				logger.info("准备跳转登陆："+login_page);
				logger.info("当前："+current_url);
				if(!current_url.equals(login_page)){
					rep.sendRedirect(login_page+"?reDirect="+current_url);
					return;
				}
				rep.sendRedirect(login_page);
			}
		}
			
	}
	
	
	private boolean requestIsExclude(ServletRequest request) {
		// 没有设定excludes时，所以经过filter的请求都需要被处理
		if (StringUtils.isBlank(common_page)) {
			return false;
		}
		// 获取去除context path后的请求路径
		String contextPath = request.getServletContext().getContextPath();
		String uri = ((HttpServletRequest) request).getRequestURI();
		uri = uri.substring(contextPath.length());

		// 正则模式匹配的uri被排除，不需要拦截
		boolean isExcluded = uri.matches(common_page);

		if (isExcluded) {
			logger.info("request path: {} is excluded!", uri);
		}

		return isExcluded;
	}

	private boolean isHtmlEnd(ServletRequest request) {

		// 没有设定excludes时，所以经过filter的请求都需要被处理
		if (StringUtils.isBlank(common_page)) {
			return false;
		}
		// 获取去除context path后的请求路径
		String contextPath = request.getServletContext().getContextPath();
		String uri = ((HttpServletRequest) request).getRequestURI();
		
		uri = uri.substring(contextPath.length());
		uri = subStatic(uri);
		
		if (uri.equals("html")||uri.equals("js")||uri.equals("css")) {
			return true;
		} else {
			return false;
		}

	}
	
	public String subStatic(String url){
		return url.substring(url.lastIndexOf(".")+1, url.length());
	}
	
	public String subDomain(String url){
		return url.substring(url.lastIndexOf("/")+1, url.length());
	}
	
	public boolean ft(String end){
		switch (end) {
		case "add.html":
			return false;
		case "add.js":
			return false;
		case "edit.html":
			return false;
		case "del.html":
			return false;
		default:
			break;
		}
		return false;
	}
	  
	@Override
	public void destroy() {
		
	}
}
