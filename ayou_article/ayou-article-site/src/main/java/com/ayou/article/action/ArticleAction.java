package com.ayou.article.action;

import java.io.PrintWriter;
import java.io.StringWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.markdown4j.Markdown4jProcessor;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ayou.article.bo.Article;

@RestController
@RequestMapping(value = "/article", produces = { "text/json;charset=UTF-8" })
public class ArticleAction extends ArticleBaseAction {

	@RequestMapping(value = "/del")
	public String del(ModelMap map, HttpServletRequest request, String id) {
		if(StringUtils.isNotBlank(id)){
			map.put("success", true);
			map.put("成功数", articleService.del(id));
			return renderJsonMsg(map, "更新成功！");
		}else {
			map.put("msg", "id不能为空！");
			return renderJsonMsg(map, "更新失败！");
		}
	}

	@RequestMapping(value = "/update")
	public String update(ModelMap map, HttpServletRequest request,@Valid Article article, BindingResult r) {
		if(null != article.getId()){
			map.put("success", true);
			map.put("成功数", articleService.update(article));
			return renderJsonMsg(map, "更新成功！");
		}else {
			map.put("success", false);
			map.put("msg", "id不能为空！");
			return renderJsonMsg(map, "更新失败！");
		}
	}

	@RequestMapping(value = "/list")
	public String list(ModelMap map, HttpServletRequest request, Article article) {
		try {
			map.put("result", articleService.list(article));
			map.put("counts", articleService.counts(article));
			return renderJsonMsg(map, "获取列表成功！");
		} catch (Exception e) {
			//打印堆栈信息
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			logger.error("downalod exception:" + e.getMessage() + "\t" + errors.toString());
			e.printStackTrace();
			return renderJsonError(map, "异常", e);
		}
	}
	
	@RequestMapping(value = "/like/{uuid}", method = RequestMethod.POST)
	public String like(ModelMap map, HttpServletResponse response, HttpServletRequest request, @PathVariable("uuid") String uuid) {
		try {
			map.put("ok", articleService.addLike(uuid));
			return renderJsonMsg(map, "点赞成功！");
		} catch (Exception e) {
			return renderJsonError(map, "点赞失败！", e);
		}
	}
	
	@RequestMapping(value = "/preview", method = RequestMethod.POST)
	public String preview(ModelMap map, HttpServletResponse response, HttpServletRequest request, String content) {
		try {
			String result = new Markdown4jProcessor().process(content);
			map.put("result", result);
			return renderJsonMsg(map, "正常转换！");
		} catch (Exception e) {
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			map.put("result", null);
			return renderJsonError(map, "转换异常！", e);
		}
	}
}
