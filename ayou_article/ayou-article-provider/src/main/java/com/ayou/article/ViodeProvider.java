package com.ayou.article;

import java.util.List;

import com.ayou.article.bo.Viode;

public interface ViodeProvider {
	public int add(Viode viode);

	public int del(String uuid);

	public int update(Viode viode);

	public Viode view(String uuid);

	public List<Viode> list(Viode viode);

	public int counts(Viode viode);

	public int show(String uuid);

	public int like(String uuid);
}
