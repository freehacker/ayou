package com.ayou.article;

import java.util.List;
import java.util.Map;

import com.ayou.article.bo.Article;
import com.ayou.article.param.ArticleQueryParams;

/**
 * @category 文章接口
 * @version 1.0
 * @author AYOU
 * @date 2016年6月15日 上午9:54:10
 */
public interface ArticleProvider {
	public int add(Article article);

	public int del(String id);

	public int update(Article article);

	public int counts(Article article);

	public Article view(String uuid);

	public Integer addShow(String uuid);

	public Integer addLike(String uuid);

	public List<Article> list(Article article);

	/**
	 * jdbc方式
	 * 
	 * @param queueKey
	 * @param object
	 */
	public List<Map<String, Object>> listbyjdbc(ArticleQueryParams p);

	/**
	 * 发送消息到指定队列
	 * 
	 * @param queueKey
	 * @param object
	 */
	public void sendDataToQueue(String queueKey, Object object);

	public void sendMessage(Object message);
}
