package com.ayou.article.bo;

import java.io.Serializable;
import java.util.Date;

import com.ayou.module.identity.entity.User;

public class Viode implements Serializable{
	private static final long serialVersionUID = 4277549337829861180L;
	private Integer id;
	private String uuid;
	public Date createDate;
	public Date updateDate;
	private String type;
	private Boolean isprivate;
	private Integer like;
	private Integer show;
	private String title;
	private String name;
	private String description;
	private String content;
	private String status;
	private String url;
	private String down;
	private String backImage;
	private User user;
	private Integer l_start;
	private Integer l_counts;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getUuid() {
		return uuid;
	}
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public Date getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Boolean getIsprivate() {
		return isprivate;
	}
	public void setIsprivate(Boolean isprivate) {
		this.isprivate = isprivate;
	}
	public Integer getLike() {
		return like;
	}
	public void setLike(Integer like) {
		this.like = like;
	}
	public Integer getShow() {
		return show;
	}
	public void setShow(Integer show) {
		this.show = show;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getDown() {
		return down;
	}
	public void setDown(String down) {
		this.down = down;
	}
	public String getBackImage() {
		return backImage;
	}
	public void setBackImage(String backImage) {
		this.backImage = backImage;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public Integer getL_start() {
		return l_start;
	}
	public void setL_start(Integer l_start) {
		this.l_start = l_start;
	}
	public Integer getL_counts() {
		return l_counts;
	}
	public void setL_counts(Integer l_counts) {
		this.l_counts = l_counts;
	}
}
