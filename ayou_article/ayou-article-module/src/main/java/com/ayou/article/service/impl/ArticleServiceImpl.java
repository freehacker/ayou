/*package com.ayou.article.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import com.ayou.article.dao.ArticleDao;
import com.ayou.article.entity.Article;
import com.ayou.article.param.ArticleQueryParams;
import com.ayou.article.service.ArticleService;
import com.ayou.core.commons.utils.db.DBUtil;
import com.ayou.core.commons.utils.db.DBUtil.SqlTypes;
import com.ayou.module.base.dao.JdbcDao;


public class ArticleServiceImpl implements ArticleService {
	@Resource
	ArticleDao articleDao;

	@Resource
	JdbcDao jdbcDao;

	@Override
	public int add(Article article) {
		article.generateUuid();
		return jdbcDao.saveOne(article);
	}

	@Override
	public int del(String id) {
		Article article = new Article();
		article.setId(Long.parseLong(id));

		List<Object> params = new ArrayList<Object>();
		String sql = DBUtil.getSqlByObject(SqlTypes.DELETE, article, params);
		Object[] args = params.toArray(new Object[0]);

		return jdbcDao.update(sql, args);
	}

	@Override
	public int update(Article article) {
		List<Object> params = new ArrayList<Object>();
		String sql = DBUtil.getSqlByObject(SqlTypes.UPDATE, article, params);
		Object[] args = params.toArray(new Object[0]);

		return jdbcDao.update(sql, args);
	}

	@Override
	public List<Map<String, Object>> list(ArticleQueryParams p) {
		List<Object> params = new ArrayList<Object>();
		String sql = p.buildSql(params);
		return jdbcDao.queryForMapList(sql, params.toArray(new Object[0]));
	}

	@Override
	public Article view(String uuid) {
		//String sql = "SELECT a.*, u.uuid as u_uuid,u.name as u_name,u.email as u_email,u.sex u_sex,u.superUser u_superUser FROM article a, user u WHERE a.user_id=u.id AND a.id=?";
		//return jdbcDao.queryForObject(Article.class, sql, id);
		//return articleDao.findById(id.toString());
		return articleDao.view(uuid);
	}

	@Override
	public int counts(ArticleQueryParams p) {
		String sql = "SELECT id FROM article where status=?";
		return jdbcDao.counts(sql, 1);
	}

	@Override
	public Integer addShow(String uuid) {
		
		return articleDao.addShow(uuid);
		
		String sql = "UPDATE article a SET a.show = a.show+1 WHERE a.uuid = ?";
		try {
			return jdbcDao.update(sql, uuid);
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
	}

	*//**
	 * 
	 * 测试查询
	 *//*
	public void show() {
		Article article = new Article();
		article.setId((long) 14);
		article.setLike(15);
		article.setComment_uuid("270bdd4e910a4b41b2e265e648a46db7");
		article.setContent("hahaha");
		List<Object> params = new ArrayList<Object>();
		String sql = DBUtil.getSqlByObject(SqlTypes.QUERY, article, params);
		Object[] args = params.toArray(new Object[0]);

		System.out.println(sql);
		System.out.println("数组长度为：" + args.length);
		for (int i = 0; i < args.length; i++) {
			System.out.println("元素[" + (i + 1) + "]：" + args[i]);
		}
	}

	*//**
	 * 
	 * 测试删除
	 *//*
	public void delete() {
		Article article = new Article();
		// article.setId((long) 14);
		article.setLike(15);
		article.setComment_uuid("270bdd4e910a4b41b2e265e648a46db7");
		article.setContent("hahaha");
		List<Object> params = new ArrayList<Object>();
		String sql = DBUtil.getSqlByObject(SqlTypes.DELETE, article, params);
		Object[] args = params.toArray(new Object[0]);

		System.out.println(sql);
		System.out.println("数组长度为：" + args.length);
		for (int i = 0; i < args.length; i++) {
			System.out.println("元素[" + (i + 1) + "]：" + args[i]);
		}
	}

	*//**
	 * 
	 * 测试修改
	 *//*
	public void udpate() {
		Article article = new Article();
		// article.setId((long) 14);
		article.setLike(15);
		article.setComment_uuid("270bdd4e910a4b41b2e265e648a46db7");
		article.setContent("hahaha");
		List<Object> params = new ArrayList<Object>();
		String sql = DBUtil.getSqlByObject(SqlTypes.UPDATE, article, params);
		Object[] args = params.toArray(new Object[0]);

		System.out.println(sql);
		System.out.println("数组长度为：" + args.length);
		for (int i = 0; i < args.length; i++) {
			System.out.println("元素[" + (i + 1) + "]：" + args[i]);
		}
	}

	public void insert() {
		Article article = new Article();
		article.generateUuid();
		// article.setId((long) 14);
		article.setLike(15);
		article.setComment_uuid("270bdd4e910a4b41b2e265e648a46db7");
		article.setContent("hahaha");
		List<Object> params = new ArrayList<Object>();
		String sql = DBUtil.getSqlByObject(SqlTypes.INSERT, article, params);
		Object[] args = params.toArray(new Object[0]);

		System.out.println(sql);
		System.out.println("数组长度为：" + args.length);
		for (int i = 0; i < args.length; i++) {
			System.out.println("元素[" + (i + 1) + "]：" + args[i]);
		}
	}

}
*/