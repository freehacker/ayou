package com.ayou.article.param;

import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.ayou.module.base.param.SqlQueryParams;

/**
 * @author AYOU
 * @version 2016年4月7日 下午7:49:15
 */
public class ArticleQueryParams extends SqlQueryParams {
	private static final long serialVersionUID = -1175405687111963057L;
	
	private Integer type;
	private Boolean isprivate;
	private String user_uuid;
	private Integer like;
	private Integer show;
	private String comment_uuid;
	private String status;
	private Integer limit_start;
	private Integer limit_counts;
	@Override
	public String buildSql(List<Object> params) {
		String sql = "SELECT * FROM (SELECT A.*,U.NAME U_NAME,U.ID U_ID,U.HEADURL U_HEAD, U.UUID U_UUID,U.EMAIL U_EMAIL,U.SEX U_SEX FROM ARTICLE A INNER JOIN USER U ON U.UUID=A.USER_UUID LIMIT ?, ?) AS C WHERE 1=1";

		if (null != getLimit_start()) {
			params.add(getLimit_start() * getLimit_counts());
		} else {
			params.add(0);
		}
		if (null != getLimit_counts()) {
			params.add(getLimit_counts());
		} else {
			params.add(10);
		}

		if (null != getId()) {
			sql += " and id=?";
			params.add(getId());
		}
		if (StringUtils.isNoneEmpty(getUser_uuid())) {
			sql += " and user_uuid=?";
			params.add(getUser_uuid());
		}
		return sql;
	}
	
	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Boolean getIsprivate() {
		return isprivate;
	}

	public void setIsprivate(Boolean isprivate) {
		this.isprivate = isprivate;
	}

	public String getUser_uuid() {
		return user_uuid;
	}

	public void setUser_uuid(String user_uuid) {
		this.user_uuid = user_uuid;
	}

	public Integer getLike() {
		return like;
	}

	public void setLike(Integer like) {
		this.like = like;
	}

	public Integer getShow() {
		return show;
	}

	public void setShow(Integer show) {
		this.show = show;
	}

	public String getComment_uuid() {
		return comment_uuid;
	}

	public void setComment_uuid(String comment_uuid) {
		this.comment_uuid = comment_uuid;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getLimit_start() {
		return limit_start;
	}

	public void setLimit_start(Integer limit_start) {
		this.limit_start = limit_start;
	}

	public Integer getLimit_counts() {
		return limit_counts;
	}

	public void setLimit_counts(Integer limit_counts) {
		this.limit_counts = limit_counts;
	}

}
