package com.ayou.article.entity;

import com.ayou.module.base.entity.BaseEntity;

public class Video extends BaseEntity {
	private static final long serialVersionUID = -2672212947054294883L;
	
	private String type;
	private Boolean isprivate;
	private Integer like;
	private Integer show;
	private String title;
	private String name;
	private String description;
	private String content;
	private String status;
	private String url;
	private String down;
	private String backimage;
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Boolean getIsprivate() {
		return isprivate;
	}
	public void setIsprivate(Boolean isprivate) {
		this.isprivate = isprivate;
	}
	public Integer getLike() {
		return like;
	}
	public void setLike(Integer like) {
		this.like = like;
	}
	public Integer getShow() {
		return show;
	}
	public void setShow(Integer show) {
		this.show = show;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getDown() {
		return down;
	}
	public void setDown(String down) {
		this.down = down;
	}
	public String getBackimage() {
		return backimage;
	}
	public void setBackimage(String backimage) {
		this.backimage = backimage;
	}
}
