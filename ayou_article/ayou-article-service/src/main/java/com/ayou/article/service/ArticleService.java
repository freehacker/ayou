package com.ayou.article.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Propagation;
//import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.dubbo.config.annotation.Service;
import com.ayou.article.ArticleProvider;
import com.ayou.article.bo.Article;
import com.ayou.article.dao.ArticleDao;
import com.ayou.article.param.ArticleQueryParams;
import com.ayou.core.commons.utils.db.DBUtil;
import com.ayou.core.commons.utils.db.DBUtil.SqlTypes;
import com.ayou.module.base.dao.JdbcDao;

/**
 * @category 文章服务
 * @version 1.0
 * @author AYOU
 * @date 2016年6月15日 上午9:51:37
 */
@Service(version="1.0")
@Transactional(propagation = Propagation.REQUIRED)
public class ArticleService implements ArticleProvider {
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	@Resource
	ArticleDao articleDao;
	@Resource
	JdbcDao jdbcDao;
	/*@Resource
	AmqpTemplate amqpTemplate;*/

	@Override
	public int add(Article article) {
		article.generateUuid();
		return jdbcDao.saveOne(article);
	}

	@Override
	public int del(String id) {
		Article article = new Article();
		article.setId(Long.parseLong(id));
		List<Object> params = new ArrayList<Object>();
		String sql = DBUtil.getSqlByObject(SqlTypes.DELETE, article, params);
		Object[] args = params.toArray(new Object[0]);

		return jdbcDao.update(sql, args);
	}

	@Override
	public int update(Article article) {
		List<Object> params = new ArrayList<Object>();
		String sql = DBUtil.getSqlByObject(SqlTypes.UPDATE, article, params);
		Object[] args = params.toArray(new Object[0]);

		return jdbcDao.update(sql, args);
	}

	@Override
	public Article view(String uuid) {
		return articleDao.view(uuid);
	}

	@Override
	public Integer addShow(String uuid) {
		return articleDao.addShow(uuid);
	}

	@Override
	public Integer addLike(String uuid) {
		return articleDao.addLike(uuid);
	}

	@Override
	public List<Article> list(Article article) {
		if (article.getL_start() == null) {
			article.setL_start(0);
		}
		if (article.getL_counts() == null) {
			article.setL_counts(15);
		}
		return articleDao.list(article);
	}
	
	@Override
	public int counts(Article article) {
		return articleDao.counts(article);
	}
	
	@Override
	public List<Map<String, Object>> listbyjdbc(ArticleQueryParams p) {
		List<Object> params = new ArrayList<Object>();
		String sql = p.buildSql(params);
		return jdbcDao.queryForMapList(sql, params.toArray(new Object[0]));
	}
	
	@Override
	public void sendDataToQueue(String queueKey, Object object) {
		try {
			logger.info("发送开始...");
			//amqpTemplate.convertAndSend(queueKey, object);
			logger.info("发送结束...");
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.toString());
		}
	}

	@Override
	public void sendMessage(Object message) {
		logger.info("发送---------->>>{}", message);
		//amqpTemplate.convertAndSend("queueTestKey", message);
	}
}
