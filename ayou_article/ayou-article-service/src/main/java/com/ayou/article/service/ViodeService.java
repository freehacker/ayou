package com.ayou.article.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.dubbo.config.annotation.Service;
import com.ayou.article.ViodeProvider;
import com.ayou.article.bo.Viode;
import com.ayou.article.dao.ViodeDao;

@Service(version="1.0")
@Transactional(propagation = Propagation.REQUIRED)
public class ViodeService implements ViodeProvider{
	@Resource
	private ViodeDao viodeDao;

	@Override
	public int add(Viode viode) {
		return viodeDao.add(viode);
	}

	@Override
	public int del(String uuid) {
		return viodeDao.del(uuid);
	}

	@Override
	public int update(Viode viode) {
		return viodeDao.update(viode);
	}

	@Override
	public Viode view(String uuid) {
		return viodeDao.view(uuid);
	}

	@Override
	public List<Viode> list(Viode viode) {
		if (viode.getL_start() == null) {
			viode.setL_start(0);
		}
		if (viode.getL_counts() == null) {
			viode.setL_counts(15);
		}
		return viodeDao.list(viode);
	}

	@Override
	public int counts(Viode viode) {
		return viodeDao.counts(viode);
	}

	@Override
	public int show(String uuid) {
		return viodeDao.show(uuid);
	}

	@Override
	public int like(String uuid) {
		return viodeDao.like(uuid);
	}

}
