package com.ayou.article.service;

import java.io.UnsupportedEncodingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageListener;

public class MessageConsumer implements MessageListener {
	private Logger logger = LoggerFactory.getLogger(getClass());

	@Override
	public void onMessage(Message message) {
		byte[] b = message.getBody();
		String s = "";
		// String s = javax.xml.bind.DatatypeConverter.printBase64Binary(b);
		try {
			s = new String(b, "utf-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		logger.info("接收---------->>>{}", s);
	}
}
