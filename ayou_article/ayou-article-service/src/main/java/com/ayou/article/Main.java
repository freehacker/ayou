package com.ayou.article;

import java.io.IOException;

import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @category 启动-注入
 * @author AYOU
 * @version 2016年5月31日 下午9:14:55
 */
@SuppressWarnings("resource")
public class Main {
	public static void main(String[] args) throws IOException {
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(new String[] { "appctx-article-service.xml" });
		context.start();
		System.out.println("Article is OK...");
		System.in.read(); 
	}
}
