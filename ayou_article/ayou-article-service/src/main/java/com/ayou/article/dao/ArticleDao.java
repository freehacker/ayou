package com.ayou.article.dao;

import java.util.List;

import com.ayou.article.bo.Article;


public interface ArticleDao {
	public int add(Article article);
	public int update(Article article);
	public int counts(Article article);
	public Article view(String uuid);
	public Integer addShow(String uuid);
	public Integer addLike(String uuid);
	public List<Article> list(Article article);
}
