package com.ayou.article;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ayou.article.entity.Article;
import com.ayou.core.commons.utils.db.DBUtil;
import com.ayou.core.commons.utils.db.DBUtil.SqlTypes;

public class ArticleSqlUtil {
	private Logger logger = LoggerFactory.getLogger(getClass());
	/**
	 * 测试查询
	 */
	public void show() {
		Article article = new Article();
		article.setId((long) 14);
		article.setLike(15);
		article.setContent("hahaha");
		List<Object> params = new ArrayList<Object>();
		String sql = DBUtil.getSqlByObject(SqlTypes.QUERY, article, params);
		Object[] args = params.toArray(new Object[0]);

		logger.info(sql);
		logger.info("数组长度为：" + args.length);
		for (int i = 0; i < args.length; i++) {
			logger.info("元素[" + (i + 1) + "]：" + args[i]);
		}
	}

	/**
	 * 测试删除
	 */
	public void delete() {
		Article article = new Article();
		// article.setId((long) 14);
		article.setLike(15);
		article.setContent("hahaha");
		List<Object> params = new ArrayList<Object>();
		String sql = DBUtil.getSqlByObject(SqlTypes.DELETE, article, params);
		Object[] args = params.toArray(new Object[0]);

		logger.info(sql);
		logger.info("数组长度为：" + args.length);
		for (int i = 0; i < args.length; i++) {
			logger.info("元素[" + (i + 1) + "]：" + args[i]);
		}
	}

	/**
	 * 测试修改
	 */
	public void udpate() {
		Article article = new Article();
		// article.setId((long) 14);
		article.setLike(15);
		article.setContent("hahaha");
		List<Object> params = new ArrayList<Object>();
		String sql = DBUtil.getSqlByObject(SqlTypes.UPDATE, article, params);
		Object[] args = params.toArray(new Object[0]);

		logger.info(sql);
		logger.info("数组长度为：" + args.length);
		for (int i = 0; i < args.length; i++) {
			logger.info("元素[" + (i + 1) + "]：" + args[i]);
		}
	}

	public void insert() {
		Article article = new Article();
		article.generateUuid();
		// article.setId((long) 14);
		article.setLike(15);
		article.setContent("hahaha");
		List<Object> params = new ArrayList<Object>();
		String sql = DBUtil.getSqlByObject(SqlTypes.INSERT, article, params);
		Object[] args = params.toArray(new Object[0]);

		logger.info(sql);
		logger.info("数组长度为：" + args.length);
		for (int i = 0; i < args.length; i++) {
			logger.info("元素[" + (i + 1) + "]：" + args[i]);
		}
	}
}
